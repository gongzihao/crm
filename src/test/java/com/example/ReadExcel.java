package com.example;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.springframework.web.client.RestTemplate;

import cn.hd01.sms.ucp.UCPSmsService;
import cn.hd01.sms.web.SmsController;

public class ReadExcel {

	/**
	 * @param args
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws InvalidFormatException
	 * @throws EncryptedDocumentException
	 */
	public static void main(String[] args) throws EncryptedDocumentException, InvalidFormatException,
			FileNotFoundException, IOException {

//		Workbook book = WorkbookFactory.create(new FileInputStream("test.xls"));
//		book.setMissingCellPolicy(MissingCellPolicy.CREATE_NULL_AS_BLANK);
//
//		Sheet sheet = book.getSheetAt(0);
//		for (int i = 1; i <= sheet.getLastRowNum(); i++) {
//			Row row = sheet.getRow(i);
//			String[] fields = new String[row.getLastCellNum()];
//			for (int j = 0; j < row.getLastCellNum(); j++) {
//				fields[j] = convertCellToString(row.getCell(j));
//
//			}
//			System.err.println(Arrays.toString(fields));
//		}

		UCPSmsService service = new UCPSmsService();
		service.setUrl("https://api.ucpaas.com/2014-06-30/Accounts/8f405ee4f3a7759e9d27b4d8b46facf3/Messages/templateSMS");
		service.setAppid("dc56cb996ea243f893151114b5458606");
		service.setSid("8f405ee4f3a7759e9d27b4d8b46facf3");
		service.setTemplateId("43170");
		service.setToken("9e8b857af8478f894b33b6ab686ae630");
		
//		service.send("13590484861", "1234");
		
		SmsController c = new SmsController();
		System.err.println(c.code());
	}

	@SuppressWarnings("deprecation")
	private static String convertCellToString(Cell cell) {
		switch (cell.getCellTypeEnum()) {
		case BOOLEAN:
			return Boolean.toString(cell.getBooleanCellValue());
		case STRING:
			return cell.getStringCellValue();
		case NUMERIC: {
			if (DateUtil.isCellDateFormatted(cell)) {
				Date date = DateUtil.getJavaDate(cell.getNumericCellValue());
				return new SimpleDateFormat("yyyyMMddHHmmss").format(date);
			}
			return NumberToTextConverter.toText(cell.getNumericCellValue());
		}
		case FORMULA:
		case ERROR:
		case BLANK:
		default:
			return "";
		}

	}
}
