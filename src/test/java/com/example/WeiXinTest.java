package com.example;

import com.github.sd4324530.fastweixin.api.MenuAPI;
import com.github.sd4324530.fastweixin.api.SystemAPI;
import com.github.sd4324530.fastweixin.api.config.ApiConfig;
import com.github.sd4324530.fastweixin.api.entity.Menu;
import com.github.sd4324530.fastweixin.api.entity.MenuButton;
import com.github.sd4324530.fastweixin.api.enums.MenuType;
import com.github.sd4324530.fastweixin.api.enums.ResultType;
import org.assertj.core.util.Lists;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author chenyongbo
 * @date 2017/3/22.
 */
public class WeiXinTest {

    private static final ApiConfig apiConfig = new ApiConfig(
            "wx38d0b8d769820a30", "76bc0a82170c6edd9a44ffea2828bfce");

    // 本地
//    private static final ApiConfig apiConfig = new ApiConfig(
//            "wx6fb0ca823fba4b10", "5ed5c8db05f229a7b0e22e8404c33dbb");

    // 正式公众号 gh_57deaa58280a
//    private static final ApiConfig apiConfig = new ApiConfig(
//            "wx78f49bfe4b3da31d", "278808ca8014bc01692bce37bb5427a6");

    private static SystemAPI systemAPI;
    private static MenuAPI menuAPI;

    @BeforeClass
    public static void init() {
//        apiConfig.addHandle(new TestConfigChangeHandle());

        systemAPI = new SystemAPI(apiConfig);
        menuAPI = new MenuAPI(apiConfig);
    }

    @Test
    public void className() {
        System.out.println(WeiXinTest.class.getName());
    }

    @Test
    public void threadPool() {
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors
                .newFixedThreadPool(4);

        executor.prestartAllCoreThreads();
    }

    @Test
    public void getToken() {
        System.out.println(apiConfig.getAccessToken());
    }

    @Test
    public void getIpList() {
        List<String> callbackIP = systemAPI.getCallbackIP();
        for (String ip : callbackIP) {
            System.out.println(ip);
        }
        System.out.println("总计ip列表：" + callbackIP.size());
    }

    @Test
    public void deleteMenu() {
        System.out.println(menuAPI.deleteMenu().toString());
    }

    @Test
    public void queryMenu() {
        System.out.println(menuAPI.getMenu().toJsonString());
    }

    @Test
    public void createMenu() {
        // 主菜单1
//        MenuButton main1 = new MenuButton();
//        main1.setType(MenuType.VIEW);
//        main1.setKey("test");
//        main1.setName("注册");
//        main1.setUrl("http://4b427d99.ngrok.io/mall/weixin/mall/choseRegister?domain=cyb_test");

        // 子菜单
//        MenuButton sub1 = new MenuButton();
//        sub1.setType(MenuType.VIEW);
//        sub1.setKey("sub1");
//        sub1.setName("供应商注册");
//        sub1.setUrl("http://yaodian.yangpinjie.com/mall/weixin/mall/supplierRegister?domain=cyb");
//
//        MenuButton sub2 = new MenuButton();
//        sub2.setType(MenuType.VIEW);
//        sub2.setKey("sub2");
//        sub2.setName("设计师注册");
//        sub2.setUrl("http://yaodian.yangpinjie.com/mall/weixin/mall/register?domain=cyb");
//
//        MenuButton sub3 = new MenuButton();
//        sub3.setType(MenuType.VIEW);
//        sub3.setKey("sub3");
//        sub3.setName("用户注册");
//        sub3.setUrl("http://yaodian.yangpinjie.com/mall/weixin/mall/userRegister?domain=cyb");
//
//        List<MenuButton> subList = new LinkedList<>();
//        subList.add(sub1);
//        subList.add(sub2);
//        subList.add(sub3);
//
//        main1.setSubButton(subList);

        MenuButton main2 = new MenuButton();
        main2.setType(MenuType.VIEW);
        main2.setKey("test2");
        main2.setName("合作");
        main2.setUrl("http://28652ed1.ngrok.io/mall/weixin/pay/rightsPay?domain=cyb");
//        main2.setUrl("http://yaodian.yangpinjie.com/mall/weixin/pay/rightsPay?domain=yaodian");

        // 子菜单
//        MenuButton sub21 = new MenuButton();
//        sub21.setType(MenuType.VIEW);
//        sub21.setKey("sub21");
//        sub21.setName("供应商权益");
//        sub21.setUrl("http://4b427d99.ngrok.io/mall/weixin/pay/supplierRightsPay?domain=cyb_test");
//
//        MenuButton sub22 = new MenuButton();
//        sub22.setType(MenuType.VIEW);
//        sub22.setKey("sub22");
//        sub22.setName("设计师权益");
//        sub22.setUrl("http://4b427d99.ngrok.io/mall/weixin/pay/rightsPay?domain=cyb_test");
//
//        main2.setSubButton(Lists.newArrayList(sub21, sub22));

        MenuButton main3 = new MenuButton();
        main3.setType(MenuType.CLICK);
        main3.setKey("test3");
        main3.setName("体验");
        // 子菜单
        MenuButton sub31 = new MenuButton();
        sub31.setType(MenuType.VIEW);
        sub31.setKey("sub31");
        sub31.setName("体验中心");
        sub31.setUrl("http://0f472afa.ngrok.io/mall/tiyan/index");
        sub31.setUrl("http://yaodian.yangpinjie.com/mall/tiyan/index");
        main3.setSubButton(Lists.newArrayList(sub31));

        List<MenuButton> mainList = new LinkedList<>();
//        mainList.add(main1);
        mainList.add(main2);
        mainList.add(main3);

        Menu menu = new Menu();
        menu.setButton(mainList);

        System.out.println(menu.toJsonString());
        ResultType result = menuAPI.createMenu(menu);
        System.out.println(result.toString());
    }

}
