package com.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import cn.hd01.repository.entity.User;
import cn.hd01.service.PermissionsService;
import cn.hd01.service.UserService;

import com.alibaba.fastjson.JSON;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AdminApplicationTests {

	@Autowired
	private UserService service;

	@Autowired
	private PermissionsService ps;

	@Test
	public void findUserTest() {
		User u = new User();
		u.setName("dddd");
		u.setMobile("13590484861");
		u.setRoleName("超级管理员");
		Page<User> page = service.findAdmin(null, new PageRequest(0, 10));

		System.out.println(JSON.toJSONString(page, true));
	}

	@Test
	public void findpassword() {
		User user = service.find("gzh", "11111");
		System.out.println(JSON.toJSONString(user, true));
	}

	@Test
	public void exsitName() {
		System.out.println(service.exists("sss"));
	}

}
