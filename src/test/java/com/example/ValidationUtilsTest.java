package com.example;

import java.util.Date;

import cn.hd01.util.ValidationResult;
import cn.hd01.util.ValidationUtils;

public class ValidationUtilsTest {

	public void validateSimpleEntity() {
		SimpleEntity se = new SimpleEntity();
		se.setDate(new Date());
		se.setEmail("123");
		se.setName("123");
		se.setPassword("123");
		se.setValid(false);
		ValidationResult result = ValidationUtils.validateEntity(se);
		System.out.println("--------------------------");
		System.out.println(result);
	}

	public void validateSimpleProperty() {
		SimpleEntity se = new SimpleEntity();
		ValidationResult result = ValidationUtils.validateProperty(se, "name");
		System.out.println("--------------------------");
		System.out.println(result);
	}

	public static void main(String[] args) {
		new ValidationUtilsTest().validateSimpleEntity();
	}
}
