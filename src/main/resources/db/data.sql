INSERT INTO `sys_permissions`(id, NAME, url,`menu_Order`, parentid, is_Menu, img) VALUES(0, '首页' , '/index', 10, -1, 1, 'icon-home');
INSERT INTO `sys_permissions`(id, NAME, url,`menu_Order`, parentid, is_Menu, img) VALUES(1, '微信管理' , '', 13, -1, 1, 'icon-home');
INSERT INTO `sys_permissions`(id, NAME, url,`menu_Order`, parentid, is_Menu, img) VALUES(2, '微信配置' , '/wxapp', 102, 1, 1, 'icon-home');
INSERT INTO `sys_permissions`(id, NAME, url,`menu_Order`, parentid, is_Menu, img) VALUES(3, '客户管理' , '/client/', 103, 1, 1, 'icon-users');
INSERT INTO `sys_permissions`(id, NAME, url,`menu_Order`, parentid, is_Menu, img) VALUES(4, '系统管理' , '', 14, -1, 1, 'icon-home');
INSERT INTO `sys_permissions`(id, NAME, url,`menu_Order`, parentid, is_Menu, img) VALUES(5, '系统参数' , '/dict/', 104, 4, 1, 'icon-pencil');
INSERT INTO `sys_permissions`(id, NAME, url,`menu_Order`, parentid, is_Menu, img) VALUES(6, '角色管理' , '/role/', 105, 4, 1, 'icon-home');
INSERT INTO `sys_permissions`(id, NAME, url,`menu_Order`, parentid, is_Menu, img) VALUES(7, '用户管理' , '/user/', 106, 4, 1, 'icon-users');
insert into `sys_permissions`(id, name, url,`menu_Order`, parentid, is_Menu, img) values(8, '会员等级' , '/vip/', 107, 4, 1, 'icon-filter');
insert into `sys_permissions`(id, name, url,`menu_Order`, parentid, is_Menu, img) values(9, '数据管理' , '', 11, -1, 1, 'icon-align-justify');
insert into `sys_permissions`(id, name, url,`menu_Order`, parentid, is_Menu, img) values(10, '方案管理' , '/', 101, 9, 1, 'icon-file-picture');
insert into `sys_permissions`(id, name, url,`menu_Order`, parentid, is_Menu, img) values(11, '产品管理' , '/product/', 105, 9, 1, 'icon-book');

insert into `sys_permissions`(id, name, url,`menu_Order`, parentid, is_Menu, img) values(12, '发单接单' , '', 110, -1, 1, 'icon-book');
insert into `sys_permissions`(id, name, url,`menu_Order`, parentid, is_Menu, img) values(13, '我的发单' , '/requirement/my', 111, 12, 1, 'icon-arrow-up');
-- insert into `sys_permissions`(id, name, url,`menu_Order`, parentid, is_Menu, img) values(14, '发单审核' , '/requirement/exam', 112, 12, 1, 'icon-eye-blocked');
insert into `sys_permissions`(id, name, url,`menu_Order`, parentid, is_Menu, img) values(15, '我的接单' , '/requirement/pick', 113, 12, 1, 'icon-arrow-down');
insert into `sys_permissions`(id, name, url,`menu_Order`, parentid, is_Menu, img) values(16, '待审核列表' , '/requirement/exam', 115, 12, 1, 'icon-list');
insert into `sys_permissions`(id, name, url,`menu_Order`, parentid, is_Menu, img) values(17, '待接单列表' , '/requirement/status/2', 116, 12, 1, 'icon-list');
insert into `sys_permissions`(id, name, url,`menu_Order`, parentid, is_Menu, img) values(18, '待提交作品列表' , '/requirement/status/3', 117, 12, 1, 'icon-list');
insert into `sys_permissions`(id, name, url,`menu_Order`, parentid, is_Menu, img) values(19, '待验收列表' , '/requirement/status/4', 118, 12, 1, 'icon-list');
insert into `sys_permissions`(id, name, url,`menu_Order`, parentid, is_Menu, img) values(20, '代付款列表' , '/requirement/status/5', 119, 12, 1, 'icon-list');
insert into `sys_permissions`(id, name, url,`menu_Order`, parentid, is_Menu, img) values(21, '已完成列表' , '/requirement/status/6', 120, 12, 1, 'icon-list');
insert into `sys_permissions`(id, name, url,`menu_Order`, parentid, is_Menu, img) values(22, '审核拒绝列表' , '/requirement/status/7', 121, 12, 1, 'icon-list');
insert into `sys_permissions`(id, name, url,`menu_Order`, parentid, is_Menu, img) values(23, '产品banner列表' , '/banner/type/1', 106, 9, 1, 'icon-list');
insert into `sys_permissions`(id, name, url,`menu_Order`, parentid, is_Menu, img) values(24, '方案banner列表' , '/banner/type/2', 107, 9, 1, 'icon-list');
insert into `sys_permissions`(id, name, url,`menu_Order`, parentid, is_Menu, img) values(25, '我的购物车' , '/car/', 108, 9, 1, 'glyphicon glyphicon-shopping-cart');

insert into `sys_role`(id, name, description, create_time, read_only) values(1, '超级管理员','超级管理员角色', now(), true);
insert into `sys_role`(id, name, description, create_time, read_only) values(2, '普通用户','普通用户角色', now(), true);
insert into `sys_role`(id, name, description, create_time, read_only) values(3, '普通设计师','普通设计师角色', now(), true);
insert into `sys_role`(id, name, description, create_time, read_only) values(4, '供应商','供应商角色', now(), true);
insert into `sys_role`(id, name, description, create_time, read_only) values(5, '原创设计师','原创设计师角色', now(), true);


insert into `sys_role_permissions`(role_Id, permission_Id) values(1, 0);
insert into `sys_role_permissions`(role_Id, permission_Id) values(1, 1);
insert into `sys_role_permissions`(role_Id, permission_Id) values(1, 2);
insert into `sys_role_permissions`(role_Id, permission_Id) values(1, 3);
insert into `sys_role_permissions`(role_Id, permission_Id) values(1, 4);
insert into `sys_role_permissions`(role_Id, permission_Id) values(1, 5);
insert into `sys_role_permissions`(role_Id, permission_Id) values(1, 6);
insert into `sys_role_permissions`(role_Id, permission_Id) values(1, 7);
insert into `sys_role_permissions`(role_Id, permission_Id) values(1, 8);
insert into `sys_role_permissions`(role_Id, permission_Id) values(1, 9);
insert into `sys_role_permissions`(role_Id, permission_Id) values(1, 10);
insert into `sys_role_permissions`(role_Id, permission_Id) values(1, 11);
insert into `sys_role_permissions`(role_Id, permission_Id) values(1, 11);
insert into `sys_role_permissions`(role_Id, permission_Id) values(1, 12);
insert into `sys_role_permissions`(role_Id, permission_Id) values(1, 13);
-- insert into `sys_role_permissions`(role_Id, permission_Id) values(1, 14);
insert into `sys_role_permissions`(role_Id, permission_Id) values(1, 15);
insert into `sys_role_permissions`(role_Id, permission_Id) values(1, 16);
insert into `sys_role_permissions`(role_Id, permission_Id) values(1, 17);
insert into `sys_role_permissions`(role_Id, permission_Id) values(1, 18);
insert into `sys_role_permissions`(role_Id, permission_Id) values(1, 19);
insert into `sys_role_permissions`(role_Id, permission_Id) values(1, 20);
insert into `sys_role_permissions`(role_Id, permission_Id) values(1, 21);
insert into `sys_role_permissions`(role_Id, permission_Id) values(1, 22);
insert into `sys_role_permissions`(role_Id, permission_Id) values(1, 23);
insert into `sys_role_permissions`(role_Id, permission_Id) values(1, 24);
insert into `sys_role_permissions`(role_Id, permission_Id) values(1, 25);


insert into `sys_user`(name, password,email, role_Id,role_name,mobile,type) values('admin', 'admin', '123@qq.com', 1, '超级管理员','13590484861', 3);

insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('system', '域名', 'www.baidu.com', '配置系统的域名', true);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('system', 'weixin', 'www.baidu.com', '配置系统的域名', true);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('system', 'app', 'www.baidu.com', '配置系统的域名', true);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('staffType', '催乳师', '催乳师', '配置系统的域名', true);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('staffType', '调理师', '调理师', '配置系统的域名', true);

insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('userType', '普通用户', '0', '商城用户类型参数', true);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('userType', '设计师', '1',  '商城用户类型参数', true);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('userType', '供应商', '2', '商城用户类型参数', true);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('userType', '后台用户', '3', '后台用户类型参数', true);


insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('major', '办公', '1', '行业参数', false);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('major', '酒店', '2', '行业参数', false);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('major', '商业', '3', '行业参数', false);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('major', '地产', '4', '行业参数', false);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('major', '其他', '5', '行业参数', false);

insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('city', '深圳', '深圳', '城市', false);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('city', '上海', '上海', '城市', false);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('city', '北京', '北京', '城市', false);

insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('style', '新中', '1', '风格参数', false);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('style', '现代', '2', '风格参数', false);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('style', '简欧', '3', '风格参数', false);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('style', '工业', '4', '风格参数', false);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('style', '其他', '5', '风格参数', false);

insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('productType', '工装', '1', '产品类型-工装', false);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('productType', '家装', '2', '产品类型-家装', false);

insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('requirement_status', '待付定金', '0', '发单人提单完成, 等待付定金', true);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('requirement_status', '待审核', '1', '发单人已付完定金, 等待管理员审核', true);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('requirement_status', '待接单', '2', '发单已经审核通过, 等待设计师接单', true);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('requirement_status', '待提交作品', '3', '设计师已接单, 正在处理中', true);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('requirement_status', '待验收', '4', '设计师提交作品, 等待发单人验收', true);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('requirement_status', '待付款', '5', '审核通过，等待发单人支付尾款', true);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('requirement_status', '已完成', '6', '订单已经付款, 流程结束', true);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('requirement_status', '审核拒绝', '7', '审核不通过', true);

insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('requirement_flow', '建模完成', '建模完成', '接单设计师建模完成', true);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('requirement_flow', '确认建模完成', '确认建模完成', '发单人确认建模完成', true);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('requirement_flow', '灯光渲染完成', '灯光渲染完成', '接单设计师灯光渲染完成', true);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('requirement_flow', '确认灯光渲染完成', '确认灯光渲染完成', '发单设计师灯光渲染完成', true);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('requirement_flow', '后期处理完成', '后期处理完成', '接单设计师后期处理完成', true);
insert into `sys_dictionary`(type, name, `value`, comment, read_Only) values('requirement_flow', '确认后期处理完成', '确认后期处理完成', '发单设计师确认后期处理完成', true);


insert into `sys_weixin_app`(weixin_Id, name, app_Id, app_Secret, domain, token, type, mch_id, sh_key) values('gh_990ef69d78e9', 'test', 'wx1582a9e9099f5702', '97f8a80159f21aca1f6746c3006ff94b', 'gzh', 'eric', 1, '1467096702', '');
insert into `sys_weixin_app`(weixin_Id, name, app_Id, app_Secret, domain, token, type, mch_id, sh_key) values('gh_3d3c97ac33cd', 'cyb', 'wx38d0b8d769820a30', '76bc0a82170c6edd9a44ffea2828bfce', 'cyb', 'code', 1, '1467096702', '');
insert into `sys_weixin_app`(weixin_Id, name, app_Id, app_Secret, domain, token, type, mch_id, sh_key) values('gh_f1768bebb10d', 'cyb_test', 'wx6fb0ca823fba4b10', '5ed5c8db05f229a7b0e22e8404c33dbb', 'cyb_test', 'code', 1, '1467096702', 'D1846404322d11e7b6af4ad44cfd1468');