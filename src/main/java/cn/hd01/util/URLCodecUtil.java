package cn.hd01.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class URLCodecUtil {
	public static final String dfltEncName = "utf-8";

	public static String encode(String str) {
		try {
			return URLEncoder.encode(str, dfltEncName);
		} catch (UnsupportedEncodingException e) {
			return str;
		}
	}

	public static String decode(String str) {
		try {
			return URLDecoder.decode(str, dfltEncName);
		} catch (UnsupportedEncodingException e) {
			return str;
		}
	}
}
