package cn.hd01.util.mapper;

import java.text.ParseException;
import java.util.Date;

import cn.hd01.export.AbstractRowProccess;
import cn.hd01.export.impl.ExcelImporter;
import cn.hd01.export.listener.RowProccessListener;
import cn.hd01.repository.entity.Customer;
import cn.hd01.service.CustomerService;

public class CustomerMapper extends AbstractRowProccess<Customer> {

	public CustomerMapper(RowProccessListener listener) {
		super(listener);
	}

	private CustomerService repository;

	public void setRepository(CustomerService customerService) {
		this.repository = customerService;
	}

	@Override
	public Customer mapper(String[] row) {
		Customer customer = new Customer();
		customer.setName(row[0]);
		customer.setPhone(row[1]);
		customer.setWeixin(row[2]);
		customer.setAddress(row[3]);
		customer.setHospital(row[4]);
		customer.setBirthday(parse(row[5]));
		customer.setSex(row[6]);
		customer.setStore(row[7]);
		customer.setSource(row[8]);
		customer.setComment(row[9]);
		return customer;
	}

	private Date parse(String date) {
		try {
			return ExcelImporter.DATA_FORMATER.parse(date);
		} catch (ParseException e) {
			return null;
		}
	}

	@Override
	public void process(Customer obj) {
		repository.save(obj);
	}

}
