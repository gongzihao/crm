package cn.hd01.util;

import java.util.ArrayList;
import java.util.List;

public class TreeNode<T> {
	private T data;
	private List<TreeNode<T>> children = new ArrayList<TreeNode<T>>();

	public TreeNode() {
	}

	public TreeNode(T data) {
		this.data = data;
	}

	public List<TreeNode<T>> getChildren() {
		return children;
	}

	public void addChildren(TreeNode<T> children) {
		this.children.add(children);
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	};

}
