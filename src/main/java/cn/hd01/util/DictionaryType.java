package cn.hd01.util;

public enum DictionaryType {
	SYSTEM("系统配置", "system", "配置系统相关参数"),
    USER_TYPE("用户类型", "userType", "商城用户类型参数"),
	MAJOR("行业", "major", "行业参数"),
	CITY("城市", "city", "城市参数"),
	STYLE("风格", "style", "风格参数"),
	REQUIREMENT_STATUS("发单状态", "requirement_status", "发单状态参数"),
	PRODUCT_TYPE("产品类型", "productType", "产品类型参数"),
	REQUIREMENT_FLOW("发单验收流程", "requirement_flow", "发单验收流程"),
	;

	private String text;
	private String name;
	private String desc;

	private DictionaryType(String text, String name) {
		this.text = text;
		this.name = name;
	}

	private DictionaryType(String text, String name, String desc) {
		this(text, name);
		this.desc = desc;
	}

	public String getName() {
		return name;
	}

	public String getDesc() {
		return desc;
	}

	public String getText() {
		return text;
	}

	@Override
	public String toString() {
		return text;
	}

}
