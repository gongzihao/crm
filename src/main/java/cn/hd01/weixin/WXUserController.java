package cn.hd01.weixin;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.hd01.repository.entity.CustomerFeedBack;
import cn.hd01.repository.entity.Staff;
import cn.hd01.repository.entity.StaffFeedback;
import cn.hd01.repository.entity.TelReservation;
import cn.hd01.repository.entity.WXApp;
import cn.hd01.repository.entity.WXOauth;
import cn.hd01.service.CustomerService;
import cn.hd01.service.StaffService;
import cn.hd01.service.TelReserveService;
import cn.hd01.service.WXAppService;
import cn.hd01.web.auth.Auth;
import cn.hd01.web.auth.AuthType;
import cn.hd01.web.util.WebHelper;

import com.github.sd4324530.fastweixin.api.JsAPI;
import com.github.sd4324530.fastweixin.api.response.GetSignatureResponse;

@Controller
@RequestMapping("/weixin")
@Auth(AuthType.WECHAT)
public class WXUserController {

	@Autowired
	private StaffService staffService;

	@Autowired
	private WXAppService appService;

	@Autowired
	private TelReserveService telService;

	@Autowired
	private CustomerService customerService;

	@RequestMapping(path = "/bind", method = RequestMethod.GET)
	public String index(Model m) {
		WXOauth oauth = WebHelper.currentWXOauth();
		Staff weixinUser = staffService.findByOauthId(oauth.getId());
		if (weixinUser == null) {
			return "redirect:/weixin/register";
		}

		m.addAttribute("user", weixinUser);
		m.addAttribute("list", telService.findByStaffId(weixinUser.getId()));
		return "/weixin/index";
	}

	@RequestMapping(path = "/register", method = RequestMethod.GET)
	public String register() {
		return "/weixin/register";
	}

	@RequestMapping(path = "/register", method = RequestMethod.POST)
	public String register(Model m, Staff user) {
		Staff tmp = staffService.findByNameAndPassword(user.getName(), user.getPassword());
		if(tmp == null){
			m.addAttribute("msg", "用户名或者密码错误");
			m.addAttribute("return", WebHelper.basePath() + "/weixin/register");
			return "/weixin/fail";
		}
		
		tmp.setOauthId(WebHelper.currentWXOauth().getId());
		tmp.setBindTime(new Date());
		staffService.save(tmp);
		return "redirect:/weixin/bind";
	}

	@RequestMapping(path = "/detail/{id}", method = RequestMethod.GET)
	private String detail(@PathVariable("id") Integer id, Model m) {
		TelReservation reservation = telService.findOne(id);
		m.addAttribute("reservation", reservation);
		m.addAttribute("customer", customerService.findOne(reservation.getCustomerId()));

		if (reservation.getState() == 2) {
			WXApp app = appService.findByWeixinId(WebHelper.currentWXOauth().getWeixinId());
			GetSignatureResponse response = new JsAPI(appService.getApiConfig(app)).getSignature(WebHelper.fullUrl());
			m.addAttribute("config", response);
			m.addAttribute("appid", app.getAppId());
			m.addAttribute("url", WebHelper.basePath() + "/weixin/customerFeedBack/" + id);
		}

		return "/weixin/detail";
	}

	@Auth(AuthType.OFF)
	@RequestMapping(path = "/customerFeedBack/{id}", method = RequestMethod.GET)
	private String customerFeedBack(@PathVariable("id") Integer id, Model m) {
		TelReservation reservation = telService.findOne(id);
		if (reservation.getState() != 2) {
			m.addAttribute("msg", "该条记录无法评价");
			return "/weixin/fail";
		}
		
		m.addAttribute("id", id);
		return "/weixin/customerFeedBack";
	}

	@Auth(AuthType.OFF)
	@RequestMapping(path = "/customerFeedBack", method = RequestMethod.POST)
	private String customerFeedBack(CustomerFeedBack feedBack, Model m) {
		telService.customerFeedBack(feedBack);
		m.addAttribute("msg", "谢谢您的评价");
		return "/weixin/success";
	}

	@RequestMapping(path = "/staffFeedBack/{id}", method = RequestMethod.GET)
	private String staffFeedBack(@PathVariable("id") Integer id, Model m) {
		m.addAttribute("id", id);
		return "/weixin/staffFeedBack";
	}

	@RequestMapping(path = "/staffFeedBack", method = RequestMethod.POST)
	private String staffFeedBack(StaffFeedback feedBack, Model m) {
		telService.staffFeedback(feedBack);
		m.addAttribute("msg", "成功提交反馈");
		m.addAttribute("return", WebHelper.basePath() + "/weixin/bind");
		return "/weixin/success";
	}

	@RequestMapping(path = "/state/{id}", method = RequestMethod.GET)
	private String detail(@PathVariable("id") Integer id) {
		telService.updateState(id);
		return "redirect:/weixin/bind";
	}
}
