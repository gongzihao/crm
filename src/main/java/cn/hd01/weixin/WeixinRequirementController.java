package cn.hd01.weixin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.hd01.repository.entity.Requirement;
import cn.hd01.repository.entity.User;
import cn.hd01.repository.entity.WXOauth;
import cn.hd01.service.DictionaryService;
import cn.hd01.service.RequirementService;
import cn.hd01.service.UserService;
import cn.hd01.util.DictionaryType;
import cn.hd01.web.auth.Auth;
import cn.hd01.web.auth.AuthType;
import cn.hd01.web.util.WebHelper;
import cn.hd01.weixin.model.Result;

/**
 * Created by eric.gong Time:2017/5/14 12:19
 */
@Controller
@RequestMapping("/weixin/requirement")
@Auth(AuthType.WECHAT)
public class WeixinRequirementController {

	// 发单定金
	@Value("${requirement.deposits.rmb}")
	private double depositsRmb;

	@Autowired
	private RequirementService requirementService;

	@Autowired
	private DictionaryService dictionaryService;

	@Autowired
	private UserService userService;

	@RequestMapping(path = "/index", method = RequestMethod.GET)
	public String index(Model model) {
		WXOauth auth = WebHelper.currentWXOauth();
		User user = userService.findByOauthId(auth.getId());

		if (user == null || user.getRoleId() != 5) {
			return "weixin/noAccess";
		}

		model.addAttribute("majors", dictionaryService.findByType(DictionaryType.MAJOR));
		model.addAttribute("styles", dictionaryService.findByType(DictionaryType.STYLE));
		return "weixin/jiedan";
	}

	@RequestMapping(path = "/my", method = RequestMethod.GET)
	public String my(Model model) {
		WXOauth auth = WebHelper.currentWXOauth();
		User user = userService.findByOauthId(auth.getId());

		if (user == null || user.getRoleId() != 5) {
			return "weixin/noAccess";
		}

		model.addAttribute("majors", dictionaryService.findByType(DictionaryType.MAJOR));
		model.addAttribute("styles", dictionaryService.findByType(DictionaryType.STYLE));
		return "weixin/my";
	}

	@RequestMapping(path = "/add", method = RequestMethod.GET)
	public String add(Model model) {
		WXOauth auth = WebHelper.currentWXOauth();
		User user = userService.findByOauthId(auth.getId());

		if (user == null || user.getType() != 1) {
			return "weixin/noAccess";
		}

		model.addAttribute("majors", dictionaryService.findByType(DictionaryType.MAJOR));
		model.addAttribute("styles", dictionaryService.findByType(DictionaryType.STYLE));
		return "weixin/addRequirement";
	}

	/**
	 * 接单大厅列表，显示待接单列表 状态为 2-审核通过
	 */
	@ResponseBody
	@RequestMapping(path = "/pick/allList", method = RequestMethod.GET)
	public Page<Requirement> pickAllList(Requirement requirement, @PageableDefault Pageable pageable) {
		// 审核通过单子
		requirement.setStatus(2);
		return requirementService.find(requirement, pageable);
	}

	/**
	 * 设计师接单
	 */
	@ResponseBody
	@RequestMapping(path = "/pick/{id}", method = RequestMethod.GET)
	public int pickRequirement(@PathVariable("id") Integer id) {
		WXOauth auth = WebHelper.currentWXOauth();
		User user = userService.findByOauthId(auth.getId());
		return requirementService.pick(id, user);
	}

	@ResponseBody
	@RequestMapping(path = "/my/list", method = RequestMethod.GET)
	public Page<Requirement> myList(Requirement requirement, @PageableDefault Pageable pageable) {
		WXOauth auth = WebHelper.currentWXOauth();
		User user = userService.findByOauthId(auth.getId());
		requirement.setDesignerId(user.getId());

		return requirementService.find(requirement, pageable);
	}

	@ResponseBody
	@RequestMapping(path = "/add", method = RequestMethod.POST)
	public Result addRequirement(Requirement requirement) {
		WXOauth auth = WebHelper.currentWXOauth();
		User user = userService.findByOauthId(auth.getId());

		requirement.setOwnerId(user.getId());
		requirement.setOwnerName(user.getName());
		requirement.setStatus(0);
		requirement.setAlreadyPaid(0.0);
		requirement.setBudget(requirement.getBudget()+"万");
		requirement.setDeposits(depositsRmb);

		requirementService.save(requirement);
		return new Result(1, "", "/mall/weixin/requirement/add/success");
	}

	@RequestMapping(path = "/add/success", method = RequestMethod.GET)
	public String success(Model model) {
		model.addAttribute("msg", "您的发单信息已经成功录入系统，请登陆PC端系统上传相关附件");
		return "weixin/success";
	}

}
