package cn.hd01.weixin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import cn.hd01.web.auth.Auth;
import cn.hd01.web.auth.AuthType;

@Controller
@RequestMapping("/tiyan")
@Auth(AuthType.OFF)
public class TiyanController {

	@RequestMapping(path = "/index", method = RequestMethod.GET)
	public String index() {
		return "weixin/tiyan/index";
	}

	@RequestMapping(path = "/detail", method = RequestMethod.GET)
	public String detail(@RequestParam("id") String id, Model m) {
		m.addAttribute("id", id);
		return "weixin/tiyan/detail";
	}
}
