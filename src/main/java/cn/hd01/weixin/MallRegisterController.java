package cn.hd01.weixin;

import cn.hd01.repository.entity.Role;
import cn.hd01.service.DictionaryService;
import cn.hd01.service.RoleService;
import cn.hd01.util.DictionaryType;
import cn.hd01.web.util.WebConstant;
import cn.hd01.weixin.model.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.sd4324530.fastweixin.api.JsAPI;
import com.github.sd4324530.fastweixin.api.response.GetSignatureResponse;

import cn.hd01.repository.entity.User;
import cn.hd01.repository.entity.WXApp;
import cn.hd01.service.UserService;
import cn.hd01.service.WXAppService;
import cn.hd01.web.auth.Auth;
import cn.hd01.web.auth.AuthType;
import cn.hd01.web.util.WebHelper;

@Controller
@RequestMapping("/weixin/mall/")
@Auth(AuthType.WECHAT)
public class MallRegisterController {

	@Value("${mall.user.normal.roleId}")
	private int normalUserRoleId;

	@Value("${mall.user.designer.roleId}")
	private int designerUserRoleId;

	@Value("${mall.user.origin.designer.roleId}")
	private int originDesignerUserRoleId;

	@Value("${mall.user.supplier.roleId}")
	private int supplierUserRoleId;

	@Autowired
	private RoleService roleService;

	@Autowired
	private DictionaryService dictionaryService;

	@Autowired
	private WXAppService appService;

	@Autowired
	private UserService service;

	@RequestMapping(path = "/choseRegister", method = RequestMethod.GET)
	public String choseRegister(){
		return "weixin/mall/choseRole";
	}

	@RequestMapping(path = "/register", method = RequestMethod.GET)
	public String register(Model m) {
		jsApi(m);
		m.addAttribute("cities", dictionaryService.findByType(DictionaryType.CITY));
		m.addAttribute("majors", dictionaryService.findByType(DictionaryType.MAJOR));
		return "weixin/mall/register";
	}

	@RequestMapping(path = "/userRegister", method = RequestMethod.GET)
	public String userRegister(Model m) {
		jsApi(m);
		m.addAttribute("cities", dictionaryService.findByType(DictionaryType.CITY));
		m.addAttribute("majors", dictionaryService.findByType(DictionaryType.MAJOR));
		return "weixin/mall/userRegister";
	}
	
	@RequestMapping(path = "/supplierRegister", method = RequestMethod.GET)
	public String supplierRegister(Model m) {
		jsApi(m);
		m.addAttribute("cities", dictionaryService.findByType(DictionaryType.CITY));
		return "weixin/mall/supplierRegister";
	}

	@RequestMapping(path = "/register", method = RequestMethod.POST)
	@ResponseBody
	public Result save(Model m, User u, String verifyCode, Integer grade) {
		String code = WebHelper.sessionAttr("code", String.class);
		if (verifyCode == null || !verifyCode.equals(code)) {
			return new Result(0, "验证码错误");
		}
		Integer oauthId = WebHelper.currentWXOauth().getId();
		User oldUser = service.findByOauthId(oauthId);
		if (oldUser != null){
			return new Result(0, "已经注册过");
		}

		if (u.getType() == 0){
			u.setRoleId(normalUserRoleId);
		} else if (u.getType() ==1){
			if (grade == 1){
				// 原创设计师
				u.setRoleId(originDesignerUserRoleId);
			} else {
				u.setRoleId(designerUserRoleId);
			}
		} else if (u.getType() == 2){
			u.setRoleId(supplierUserRoleId);
		}

		Role role = roleService.findOne(u.getRoleId());
		if (role != null){
			u.setRoleName(role.getName());
		}

		WebHelper.session().removeAttribute("code");
		u.setOauthId(oauthId);
		service.save(u);
		return new Result(1, "注册成功", WebHelper.getAndRemoveSessionAttr(WebConstant.REDIRECT_URL, String.class));
	}

	@RequestMapping(path = "/registerSuccess")
    public String registerSuccess(){
	    return "weixin/mall/registerSuccess";
    }

	private void jsApi(Model m) {
		WXApp app = appService.findByWeixinId(WebHelper.currentWXOauth().getWeixinId());
		GetSignatureResponse response = new JsAPI(appService.getApiConfig(app)).getSignature(WebHelper.fullUrl());
		m.addAttribute("config", response);
		m.addAttribute("appid", app.getAppId());
	}
}
