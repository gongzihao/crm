package cn.hd01.weixin.model;

/**
 * @author cyb
 * @date 2017/4/28.
 */
public class Result {

    private int code;

    private String msg;

    private String redirectUrl;

    /**
     * 操作结果
     *
     * @param code 1 成功 0 失败
     * @param msg  提示信息
     */
    public Result(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /**
     * 操作结果
     *
     * @param code        1 成功 0 失败
     * @param msg         提示信息
     * @param redirectUrl 跳转url
     */
    public Result(int code, String msg, String redirectUrl) {
        this(code, msg);
        this.redirectUrl = redirectUrl;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }
}
