package cn.hd01.weixin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.hd01.repository.entity.Banner;
import cn.hd01.repository.entity.Style;
import cn.hd01.repository.entity.Upload;
import cn.hd01.repository.entity.User;
import cn.hd01.repository.entity.ViewHistory;
import cn.hd01.repository.entity.WXOauth;
import cn.hd01.service.BannerService;
import cn.hd01.service.DictionaryService;
import cn.hd01.service.StyleService;
import cn.hd01.service.UploadService;
import cn.hd01.service.UserService;
import cn.hd01.service.ViewHistoryService;
import cn.hd01.util.DictionaryType;
import cn.hd01.web.auth.Auth;
import cn.hd01.web.auth.AuthType;
import cn.hd01.web.util.WebHelper;

@Controller
@RequestMapping("/weixin/style")
@Auth(AuthType.WECHAT)
public class WXStyleController {
	@Autowired
	private StyleService styleService;

	@Autowired
	private DictionaryService dictionaryService;
	
	@Autowired
	private BannerService bannerService;

	@Autowired
	private UploadService uploadService;
	
	@Autowired
	private ViewHistoryService vhService;

	@Autowired
	private UserService userService;
	
	@RequestMapping(path = { "/", "" }, method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute("styles", dictionaryService.findByType(DictionaryType.STYLE));
		model.addAttribute("banners", bannerService.findByType(Banner.TYPE_STYLE));
		return "weixin/style";
	}

	@ResponseBody
	@RequestMapping(path = { "/list" }, method = RequestMethod.GET)
	public Page<Style> List(String hot, String style, @PageableDefault Pageable pageable) {
		Style domain = new Style();

		if ("0".equals(hot)) {
			domain.setHot(true);
		}

		if ("1".equals(hot)) {
			domain.setRecommend(true);
		}

		if (StringUtils.hasText(style)) {
			domain.setType(Integer.valueOf(style));
		}
		return styleService.findAll(domain, pageable);
	}

	@RequestMapping(path = { "/detail/{id}" }, method = RequestMethod.GET)
	public String detail(Model model, @PathVariable Integer id) {
		WXOauth oauth = WebHelper.currentWXOauth();
		User user = userService.findByOauthId(oauth.getId());
		vhService.view(user.getId(), ViewHistory.TYPE_STYLE, id);
		
		List<Upload> imgs = uploadService.findByTypeAndRefIdOrderBySortAsc("style", id);
		model.addAttribute("imgs", imgs);
		model.addAttribute("style", styleService.findOne(id));
		return "weixin/styleDetail";
	}
}
