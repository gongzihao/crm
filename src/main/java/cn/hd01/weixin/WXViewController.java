package cn.hd01.weixin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.hd01.repository.entity.User;
import cn.hd01.repository.entity.ViewHistory;
import cn.hd01.repository.entity.WXOauth;
import cn.hd01.service.UserService;
import cn.hd01.service.ViewHistoryService;
import cn.hd01.web.auth.Auth;
import cn.hd01.web.auth.AuthType;
import cn.hd01.web.util.WebHelper;

@Controller
@RequestMapping("/weixin/history")
@Auth(AuthType.WECHAT)
public class WXViewController {
	@Autowired
	private ViewHistoryService vhService;
	
	@Autowired
	private UserService userService;

	@RequestMapping(path = { "/", "" }, method = RequestMethod.GET)
	public String index(Model model) {
		return "weixin/history";
	}

	@ResponseBody
	@RequestMapping(path = { "/list" }, method = RequestMethod.GET)
	public Page<ViewHistory> List(@PageableDefault Pageable pageable) {
		WXOauth oauth = WebHelper.currentWXOauth();
		User user = userService.findByOauthId(oauth.getId());

		return vhService.findByUserId(user.getId(), pageable);
	}

}
