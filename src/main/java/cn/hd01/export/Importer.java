package cn.hd01.export;

import java.io.InputStream;

public interface Importer {
	public <T> void importer(InputStream is, RowProccess<T> proccess) throws Exception;
}
