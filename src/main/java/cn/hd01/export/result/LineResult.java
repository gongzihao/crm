package cn.hd01.export.result;

public class LineResult {
	private int index;
	private boolean status;
	private String detail;

	public LineResult() {
	}

	public LineResult(int index, boolean status) {
		this.index = index;
		this.status = status;
	}

	public LineResult(int index, boolean status, String detail) {
		this.index = index;
		this.status = status;
		this.detail = detail;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}
}
