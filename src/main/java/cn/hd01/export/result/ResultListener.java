package cn.hd01.export.result;

import cn.hd01.export.listener.RowProccessListener;

public class ResultListener implements RowProccessListener {
	private ImportResult result = new ImportResult();

	public ResultListener() {
	}

	@Override
	public void onSuccess(int index) {
		result.setIndex(index);
		result.addDetail(new LineResult(index, true));
	}

	@Override
	public void onFailure(int index, Exception e) {
		e.printStackTrace();
		result.setIndex(index);
		result.addDetail(new LineResult(index, false, e.getMessage()));
	}

	public ImportResult getResult() {
		return result;
	}

}
