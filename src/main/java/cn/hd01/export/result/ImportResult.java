package cn.hd01.export.result;

import java.util.ArrayList;
import java.util.List;

public class ImportResult {
	private int total = -1;
	private int index;
	private List<LineResult> detail = new ArrayList<LineResult>();

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public List<LineResult> getDetail() {
		return detail;
	}

	public void addDetail(LineResult line) {
		detail.add(line);
	}

}
