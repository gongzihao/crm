package cn.hd01.export.listener;

public interface RowProccessListener {
	public void onSuccess(int index);

	public void onFailure(int index, Exception e);
}
