package cn.hd01.web.entity;

import org.springframework.beans.BeanUtils;
import org.springframework.util.StringUtils;

import cn.hd01.repository.entity.StaffFeedback;

public class StaffFeedbackModel extends StaffFeedback {
	private String[] features;
	private String[] focuses;

	public String[] getFeatures() {
		return features;
	}

	public void setFeatures(String[] features) {
		this.features = features;
		setFeature(StringUtils.arrayToCommaDelimitedString(features));
	}

	public String[] getFocuses() {
		return focuses;
	}

	public void setFocuses(String[] focuses) {
		this.focuses = focuses;
		setFocus(StringUtils.arrayToCommaDelimitedString(focuses));
	}
	
	public StaffFeedback toStaffFeedback(){
		StaffFeedback sf = new StaffFeedback();
		BeanUtils.copyProperties(this, sf);
		return sf;
	}

}
