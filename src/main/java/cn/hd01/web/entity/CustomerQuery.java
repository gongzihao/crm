package cn.hd01.web.entity;

import java.util.Date;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.format.annotation.DateTimeFormat;

import cn.hd01.repository.entity.Customer;

@NoRepositoryBean
public class CustomerQuery extends Customer {
	@DateTimeFormat(pattern = "yyyyMMddHHmmss")
	private Date beginCreateTime;

	@DateTimeFormat(pattern = "yyyyMMddHHmmss")
	private Date endCreateTime;

	public Date getBeginCreateTime() {
		return beginCreateTime;
	}

	public void setBeginCreateTime(Date beginCreateTime) {
		this.beginCreateTime = beginCreateTime;
	}

	public Date getEndCreateTime() {
		return endCreateTime;
	}

	public void setEndCreateTime(Date endCreateTime) {
		this.endCreateTime = endCreateTime;
	}
}
