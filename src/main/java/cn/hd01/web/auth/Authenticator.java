package cn.hd01.web.auth;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Authenticator {
	public boolean authenticate(HttpServletRequest request, HttpServletResponse response) throws Exception;

	public AuthType type();
}
