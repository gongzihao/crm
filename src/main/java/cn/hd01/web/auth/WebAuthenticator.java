package cn.hd01.web.auth;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import cn.hd01.web.util.WebConstant;
import cn.hd01.web.util.WebHelper;

@Component
public class WebAuthenticator implements Authenticator {

	@Override
	public boolean authenticate(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if (WebHelper.currentUser() != null) {
			return true;
		}

		response.setStatus(HttpStatus.UNAUTHORIZED.value());
		response.sendRedirect(WebHelper.basePath() + WebConstant.LOGIN_URL);
		return false;
	}

	@Override
	public AuthType type() {
		return AuthType.WEB;
	}
}
