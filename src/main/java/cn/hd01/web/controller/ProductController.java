package cn.hd01.web.controller;

import cn.hd01.repository.entity.Product;
import cn.hd01.repository.entity.User;
import cn.hd01.service.DictionaryService;
import cn.hd01.service.ProductService;
import cn.hd01.service.UploadService;
import cn.hd01.util.DictionaryType;
import cn.hd01.web.auth.Auth;
import cn.hd01.web.util.WebException;
import cn.hd01.web.util.WebHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * 产品controller
 *
 * @author cyb
 * @date 2017/5/5.
 */
@Controller
@RequestMapping("/product")
@Auth
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private DictionaryService dictionaryService;

    @Autowired
    private UploadService uploadService;

    @RequestMapping(path = {"/", ""}, method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("types", dictionaryService.findByType(DictionaryType.PRODUCT_TYPE));
        return "product/product";
    }

    @ResponseBody
    @RequestMapping(path = "/list", method = RequestMethod.GET)
    public Page<Product> list(Product product, @PageableDefault Pageable pageable) {
        return productService.findAll(product, pageable);
    }

    @RequestMapping(path = "/view", method = RequestMethod.GET)
    public String view(@RequestParam(value = "id", defaultValue = "0")Integer id, Model model) {
        model.addAttribute("types", dictionaryService.findByType(DictionaryType.PRODUCT_TYPE));
        model.addAttribute("p", productService.findOne(id));
        return "product/view";
    }

    @ResponseBody
    @RequestMapping(path = "/delete/{id}", method = RequestMethod.GET)
    public void delete(@PathVariable("id") Integer id){
        productService.delete(id);

        // 删除管理图片
        uploadService.deleteByTypeAndRefId("product", id);
    }

    @ResponseBody
    @RequestMapping(path = "/save", method = RequestMethod.POST)
    public int save(Product product) {
        User user = WebHelper.currentUser();

        product.setAuthor(user.getName());
        product.setAuthorId(user.getId());

        if (product.getHot() == null){
            product.setHot(false);
        }
        if (product.getRecommend() == null){
            product.setRecommend(false);
        }

        Product p = productService.save(product);
        if (p == null) {
            throw new WebException(HttpStatus.BAD_REQUEST, "保存失败");
        }
        return p.getId();
    }

    /**
     * 设置产品封面
     */
    @ResponseBody
    @RequestMapping(path = "/setCoverPic/{id}", method = RequestMethod.GET)
    public int setCoverPic(@PathVariable("id") Integer id, Integer picId){
        productService.updateCoverPic(id, picId);

        return picId;
    }

    /**
     * 设置产品热门信息成功
     */
    @ResponseBody
    @RequestMapping(path = "/hot/{id}", method = RequestMethod.GET)
    public void hot(@PathVariable("id") Integer id){
        Product product = productService.getOne(id);

        productService.updateHot(product.getId(), !product.getHot());
    }

    /**
     * 设置产品推荐信息成功
     */
    @ResponseBody
    @RequestMapping(path = "/recommend/{id}", method = RequestMethod.GET)
    public void recommend(@PathVariable("id") Integer id){
        Product product = productService.getOne(id);

        productService.updateRecommend(product.getId(), !product.getRecommend());
    }

}
