package cn.hd01.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.hd01.repository.entity.Dictionary;
import cn.hd01.service.DictionaryService;
import cn.hd01.util.DictionaryType;
import cn.hd01.web.auth.Auth;

@Controller
@RequestMapping("/dict")
@Auth
public class DictController {
	@Autowired
	private DictionaryService dictService;

	@RequestMapping(path = "/", method = RequestMethod.GET)
	public String index(Model m) {
		m.addAttribute("types", DictionaryType.values());
		return "dict";
	}

	@ResponseBody
	@RequestMapping(path = "/list", method = RequestMethod.GET)
	private Page<Dictionary> find(Dictionary dict, @PageableDefault Pageable pageable) {
		return dictService.find(dict, pageable);
	}

	@ResponseBody
	@RequestMapping(path = "/delete/{id}", method = RequestMethod.GET)
	private void delete(@PathVariable("id") Integer id) {
		dictService.delete(id);
	}

	@ResponseBody
	@RequestMapping(path = "/save", method = RequestMethod.POST)
	private void save(Dictionary dict) {
		dictService.save(dict);
	}
}
