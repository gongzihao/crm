package cn.hd01.web.controller;

import java.io.File;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import cn.hd01.export.ImporterDelegate;
import cn.hd01.export.result.ImportResult;
import cn.hd01.export.result.ResultListener;
import cn.hd01.repository.entity.Customer;
import cn.hd01.service.CustomerService;
import cn.hd01.service.StoreService;
import cn.hd01.util.mapper.CustomerMapper;
import cn.hd01.web.auth.Auth;
import cn.hd01.web.entity.CustomerQuery;
import cn.hd01.web.util.WebException;

import com.google.common.io.Files;

@Controller
@RequestMapping("/customer")
@Auth
public class CustomerController {
	@Autowired
	private ImporterDelegate importer;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private StoreService storeService;

	@RequestMapping(path = "/", method = RequestMethod.GET)
	public String index(Model m) {
		m.addAttribute("stores", storeService.findAll());
		return "customer";
	}

	@RequestMapping(path = "/detail/{id}", method = RequestMethod.GET)
	public String info(Model m, @PathVariable("id") Integer id) {
		m.addAttribute("stores", storeService.findAll());
		m.addAttribute("customer", customerService.findOne(id));
		return "customerdetail";
	}

	@RequestMapping(path = "/add", method = RequestMethod.GET)
	public String add(Model m) {
		m.addAttribute("stores", storeService.findAll());
		return "addCustomer";
	}

	@ResponseBody
	@RequestMapping(path = "/list", method = RequestMethod.GET)
	private Page<Customer> find(CustomerQuery query, @PageableDefault Pageable pageable) {
		return customerService.find(query, pageable);
	}

	@ResponseBody
	@RequestMapping(path = "/save", method = RequestMethod.POST)
	private Customer save(Customer customer) {
		return customerService.save(customer);
	}

	@ResponseBody
	@RequestMapping(path = "/save/basic", method = RequestMethod.POST)
	private Customer saveBasic(Customer customer) {
		Customer tmp = customerService.findOne(customer.getId());
		tmp.setName(customer.getName());
		tmp.setPhone(customer.getPhone());
		tmp.setWeixin(customer.getWeixin());
		tmp.setAddress(customer.getAddress());
		tmp.setStore(customer.getStore());
		tmp.setSource(customer.getSource());
		tmp.setComment(customer.getComment());
		return customerService.save(tmp);
	}

	@ResponseBody
	@RequestMapping(path = "/save/child", method = RequestMethod.POST)
	private Customer saveChild(Customer customer) {
		Customer tmp = customerService.findOne(customer.getId());
		tmp.setBirthday(customer.getBirthday());
		tmp.setSex(customer.getSex());
		tmp.setHospital(customer.getHospital());
		return customerService.save(tmp);
	}

	@ResponseBody
	@RequestMapping(path = "/save/ext", method = RequestMethod.POST)
	private Customer saveExt(Customer customer) {
		Customer tmp = customerService.findOne(customer.getId());
		tmp.setHealthScore(customer.getHealthScore());
		tmp.setConsumption(customer.getConsumption());
		return customerService.save(tmp);
	}

	@ResponseBody
	@RequestMapping(path = "/upload", method = RequestMethod.POST)
	public ImportResult batchupload(@RequestParam("file") MultipartFile file, HttpSession session) {
		try {
			File dest = new File(Files.createTempDir(), file.getOriginalFilename());
			System.err.println(dest.getAbsolutePath());
			file.transferTo(dest);

			ResultListener result = new ResultListener();
			CustomerMapper mapper = new CustomerMapper(new ResultListener());
			mapper.setRepository(customerService);
			importer.asyncImporter(dest, mapper);
			return result.getResult();
		} catch (Exception e) {
			throw new WebException(HttpStatus.INTERNAL_SERVER_ERROR, e);
		}
	}
}
