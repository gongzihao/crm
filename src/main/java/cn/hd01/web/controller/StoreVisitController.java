package cn.hd01.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.hd01.repository.entity.Staff;
import cn.hd01.repository.entity.StoreVisit;
import cn.hd01.service.CustomerService;
import cn.hd01.service.StaffService;
import cn.hd01.service.StoreVisitService;
import cn.hd01.web.auth.Auth;
import cn.hd01.web.util.WebHelper;

@Auth
@Controller
@RequestMapping("/visit")
public class StoreVisitController {

	@Autowired
	private StoreVisitService visitSerivce;

	@Autowired
	private StaffService wxService;

	@Autowired
	private CustomerService customerService;

	@RequestMapping(path = "/{customerId}", method = RequestMethod.GET)
	public String index(@PathVariable("customerId") Integer customerId, Model m) {
		m.addAttribute("staffs", wxService.findByType("调理师"));
		m.addAttribute("customer", customerService.findOne(customerId));
		m.addAttribute("menu", "/customer/");
		return "telreserve";

	}

	@ResponseBody
	@RequestMapping(path = "/save", method = RequestMethod.POST)
	public void index(StoreVisit entity) {
		Staff staff = wxService.getOne(entity.getStaffId());
		entity.setStaffName(staff.getName());

		Staff current = wxService.findByOauthId(WebHelper.currentWXOauth().getId());
		entity.setCreateBy(current.getId());

		visitSerivce.save(entity);
	}

	@ResponseBody
	@RequestMapping(path = "/{customerId}/list", method = RequestMethod.GET)
	private Page<StoreVisit> find(@PathVariable("customerId") Integer customerId, @PageableDefault Pageable pageable) {
		return visitSerivce.findAll(customerId, pageable);
	}

}
