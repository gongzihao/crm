package cn.hd01.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.hd01.repository.entity.StoreReservation;
import cn.hd01.service.CustomerService;
import cn.hd01.service.StoreReserveService;
import cn.hd01.web.auth.Auth;

@Auth
@Controller
@RequestMapping("/storereserve")
public class StoreReserveController {

	@Autowired
	private StoreReserveService storeSerivce;

	@Autowired
	private CustomerService customerService;

	@RequestMapping(path = "/{customerId}", method = RequestMethod.GET)
	public String index(@PathVariable("customerId") Integer customerId, Model m) {
		m.addAttribute("customer", customerService.findOne(customerId));
		m.addAttribute("menu", "/customer/");
		return "storereserve";
	}

	@ResponseBody
	@RequestMapping(path = "/save", method = RequestMethod.POST)
	public void index(StoreReservation entity) {
		if (!entity.getAgree()) {
			entity.setReserveTime(null);
		}
		storeSerivce.save(entity);
	}

	@ResponseBody
	@RequestMapping(path = "/{customerId}/list", method = RequestMethod.GET)
	private Page<StoreReservation> find(@PathVariable("customerId") Integer customerId,
			@PageableDefault Pageable pageable) {
		return storeSerivce.findAll(customerId, pageable);
	}

}
