package cn.hd01.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.hd01.repository.entity.CustomerFeedBack;
import cn.hd01.repository.entity.Staff;
import cn.hd01.repository.entity.StaffFeedback;
import cn.hd01.repository.entity.TelReservation;
import cn.hd01.service.CustomerService;
import cn.hd01.service.StaffService;
import cn.hd01.service.TelReserveService;
import cn.hd01.web.auth.Auth;

@Auth
@Controller
@RequestMapping("/telreserve")
public class TelReserveController {

	@Autowired
	private TelReserveService telSerivce;

	@Autowired
	private StaffService wxService;

	@Autowired
	private CustomerService customerService;

	@RequestMapping(path = "/{customerId}", method = RequestMethod.GET)
	public String index(@PathVariable("customerId") Integer customerId, Model m) {
		m.addAttribute("staffs", wxService.findByType("催乳师"));
		m.addAttribute("customer", customerService.findOne(customerId));
		m.addAttribute("menu", "/customer/");
		return "telreserve";

	}

	@ResponseBody
	@RequestMapping(path = "/save", method = RequestMethod.POST)
	public void index(TelReservation entity) {
		if (entity.getAgree()) {
			Staff staff = wxService.getOne(entity.getStaffId());
			entity.setStaffName(staff.getName());
			entity.setStaffPhone(staff.getPhone());
			entity.setState(0);
		} else {
			entity.setState(null);
			entity.setStaffId(null);
			entity.setReserveTime(null);
		}

		telSerivce.save(entity);
	}

	@ResponseBody
	@RequestMapping(path = "/{customerId}/list", method = RequestMethod.GET)
	private Page<TelReservation> find(@PathVariable("customerId") Integer customerId, @PageableDefault Pageable pageable) {
		return telSerivce.findAll(customerId, pageable);
	}

	@ResponseBody
	@RequestMapping(path = "/customerFeedback/{id}", method = RequestMethod.GET)
	private CustomerFeedBack customerFeedback(@PathVariable("id") Integer customerId) {
		return telSerivce.customerFeedBack(customerId);
	}

	@ResponseBody
	@RequestMapping(path = "/staffFeedback/{id}", method = RequestMethod.GET)
	private StaffFeedback staffFeedback(@PathVariable("id") Integer id) {
		return telSerivce.staffFeedback(id);
	}

}
