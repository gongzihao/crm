package cn.hd01.web.controller;

import cn.hd01.repository.entity.Style;
import cn.hd01.repository.entity.User;
import cn.hd01.service.DictionaryService;
import cn.hd01.service.StyleService;
import cn.hd01.util.DictionaryType;
import cn.hd01.web.auth.Auth;
import cn.hd01.web.util.WebException;
import cn.hd01.web.util.WebHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * 方案controller
 *
 * @author cyb
 * @date 2017/5/19.
 */
@Controller
@RequestMapping("/style")
@Auth
public class StyleController {

    @Autowired
    private StyleService styleService;

    @Autowired
    private DictionaryService dictionaryService;


    @RequestMapping(path = {"/", ""}, method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("types", dictionaryService.findByType(DictionaryType.PRODUCT_TYPE));
        model.addAttribute("styles", dictionaryService.findByType(DictionaryType.STYLE));
        return "style/style";
    }

    @ResponseBody
    @RequestMapping(path = "/list", method = RequestMethod.GET)
    public Page<Style> list(Style style, @PageableDefault Pageable pageable) {
        return styleService.findAll(style, pageable);
    }

    @RequestMapping(path = "/view", method = RequestMethod.GET)
    public String view(@RequestParam(value = "id", defaultValue = "0") Integer id, Model model) {
        model.addAttribute("types", dictionaryService.findByType(DictionaryType.PRODUCT_TYPE));
        model.addAttribute("styles", dictionaryService.findByType(DictionaryType.STYLE));
        model.addAttribute("s", styleService.findOne(id));
        return "style/view";
    }

    @ResponseBody
    @RequestMapping(path = "/save", method = RequestMethod.POST)
    public int save(Style style) {
        User user = WebHelper.currentUser();

        style.setAuthor(user.getName());
        style.setAuthorId(user.getId());

        if (style.getHot() == null){
            style.setHot(false);
        }
        if (style.getRecommend() == null){
            style.setRecommend(false);
        }

        Style s = styleService.save(style);
        if (s == null) {
            throw new WebException(HttpStatus.BAD_REQUEST, "方案保存失败");
        }

        return s.getId();
    }

    /**
     * 设置方案封面
     */
    @ResponseBody
    @RequestMapping(path = "/setCoverPic/{id}", method = RequestMethod.GET)
    public int setCoverPic(@PathVariable("id") Integer id, Integer picId){
        styleService.updateCoverPic(id, picId);

        return picId;
    }

    /**
     * 设置产品热门信息成功
     */
    @ResponseBody
    @RequestMapping(path = "/hot/{id}", method = RequestMethod.GET)
    public void hot(@PathVariable("id") Integer id){
        Style style = styleService.getOne(id);

        styleService.updateHot(style.getId(), !style.getHot());
    }

    /**
     * 设置产品推荐信息成功
     */
    @ResponseBody
    @RequestMapping(path = "/recommend/{id}", method = RequestMethod.GET)
    public void recommend(@PathVariable("id") Integer id){
        Style style = styleService.getOne(id);

        styleService.updateRecommend(style.getId(), !style.getRecommend());
    }

}
