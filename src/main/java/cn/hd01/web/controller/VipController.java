package cn.hd01.web.controller;

import cn.hd01.repository.entity.VipLevel;
import cn.hd01.service.VipService;
import cn.hd01.web.auth.Auth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 会员等级
 */
@Controller
@RequestMapping("/vip")
@Auth
public class VipController {
    @Autowired
    private VipService vipService;

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public String index(Model model) {
        return "vip";
    }

    @ResponseBody
    @RequestMapping(path = "/list", method = RequestMethod.GET)
    public Page<VipLevel> list(@PageableDefault Pageable pageable) {
        return vipService.findAll(pageable);
    }

    @ResponseBody
    @RequestMapping(path = "/delete/{id}", method = RequestMethod.GET)
    public void delete(@PathVariable("id") Integer id) {
        vipService.delete(id);
    }

    @ResponseBody
    @RequestMapping(path = "/save", method = RequestMethod.POST)
    public boolean save(VipLevel vipLevel) {
        return vipService.save(vipLevel) != null;
    }
}
