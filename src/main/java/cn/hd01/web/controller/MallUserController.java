//package cn.hd01.web.controller;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;
//import org.springframework.data.web.PageableDefault;
//import org.springframework.http.HttpStatus;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import sun.misc.Request;
//
//import cn.hd01.repository.entity.MallUser;
//import cn.hd01.service.DictionaryService;
//import cn.hd01.service.MallUserService;
//import cn.hd01.util.DictionaryType;
//import cn.hd01.web.auth.Auth;
//import cn.hd01.web.util.WebConstant;
//import cn.hd01.web.util.WebException;
//
///**
// * 商城用户
// *
// * @author cyb
// * @date 2017/5/3.
// */
//@Controller
//@RequestMapping("/mallUser")
//@Auth
//public class MallUserController {
//
//    @Autowired
//    private DictionaryService dictionaryService;
//
//    @Autowired
//    private MallUserService mallUserService;
//
//    @RequestMapping(method = RequestMethod.GET)
//    public String index(Model model) {
//        model.addAttribute("userTypes", dictionaryService.findByType(DictionaryType.USER_TYPE));
//        return "mallUser";
//    }
//
//    @ResponseBody
//    @RequestMapping(path = "/list", method = RequestMethod.GET)
//    public Page<MallUser> list(MallUser user, @PageableDefault Pageable pageable) {
//        return mallUserService.findAll(user, pageable);
//    }
//
//    @ResponseBody
//    @RequestMapping(path = "/delete/{id}", method = RequestMethod.GET)
//    public void delete(@PathVariable("id") Integer id) {
//        mallUserService.delete(id);
//    }
//
//    @ResponseBody
//    @RequestMapping(path = "/save", method = RequestMethod.POST)
//    public void save(MallUser user){
//        mallUserService.modify(user);
//    }
//
//    @ResponseBody
//    @RequestMapping(path = "/resetPwd/{id}", method = RequestMethod.GET)
//    public String resetPwd(@PathVariable("id") Integer id) {
//        if (!mallUserService.exists(id)) {
//            throw new WebException(HttpStatus.NOT_ACCEPTABLE, "不存在改用户");
//        }
//
//        mallUserService.updatePassword(id, WebConstant.DEFAULT_PASSWORD);
//        return WebConstant.DEFAULT_PASSWORD;
//    }
//}
