package cn.hd01.web.controller;

import cn.hd01.repository.entity.User;
import cn.hd01.service.DictionaryService;
import cn.hd01.service.RoleService;
import cn.hd01.service.UserService;
import cn.hd01.util.DictionaryType;
import cn.hd01.web.auth.Auth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author cyb
 * @date 2017/5/19.
 */
@Controller
@RequestMapping("/client")
@Auth
public class ClientController {

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserService userService;

    @Autowired
    private DictionaryService dictionaryService;

    @RequestMapping(path = {"/", ""}, method = RequestMethod.GET)
    public String index(Model m) {
        m.addAttribute("roles", roleService.findAll());
        m.addAttribute("userTypes", dictionaryService.findByType(DictionaryType.USER_TYPE));
        m.addAttribute("majors", dictionaryService.findByType(DictionaryType.MAJOR));
        return "client/client";
    }

    @ResponseBody
    @RequestMapping(path = "/list", method = RequestMethod.GET)
    private Page<User> find(User u, @PageableDefault Pageable pageable) {
        return userService.findClient(u, pageable);
    }
}
