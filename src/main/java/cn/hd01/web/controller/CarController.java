package cn.hd01.web.controller;

import cn.hd01.repository.entity.Car;
import cn.hd01.repository.entity.User;
import cn.hd01.service.CarService;
import cn.hd01.web.auth.Auth;
import cn.hd01.web.util.WebHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 后台查看购物车
 *
 * @author cyb
 * @date 2017/5/31.
 */
@Controller
@RequestMapping("/car")
@Auth
public class CarController {

    @Autowired
    private CarService carService;

    @RequestMapping(path = {"/", ""}, method = RequestMethod.GET)
    public String index(Model model) {
        return "car/car";
    }

    @ResponseBody
    @RequestMapping(path = "/list", method = RequestMethod.GET)
    public Page<Car> list(Car car, @PageableDefault Pageable pageable) {
        User user = WebHelper.currentUser();
        car.setAuthorId(user.getId());

        return carService.findAll(car, pageable);
    }

    @ResponseBody
    @RequestMapping(path = "/delete/{id}", method = RequestMethod.GET)
    public void delete(@PathVariable("id") Integer id) {
        carService.delete(id);
    }
}
