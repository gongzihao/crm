package cn.hd01.web.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.hd01.repository.entity.RolePermissions;
import cn.hd01.service.PermissionsService;
import cn.hd01.web.auth.Auth;

@Controller
@RequestMapping("/permissions")
@Auth
public class PermissionsController {

	@Autowired
	private PermissionsService permissionsService;

	@ResponseBody
	@RequestMapping(path = "/list/{id}", method = RequestMethod.GET)
	public List<RolePermissions> permissions(@PathVariable("id") int id) {
		return permissionsService.findPermissions(id);
	}

	@ResponseBody
	@RequestMapping(path = "/save/{id}", method = RequestMethod.POST)
	public void save(@PathVariable("id") int id, @RequestParam("pIds[]") Integer[] pIds) {
		permissionsService.save(id, Arrays.asList(pIds));
	}

}
