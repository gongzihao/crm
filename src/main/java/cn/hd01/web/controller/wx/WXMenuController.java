package cn.hd01.web.controller.wx;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.sd4324530.fastweixin.api.MenuAPI;
import com.github.sd4324530.fastweixin.api.config.ApiConfig;
import com.github.sd4324530.fastweixin.api.entity.Menu;
import com.github.sd4324530.fastweixin.api.entity.MenuButton;
import com.github.sd4324530.fastweixin.api.enums.MenuType;
import com.github.sd4324530.fastweixin.api.enums.ResultType;
import com.github.sd4324530.fastweixin.api.response.GetMenuResponse;

import cn.hd01.service.WXAppService;
import cn.hd01.web.auth.Auth;
import cn.hd01.web.util.WebException;

@Controller
@RequestMapping("/wxmenu")
@Auth
public class WXMenuController {
	private static final Logger logger = LoggerFactory.getLogger(WXMenuController.class);

	@Autowired
	private WXAppService wxappService;

	@RequestMapping(method = RequestMethod.GET)
	public String index(Model m, String domain) {
		m.addAttribute("domain", domain);
		return "wxmenu";
	}

	@ResponseBody
	@RequestMapping(path = "/query/{domain}", method = RequestMethod.GET)
	private List<MenuButton> find(@PathVariable String domain) {
		ApiConfig apiConfig = wxappService.getApiConfig(wxappService.findByDomain(domain));
		GetMenuResponse response = new MenuAPI(apiConfig).getMenu();
		if (StringUtils.hasText(response.getErrcode()) && !"0".equals(response.getErrcode())) {
			throw new WebException(HttpStatus.BAD_REQUEST, response.getErrmsg());
		}

		return response.getMenu().getButton();
	}

	@RequestMapping(path = "/create", method = RequestMethod.POST)
	@ResponseBody
	private void create(HttpServletRequest request, String domain) {
		ArrayList<MenuButton> buttons = new ArrayList<MenuButton>();
		for (int i = 0; i < 3; i++) {
			String name = request.getParameter("menu" + i + ".name");
			String url = request.getParameter("menu" + i + ".url");
			if (!StringUtils.hasText(name)) {
				continue;
			}

			MenuButton button = new MenuButton();
			button.setName(name);

			ArrayList<MenuButton> list = new ArrayList<MenuButton>();
			for (int j = 0; j < 5; j++) {
				String subName = request.getParameter("menu" + i + ".sub" + j + ".name");
				String subUrl = request.getParameter("menu" + i + ".sub" + j + ".url");

				if (StringUtils.hasText(subName) && StringUtils.hasText(subUrl)) {
					MenuButton subButton = new MenuButton();
					subButton.setType(MenuType.VIEW);
					subButton.setName(subName);
					subButton.setUrl(subUrl);
					list.add(subButton);
				}
			}

			if (list.isEmpty() && StringUtils.hasText(url)) {
				button.setType(MenuType.VIEW);
				button.setUrl(url);
			}

			if (!list.isEmpty()) {
				button.setSubButton(list);
			}

			buttons.add(button);
		}

		ApiConfig apiConfig = wxappService.getApiConfig(wxappService.findByDomain(domain));
		MenuAPI menuAPI = new MenuAPI(apiConfig);

		Menu menu = new Menu();
		menu.setButton(buttons);
		logger.info(menu.toJsonString());
		
		ResultType result = menuAPI.createMenu(menu);
		if (result.getCode() != 0) {
			throw new WebException(HttpStatus.BAD_REQUEST, result.getDescription());
		}
	}

}
