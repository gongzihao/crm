package cn.hd01.web.util;

import org.springframework.http.HttpStatus;

@SuppressWarnings("serial")
public class WebException extends RuntimeException {

	private HttpStatus status;

	public WebException(HttpStatus status) {
		super();
		this.status = status;
	}

	public WebException(HttpStatus status, String message) {
		super(message);
		this.status = status;
	}

	public WebException(HttpStatus status, String message, Throwable cause) {
		super(message, cause);
		this.status = status;
	}

	public WebException(HttpStatus status, Throwable cause) {
		super(cause);
		this.status = status;
	}

	public HttpStatus getStatus() {
		return status;
	}
}
