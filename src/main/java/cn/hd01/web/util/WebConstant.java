package cn.hd01.web.util;

public interface WebConstant {
	public String LOGIN_URL = "/login";

	public String USER_SESSION_NAME = "_CURRENT_USER_";

	public String DEFAULT_PASSWORD = "666666";

	public String REDIRECT_URL = "REDIRECT_URL";
}
