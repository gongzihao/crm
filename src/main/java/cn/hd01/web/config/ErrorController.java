package cn.hd01.web.config;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.autoconfigure.web.BasicErrorController;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorViewResolver;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.hd01.web.auth.Auth;
import cn.hd01.web.auth.AuthType;

import com.alibaba.fastjson.JSON;

@Controller
@RequestMapping("${server.error.path:${error.path:/error}}")
@Auth(AuthType.OFF)
public class ErrorController extends BasicErrorController {

	public ErrorController(ErrorAttributes errorAttributes, ServerProperties serverProperties,
			List<ErrorViewResolver> errorViewResolvers) {
		super(errorAttributes, serverProperties.getError(), errorViewResolvers);
	}

	@Override
	@RequestMapping(produces = "text/html")
	public ModelAndView errorHtml(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView modelAndView = super.errorHtml(request, response);
		System.err.println(JSON.toJSONString(modelAndView.getModel()));
		return modelAndView;
	}

	@Override
	@RequestMapping
	@ResponseBody
	public ResponseEntity<Map<String, Object>> error(HttpServletRequest request) {
		ResponseEntity<Map<String, Object>> en = super.error(request);
		System.err.println(JSON.toJSONString(en.getBody()));
		return en;
	}

}
