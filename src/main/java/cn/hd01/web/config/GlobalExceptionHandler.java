package cn.hd01.web.config;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import cn.hd01.web.util.WebException;

@ControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(value = WebException.class)
	public void handleWebException(WebException ex, HttpServletResponse response) throws IOException {
		response.sendError(ex.getStatus().value(), ex.getMessage());
		return;
	}

	@ExceptionHandler(value = Exception.class)
	public void handleException(Exception ex, HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage());
		return;
	}
}
