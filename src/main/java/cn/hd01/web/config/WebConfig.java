package cn.hd01.web.config;

import javax.servlet.ServletRequestListener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import cn.hd01.web.auth.AuthDelegate;

@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

	@Autowired
	private AuthDelegate delegate;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new AuthInterceptor(delegate));
	}

	@Bean
	public ServletListenerRegistrationBean<ServletRequestListener> getDemoListener() {
		return new ServletListenerRegistrationBean<ServletRequestListener>(new RequestContextListener());
	}

}
