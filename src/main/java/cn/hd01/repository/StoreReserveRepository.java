package cn.hd01.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cn.hd01.repository.entity.StoreReservation;

@Repository
public interface StoreReserveRepository extends JpaRepository<StoreReservation, Integer> {

	List<StoreReservation> findByCustomerId(Integer customerId);

	Page<StoreReservation> findAll(Specification<StoreReservation> spec, Pageable pageable);

}
