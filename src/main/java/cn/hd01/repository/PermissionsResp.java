package cn.hd01.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import cn.hd01.repository.entity.Permissions;

@Repository
public interface PermissionsResp extends JpaSpecificationExecutor<Permissions>, JpaRepository<Permissions, Integer> {

}
