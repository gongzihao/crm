package cn.hd01.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cn.hd01.repository.entity.WXApp;

@Repository
public interface WXAppRepository extends JpaRepository<WXApp, Integer> {
	WXApp findByWeixinId(String weixinId);

	Page<WXApp> findAll(Specification<WXApp> spec, Pageable pageable);

	WXApp findByDomain(String domain);

	WXApp findByMchId(String mchId);
}
