package cn.hd01.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cn.hd01.repository.entity.CustomerFeedBack;

@Repository
public interface CustomerFeedBackRepository extends JpaRepository<CustomerFeedBack, Integer> {

	Page<CustomerFeedBack> findAll(Specification<CustomerFeedBack> spec, Pageable pageable);

	CustomerFeedBack findByReservationId(Integer reservationId);
}
