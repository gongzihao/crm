package cn.hd01.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import cn.hd01.repository.entity.Product;

import javax.transaction.Transactional;

/**
 * 产品 repository
 *
 * @author cyb
 * @date 2017/5/5.
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

    Page<Product> findAll(Specification<Product> spec, Pageable pageable);

    @Modifying
    @Transactional
    @Query("update Product p set p.coverPic=?1 where p.id=?2")
    int updateCoverPic(Integer picId, Integer id);

    @Modifying
    @Transactional
    @Query("update Product p set p.hot=?2 where p.id=?1")
    void updateHot(Integer id, Boolean hot);

    @Modifying
    @Transactional
    @Query("update Product p set p.recommend=?2 where p.id=?1")
    void updateRecommend(Integer id, Boolean recommend);
}
