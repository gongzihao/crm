package cn.hd01.repository;

import cn.hd01.repository.entity.Flow;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author cyb
 * @date 2017/5/23.
 */
@Repository
public interface FlowRepository extends JpaRepository<Flow, Long> {

    List<Flow> findByTypeAndRefIdOrderByCreateTime(String type, Integer refId);

    Flow findFirstByTypeAndRefIdOrderByCreateTimeDesc(String type, Integer refId);
}
