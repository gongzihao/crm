package cn.hd01.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cn.hd01.repository.entity.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
	Page<Role> findByNameContaining(String name, Pageable pageable);

	int countByName(String name);
}
