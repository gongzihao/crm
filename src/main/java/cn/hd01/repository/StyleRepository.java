package cn.hd01.repository;

import cn.hd01.repository.entity.Style;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * 方案 repository
 *
 * @author cyb
 * @date 2017/5/22.
 */
@Repository
public interface StyleRepository extends JpaRepository<Style, Integer> {

    Page<Style> findAll(Specification<Style> spec, Pageable pageable);

    @Modifying
    @Transactional
    @Query("update Style s set s.coverPic=?1 where s.id=?2")
    int updateCoverPic(Integer picId, Integer id);

    @Modifying
    @Transactional
    @Query("update Style s set s.hot=?2 where s.id=?1")
    void updateHot(Integer id, Boolean hot);

    @Modifying
    @Transactional
    @Query("update Style s set s.recommend=?2 where s.id=?1")
    void updateRecommend(Integer id, Boolean recommend);
}
