package cn.hd01.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import cn.hd01.repository.entity.StoreVisit;

@Repository
public interface StoreVisitRepository extends JpaRepository<StoreVisit, Integer> {
	Page<StoreVisit> findAll(Specification<StoreVisit> spec, Pageable pageable);

	@Query("select u from StoreVisit u where u.staffId = ?1 and u.checkData != 7")
	List<StoreVisit> findByStaffId(Integer staffId);

	@Query("select u from StoreVisit u where u.createBy = ?1 and u.isFeedback = false")
	List<StoreVisit> findByCreateBy(Integer staffId);

	@Modifying
	@Query("update StoreVisit set checkData = checkData + ?1 where id = ?2")
	int updateCheckData(Integer type, Integer id);

	@Modifying
	@Query("update StoreVisit set isFeedback=true where id = ?1")
	int isFeedback(Integer id);

	@Modifying
	@Query("update StoreVisit set isInvestigate=true where id = ?1")
	int isInvestigate(Integer id);

}
