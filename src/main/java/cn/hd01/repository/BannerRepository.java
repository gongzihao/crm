package cn.hd01.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import cn.hd01.repository.entity.Banner;

public interface BannerRepository extends JpaRepository<Banner, Integer> {
	Page<Banner> findByType(Integer type, Pageable pageable);
	
	List<Banner> findByTypeOrderBySeqAsc(Integer type);

	Banner findByTypeAndRefId(Integer type, Integer refId);
}
