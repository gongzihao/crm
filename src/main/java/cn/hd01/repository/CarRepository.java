package cn.hd01.repository;

import cn.hd01.repository.entity.Car;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author cyb
 * @date 2017/5/31.
 */
@Repository
public interface CarRepository extends JpaRepository<Car, Integer>{

    Page<Car> findAll(Specification<Car> car, Pageable pageable);
}
