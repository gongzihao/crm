package cn.hd01.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cn.hd01.repository.entity.StaffFeedback;

@Repository
public interface StaffFeedBackRepository extends JpaRepository<StaffFeedback, Integer> {

	Page<StaffFeedback> findAll(Specification<StaffFeedback> spec, Pageable pageable);

	StaffFeedback findByReservationId(Integer reservationId);
}
