package cn.hd01.repository;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.hd01.repository.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

	@Query(value = "select * from sys_user u where (u.name=:name or u.mobile=:name) and u.password=:password limit 1", nativeQuery = true)
	public User findFirstByNameOrMobileAndPassword(@Param("name") String name, @Param("password") String password);

	public int countByName(String name);

	@Modifying
	@Transactional
	@Query("update User set password=?2 where id=?1")
	public int updatePwd(Integer id, String password);

	Page<User> findAll(Specification<User> spec, Pageable pageable);

	/**
	 * 每个微信用户 只能注册一个账户
	 * @param oauthId
	 * @return
	 */
	User findByOauthId(Integer oauthId);

	/**
	 * 手机号唯一
	 */
	User findByMobile(String mobile);

	@Modifying
	@Transactional
	@Query("update User set point=:point where id=:id")
	int updatePoint(@Param("id") Integer id, @Param("point") Integer point);

	@Modifying
	@Transactional
	@Query("update User set name=?1, mobile=?2, email=?3, roleId=?4, roleName=?5 where id=?6")
	void updateUser(String name, String mobile, String email, Integer roleId, String roleName, Integer id);

	@Modifying
	@Transactional
	@Query("update User set rightsPay=?1 where id = ?2")
	int updateRightsPay(Integer rightsPay, Integer id);
}
