package cn.hd01.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import cn.hd01.repository.entity.ViewHistory;

public interface ViewHistoryRepository extends JpaRepository<ViewHistory, Integer> {
	Page<ViewHistory> findByUserId(Integer userId, Pageable pageable);
	
	ViewHistory findByTypeAndRefId(Integer type, Integer refId);
}
