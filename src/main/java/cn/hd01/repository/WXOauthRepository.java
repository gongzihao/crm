package cn.hd01.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cn.hd01.repository.entity.WXOauth;

@Repository
public interface WXOauthRepository extends JpaRepository<WXOauth, Integer> {
	WXOauth findByWeixinIdAndOpenId(String weixinId, String openId);
}
