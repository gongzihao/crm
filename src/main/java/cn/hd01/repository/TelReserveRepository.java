package cn.hd01.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import cn.hd01.repository.entity.TelReservation;

@Repository
public interface TelReserveRepository extends JpaRepository<TelReservation, Integer> {
	
	@Query("select u from TelReservation u where u.staffId = ?1 and u.state != 4")
	List<TelReservation> findByStaffId(Integer staffId);

	List<TelReservation> findByCustomerId(Integer customerId);

	Page<TelReservation> findAll(Specification<TelReservation> spec, Pageable pageable);

	@Modifying
	@Transactional
	@Query("update TelReservation set customerFeedback=true, state=3 where id = ?1")
	int customerFeedBack(Integer id);

	@Modifying
	@Transactional
	@Query("update TelReservation set staffFeedback=true, state=4 where id = ?1")
	int staffFeedback(Integer id);

	@Modifying
	@Transactional
	@Query("update TelReservation set state = state + 1 where id = ?1")
	public void updateState(Integer id);

}
