package cn.hd01.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import cn.hd01.repository.entity.RolePermissions;

@Repository
public interface RolePermissionsResp extends JpaSpecificationExecutor<RolePermissions>,
		JpaRepository<RolePermissions, Integer> {

	public void deleteByRoleId(Integer id);

	public List<RolePermissions> findByRoleId(Integer id);
}
