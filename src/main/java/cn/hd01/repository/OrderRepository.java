package cn.hd01.repository;

import cn.hd01.repository.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * @author cyb
 * @date 2017/4/26.
 */
@Repository
public interface OrderRepository extends JpaRepository<Order, String> {

    @Modifying
    @Transactional
    @Query(value = "update Order o set o.status = ?1 where o.orderId = ?2")
    int updateStatusByOrderId(Integer status, String orderId);

    Order findByUserIdAndType(Integer userId, Integer type);

    Order findByTypeAndProductId(Integer type, String productId);

    Order findByOrderId(String orderId);
}
