package cn.hd01.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import cn.hd01.repository.entity.Deal;

public interface DealRepository extends JpaRepository<Deal, Integer> {
	Page<Deal> findAll(Specification<Deal> spec, Pageable pageable);
}
