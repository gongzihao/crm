package cn.hd01.repository;

import cn.hd01.repository.entity.VipLevel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author cyb
 * @date 2017/4/25.
 */
@Repository
public interface VipLevelRepository extends JpaRepository<VipLevel, Integer> {
}
