//package cn.hd01.repository;
//
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;
//import org.springframework.data.jpa.domain.Specification;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Modifying;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;
//import org.springframework.stereotype.Repository;
//
//import javax.transaction.Transactional;
//
//import cn.hd01.repository.entity.MallUser;
//
//@Repository
//public interface MallUserRepository extends JpaRepository<MallUser, Integer> {
//
//    Page<MallUser> findAll(Specification<MallUser> spec, Pageable pageable);
//
//    /**
//     * 每个微信用户 只能注册一个账户
//     * @param oauthId
//     * @return
//     */
//    MallUser findByOauthId(Integer oauthId);
//
//    /**
//     * 手机号唯一
//     */
//    MallUser findByMobile(String mobile);
//
//    @Modifying
//    @Transactional
//    @Query("update MallUser set password=?2 where id=?1")
//    int updatePassword(Integer id, String password);
//
//    @Modifying
//    @Transactional
//    @Query("update MallUser set point=:point where id=:id")
//    int modify(@Param("id") Integer id, @Param("point") Integer point);
//}
