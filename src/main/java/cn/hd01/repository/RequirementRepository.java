package cn.hd01.repository;

import cn.hd01.repository.entity.Requirement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Date;


/**
 * Created by beta.chen
 * Time:2017/5/7 21:12
 */
@Repository
public interface RequirementRepository extends JpaRepository<Requirement, Integer> {

    Page<Requirement> findAll(Specification<Requirement> spec, Pageable pageable);

    @Modifying
    @Transactional
    @Query("update Requirement r set r.status=?1 where r.id = ?2 and r.status=?3")
    int updateStatus(Integer newStatus, Integer id, Integer oldStatus);

    @Modifying
    @Transactional
    @Query("update Requirement r set r.status=?1, r.price=?2 where r.id = ?3 and r.status=?4")
    int updateStatusAndPrice(Integer newStatus, Double price, Integer id, Integer oldStatus);

    @Modifying
    @Transactional
    @Query("update Requirement r set r.status=?1, r.alreadyPaid= r.alreadyPaid + ?2 where r.id = ?3 and r.status=?4")
    int updateStatusAndAlreadyPaid(Integer newStatus, Double sumMoney, Integer id, Integer oldStatus);

    @Modifying
    @Transactional
    @Query("update Requirement r set r.status=?1, r.dealTime=?2, r.designerId=?3, r.designer=?4, r.price=?5 where r.id =?6 and r.status=?7")
    int examAndPick(Integer status, Date time, Integer designerId, String designer, Double price, Integer id, Integer oldStatus);

    @Modifying
    @Transactional
    @Query("update Requirement r set r.status=?1, r.dealTime=?2, r.designerId=?3, r.designer=?4 where r.id =?5 and r.status=?6")
    int pickRequirement(Integer status, Date time, Integer designerId, String designer, Integer id, Integer oldStatus);
}
