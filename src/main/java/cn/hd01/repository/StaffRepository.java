package cn.hd01.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cn.hd01.repository.entity.Staff;

@Repository
public interface StaffRepository extends JpaRepository<Staff, Integer> {
	public Staff findByOauthId(Integer oauthId);

	public List<Staff> findByType(String type);

	public Page<Staff> findAll(Specification<Staff> spec, Pageable pageable);

	public Staff findByNameAndPassword(String name, String password);

	public int countByPhone(String phone);

}
