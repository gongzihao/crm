package cn.hd01.repository.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_customer_feeback")
public class CustomerFeedBack {
	@Id
	@GeneratedValue
	private Integer id;

	@Column(unique = true)
	private Integer reservationId;

	private Integer score;

	@Column(nullable = true)
	private Integer satisfaction;

	@Column(nullable = true)
	private Integer clothing;

	@Column(nullable = true)
	private Integer communication;

	@Column(nullable = true)
	private Integer introduction;

	@Column(nullable = true)
	private Integer professional;

	@Column(nullable = true)
	private String comment;

	private Date createTime = new Date();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getReservationId() {
		return reservationId;
	}

	public void setReservationId(Integer reservationId) {
		this.reservationId = reservationId;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public Integer getSatisfaction() {
		return satisfaction;
	}

	public void setSatisfaction(Integer satisfaction) {
		this.satisfaction = satisfaction;
	}

	public Integer getClothing() {
		return clothing;
	}

	public void setClothing(Integer clothing) {
		this.clothing = clothing;
	}

	public Integer getCommunication() {
		return communication;
	}

	public void setCommunication(Integer communication) {
		this.communication = communication;
	}

	public Integer getIntroduction() {
		return introduction;
	}

	public void setIntroduction(Integer introduction) {
		this.introduction = introduction;
	}

	public Integer getProfessional() {
		return professional;
	}

	public void setProfessional(Integer professional) {
		this.professional = professional;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
