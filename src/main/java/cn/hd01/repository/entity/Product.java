package cn.hd01.repository.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 产品实体
 *
 * @author cyb
 * @date 2017/5/5.
 */
@Entity
@Table(name = "t_product")
public class Product {

    @Id
    @GeneratedValue
    private Integer id;

    private String name;

    private Integer type;

    // 供应商ID
    private Integer authorId;

    // 供应商
    private String author;

    // 产品价格
    private Double price;

    // 产品描述
    private String description;

    @Column(nullable = false, updatable = false)
    private Date createTime = new Date();

    // 封面文件id, 关联upload 为空使用第一张
    private Integer coverPic;

    // 热门
    @Column(updatable = false)
    private Boolean hot;

    // 推荐
    @Column(updatable = false)
    private Boolean recommend;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getCoverPic() {
        return coverPic;
    }

    public void setCoverPic(Integer coverPic) {
        this.coverPic = coverPic;
    }

    public Boolean getHot() {
        return hot;
    }

    public void setHot(Boolean hot) {
        this.hot = hot;
    }

    public Boolean getRecommend() {
        return recommend;
    }

    public void setRecommend(Boolean recommend) {
        this.recommend = recommend;
    }
}
