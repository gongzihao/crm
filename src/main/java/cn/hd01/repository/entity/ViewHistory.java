package cn.hd01.repository.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "t_view_history")
public class ViewHistory {
	public static final Integer TYPE_PRODUCT = 1;

	public static final Integer TYPE_STYLE = 2;

	@Id
	@GeneratedValue
	private Integer id;

	private Integer type;

	private Integer userId;

	private Integer refId;

	private Integer times = 0;

	private Date updateTime = new Date();

	@Transient
	private Object attach;

	public ViewHistory(Integer type, Integer refId) {
		super();
		this.type = type;
		this.refId = refId;
	}

	public ViewHistory() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getRefId() {
		return refId;
	}

	public void setRefId(Integer refId) {
		this.refId = refId;
	}

	public Object getAttach() {
		return attach;
	}

	public void setAttach(Object attach) {
		this.attach = attach;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getTimes() {
		return times;
	}

	public void setTimes(Integer times) {
		this.times = times;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
}
