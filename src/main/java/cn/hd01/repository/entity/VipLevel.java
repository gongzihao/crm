package cn.hd01.repository.entity;


import javax.persistence.*;

/**
 * @author cyb
 * @date 2017/4/25.
 */
@Entity
@Table(name = "sys_vip")
public class VipLevel {

    @Id
    @GeneratedValue
    private Integer id;


    @Column(nullable = false, unique = true)
    private Integer level;

    @Column(nullable = false)
    private String name;

    // 折扣百分比
    @Column(nullable = false)
    private Integer discount;

    @Column
    private Long requirePoint;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Long getRequirePoint() {
        return requirePoint;
    }

    public void setRequirePoint(Long requirePoint) {
        this.requirePoint = requirePoint;
    }
}
