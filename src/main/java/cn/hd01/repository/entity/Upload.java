package cn.hd01.repository.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * 上传附件
 *
 * @author cyb
 * @date 2017/5/10.
 */
@Entity
@Table(name = "t_upload")
public class Upload {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(updatable = false)
    private String type;

    @Column(updatable = false)
    private Integer refId;

    @Column(updatable = false)
    private String fileName;

    private String description;

    @Column(updatable = false)
    private Date createTime = new Date();

    private Integer sort;

    @Transient
    private String url;

    public Upload() {
    }

    public Upload(String type, Integer refId, String fileName) {
        this.type = type;
        this.refId = refId;
        this.fileName = fileName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getRefId() {
        return refId;
    }

    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
