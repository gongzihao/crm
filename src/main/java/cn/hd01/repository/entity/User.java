package cn.hd01.repository.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sys_user")
public class User {
	@Id
	@GeneratedValue
	private Integer id;

	private String name;

	@Column(updatable = false)
	private String password;

	@Column(nullable = true)
	private String email;

//	@Column(nullable = true)
//	private String phone;

	private Integer roleId;

	private String roleName;

	@Column(nullable = true, updatable = false)
	private Date createTime = new Date();

	private String sex;

	@Column(nullable = false, unique = true)
	private String mobile;

	@Column(nullable = true)
	private String qq;

	@Column(nullable = true)
	private String weixin;

	@Column(nullable = true, updatable = false)
	private String invitateCode;

	@Column(nullable = true)
	private String city;

	@Column(nullable = true)
	private String major;

	@Column(nullable = true, updatable = false)
	private String status;

	@Column(nullable = true)
	private String latitude;

	@Column(nullable = true)
	private String longitude;

	@Column(nullable = true)
	private String factoryName;

	@Column(nullable = true)
	private String factoryAddress;

	@Column(nullable = true)
	private String factorySize;

	@Column(nullable = true)
	private String factoryNo;

	// 0 用户；1 设计师；2 供应商 3后台用户
	@Column(nullable = false, updatable = false)
	private Integer type;

	@Column(updatable = false, unique = true)
	private Integer oauthId;

	@Column(columnDefinition="integer default 0")
	private Integer point = 0;

	// 1 已支付, 2 不支付
	@Column(updatable = false, columnDefinition="integer default 0")
	private Integer rightsPay = 0;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

//	public String getPhone() {
//		return phone;
//	}
//
//	public void setPhone(String phone) {
//		this.phone = phone;
//	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getWeixin() {
		return weixin;
	}

	public void setWeixin(String weixin) {
		this.weixin = weixin;
	}

	public String getInvitateCode() {
		return invitateCode;
	}

	public void setInvitateCode(String invitateCode) {
		this.invitateCode = invitateCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getFactoryName() {
		return factoryName;
	}

	public void setFactoryName(String factoryName) {
		this.factoryName = factoryName;
	}

	public String getFactoryAddress() {
		return factoryAddress;
	}

	public void setFactoryAddress(String factoryAddress) {
		this.factoryAddress = factoryAddress;
	}

	public String getFactorySize() {
		return factorySize;
	}

	public void setFactorySize(String factorySize) {
		this.factorySize = factorySize;
	}

	public String getFactoryNo() {
		return factoryNo;
	}

	public void setFactoryNo(String factoryNo) {
		this.factoryNo = factoryNo;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getOauthId() {
		return oauthId;
	}

	public void setOauthId(Integer oauthId) {
		this.oauthId = oauthId;
	}

	public Integer getPoint() {
		return point;
	}

	public void setPoint(Integer point) {
		this.point = point;
	}

	public Integer getRightsPay() {
		return rightsPay;
	}

	public void setRightsPay(Integer rightsPay) {
		this.rightsPay = rightsPay;
	}
}
