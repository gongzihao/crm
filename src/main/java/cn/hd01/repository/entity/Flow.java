package cn.hd01.repository.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * 流程节点实体
 * @author cyb
 * @date 2017/5/23.
 */
@Entity
@Table(name = "t_flow")
public class Flow {
    public static final String REQUIREMENT = "requirement";

    @Id
    @GeneratedValue
    private Long id;

    @Column(updatable = false)
    private String type;

    @Column(updatable = false)
    private Integer refId;

    @Column(updatable = false)
    private String name;

    @Column(updatable = false)
    private String adder;

    @Column(updatable = false)
    private Integer adderId;

    @Column(updatable = false)
    private Date createTime = new Date();

    public Flow() {
    }

    public Flow(String type, Integer refId, String name, User user){
        this();

        this.type = type;
        this.refId = refId;
        this.name = name;
        this.adder = user.getName();
        this.adderId = user.getId();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getRefId() {
        return refId;
    }

    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdder() {
        return adder;
    }

    public void setAdder(String adder) {
        this.adder = adder;
    }

    public Integer getAdderId() {
        return adderId;
    }

    public void setAdderId(Integer adderId) {
        this.adderId = adderId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
