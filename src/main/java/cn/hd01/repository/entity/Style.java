package cn.hd01.repository.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * 方案实体
 * @author cyb
 * @date 2017/5/22.
 */
@Entity
@Table(name = "t_style")
public class Style {
    @Id
    @GeneratedValue
    private Integer id;

    private String name;

    private Integer type;

    // 设计师
    private Integer authorId;

    // 设计师名字
    private String author;

    private String budget;

    private Integer style;

    // 方案描述
    private String description;

    // 状态
    @Column(columnDefinition = "bit(1) default b'0'")
    private Boolean disable = false;

    @Column(nullable = false, updatable = false)
    private Date createTime = new Date();

    // 封面文件id, 关联upload 为空使用第一张
    private Integer coverPic;

    // 热门
    @Column(updatable = false)
    private Boolean hot;

    // 推荐
    @Column(updatable = false)
    private Boolean recommend;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public Integer getStyle() {
        return style;
    }

    public void setStyle(Integer style) {
        this.style = style;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getDisable() {
        return disable;
    }

    public void setDisable(Boolean disable) {
        this.disable = disable;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getCoverPic() {
        return coverPic;
    }

    public void setCoverPic(Integer coverPic) {
        this.coverPic = coverPic;
    }

    public Boolean getHot() {
        return hot;
    }

    public void setHot(Boolean hot) {
        this.hot = hot;
    }

    public Boolean getRecommend() {
        return recommend;
    }

    public void setRecommend(Boolean recommend) {
        this.recommend = recommend;
    }
}
