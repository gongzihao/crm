package cn.hd01.repository.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * 购物车实体
 *
 * @author cyb
 * @date 2017/5/31.
 */
@Entity
@Table(name = "t_car")
public class Car {
    @Id
    @GeneratedValue
    private Integer id;

    private String author;

    private Integer authorId;

    private Integer type;

    private Integer refId;

    private String refName;

    @Column(nullable = false, updatable = false)
    private Date createTime = new Date();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getRefId() {
        return refId;
    }

    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    public String getRefName() {
        return refName;
    }

    public void setRefName(String refName) {
        this.refName = refName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
