package cn.hd01.repository.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_requirement")
public class Requirement {

	@Id
	@GeneratedValue
	private Integer id;

	// 标题
	private String title;

	// 备注
	private String description;

	// 发单人id
	private Integer ownerId;

	// 发单人
	private String ownerName;

	// 行业
	private Integer industry;

	// 风格
	private Integer style;

	// 面积
	private Double size;

	// 预算， 格式 10-30， 单位万
	private String budget;

	@Column(nullable = true)
	private String qq;

	@Column(nullable = true)
	private String weixin;

	// 发单状态 0-待付定金; 1-待审核; 2-审核通过; 3-已接单; 4-已提交作品; 5-已验收; 6-审核不通过
	@Column(columnDefinition="integer default 0", updatable = false)
	private Integer status;

	// 省市区
	private String province;

	private String city;

	private String district;

	// 接单设计师id
	@Column(nullable = true, updatable = false)
	private Integer designerId;

	// 接单设计师
	@Column(updatable = false)
	private String designer;

	// 定金
	private Double deposits;

	// 价格
	private Double price;

	// 发单人已付款
	@Column(columnDefinition="double default 0", updatable = false)
	private Double alreadyPaid;

	// 接单人收款金额
	@Column(nullable = true, updatable = false)
	private Double remittance;

	// 备注
	@Column(nullable = true)
	private String comment;

	// 创建时间
	 @Column(updatable = false)
	private Date createTime = new Date();

	// 接单时间
	private Date dealTime;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public Integer getIndustry() {
		return industry;
	}

	public void setIndustry(Integer industry) {
		this.industry = industry;
	}

	public Integer getStyle() {
		return style;
	}

	public void setStyle(Integer style) {
		this.style = style;
	}

	public Double getSize() {
		return size;
	}

	public void setSize(Double size) {
		this.size = size;
	}

	public String getBudget() {
		return budget;
	}

	public void setBudget(String budget) {
		this.budget = budget;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getWeixin() {
		return weixin;
	}

	public void setWeixin(String weixin) {
		this.weixin = weixin;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public Integer getDesignerId() {
		return designerId;
	}

	public void setDesignerId(Integer designerId) {
		this.designerId = designerId;
	}

	public String getDesigner() {
		return designer;
	}

	public void setDesigner(String designer) {
		this.designer = designer;
	}

	public Double getDeposits() {
		return deposits;
	}

	public void setDeposits(Double deposits) {
		this.deposits = deposits;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getAlreadyPaid() {
		return alreadyPaid;
	}

	public void setAlreadyPaid(Double alreadyPaid) {
		this.alreadyPaid = alreadyPaid;
	}

	public Double getRemittance() {
		return remittance;
	}

	public void setRemittance(Double remittance) {
		this.remittance = remittance;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getDealTime() {
		return dealTime;
	}

	public void setDealTime(Date dealTime) {
		this.dealTime = dealTime;
	}
}
