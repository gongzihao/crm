package cn.hd01.repository.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_banner")
public class Banner {
	public static final Integer TYPE_PRODUCT = 1;

	public static final Integer TYPE_STYLE = 2;

	@Id
	@GeneratedValue
	private Integer id;

	private Integer type;

	private Integer refId;

	private Integer imgId;

	private Integer seq = 0;

	private Date createTime = new Date();

	public Banner() {
	}

	public Banner(Integer type, Integer refId, Integer imgId) {
		this();
		this.type = type;
		this.refId = refId;
		this.imgId = imgId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getRefId() {
		return refId;
	}

	public void setRefId(Integer refId) {
		this.refId = refId;
	}

	public Integer getImgId() {
		return imgId;
	}

	public void setImgId(Integer imgId) {
		this.imgId = imgId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

}
