package cn.hd01.repository.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "t_store_visit")
public class StoreVisit {
	@Id
	@GeneratedValue
	private Integer id;

	private Integer customerId;

	private String customerName;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date visitTime;

	private Boolean isFeedback = Boolean.FALSE;

	private Boolean isInvestigate = Boolean.FALSE;

	private Integer checkData = 0;

	private Integer staffId;

	private String staffName;

	private Date createTime = new Date();

	private Integer createBy;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Date getVisitTime() {
		return visitTime;
	}

	public void setVisitTime(Date visitTime) {
		this.visitTime = visitTime;
	}

	public Boolean getIsFeedback() {
		return isFeedback;
	}

	public void setIsFeedback(Boolean isFeedback) {
		this.isFeedback = isFeedback;
	}

	public Boolean getIsInvestigate() {
		return isInvestigate;
	}

	public void setIsInvestigate(Boolean isInvestigate) {
		this.isInvestigate = isInvestigate;
	}

	public Integer getStaffId() {
		return staffId;
	}

	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getCreateBy() {
		return createBy;
	}

	public void setCreateBy(Integer createBy) {
		this.createBy = createBy;
	}

	public Integer getCheckData() {
		return checkData;
	}

	public void setCheckData(Integer checkData) {
		this.checkData = checkData;
	}
}
