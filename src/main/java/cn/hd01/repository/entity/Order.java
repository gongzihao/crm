package cn.hd01.repository.entity;

import cn.hd01.weixin.util.WeiXinUtils;

import javax.persistence.*;
import java.util.Date;

/**
 * @author cyb
 * @date 2017/4/26.
 */
@Entity
@Table(name = "t_order")
public class Order {

    public static final int RIGHTS = 0; // 权益订单
    public static final int PRODUCT = 1; // 方案订单
    public static final int REQUIREMENT_DEPOSITS = 3; // 发单定金
    public static final int REQUIREMENT_BALANCE = 4; // 发单尾款
    public static final int REQUIREMENT_TRANSFER = 5; // 付款接单人

    @Id
    private String orderId;

    // 订单金额 单位分
    private Long sumFee;

    @Column(updatable = false)
    private Integer userId;

    @Column(nullable = false)
    private Date createTime = new Date();

    // 0 未支付, 1 已支付
    private Integer status = 0;

    // 0 权益订单, 1 方案订单
    @Column(nullable = false)
    private Integer type;

    @Column
    private String productId;

    /**
     * 创建根据 wxOauth创建订单
     *
     * @param user
     * @param type    订单类型， 0 权益订单, 1 方案订单
     * @param sumFee  订单金额，单位分
     * @return
     */
    public static Order createOrder(User user, Integer type, Long sumFee, String productId) {
//        int sumFee = (int) (sumYuan * 100);

        Order order = new Order();
        order.setOrderId(WeiXinUtils.nonceStr());
        order.setUserId(user.getId());
        order.setSumFee(sumFee);
        order.setProductId(productId);

        order.setType(type);

        return order;
    }

    public boolean hasPay() {
        return 1 == status;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Long getSumFee() {
        return sumFee;
    }

    public void setSumFee(Long sumFee) {
        this.sumFee = sumFee;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
