package cn.hd01.repository.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_staff_feeback")
public class StaffFeedback {
	@Id
	@GeneratedValue
	private Integer id;

	@Column(unique = true)
	private Integer reservationId;

	@Column(nullable = true)
	private String customerAbility;

	@Column(nullable = true)
	private String health;

	@Column(nullable = true)
	private String problem;

	@Column(nullable = true)
	private Integer childs;

	@Column(nullable = true)
	private String consumption;

	@Column(nullable = true)
	private Boolean communication = Boolean.FALSE;

	@Column(nullable = true)
	private String agree;

	@Column(nullable = true)
	private String focus;

	@Column(nullable = true)
	private String feature;

	@Column(nullable = true)
	private String comment;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getReservationId() {
		return reservationId;
	}

	public void setReservationId(Integer reservationId) {
		this.reservationId = reservationId;
	}

	public String getCustomerAbility() {
		return customerAbility;
	}

	public void setCustomerAbility(String customerAbility) {
		this.customerAbility = customerAbility;
	}

	public String getHealth() {
		return health;
	}

	public void setHealth(String health) {
		this.health = health;
	}

	public String getProblem() {
		return problem;
	}

	public void setProblem(String problem) {
		this.problem = problem;
	}

	public Integer getChilds() {
		return childs;
	}

	public void setChilds(Integer childs) {
		this.childs = childs;
	}

	public String getConsumption() {
		return consumption;
	}

	public void setConsumption(String consumption) {
		this.consumption = consumption;
	}

	public Boolean getCommunication() {
		return communication;
	}

	public void setCommunication(Boolean communication) {
		this.communication = communication;
	}

	public String getAgree() {
		return agree;
	}

	public void setAgree(String agree) {
		this.agree = agree;
	}

	public String getFocus() {
		return focus;
	}

	public void setFocus(String focus) {
		this.focus = focus;
	}

	public String getFeature() {
		return feature;
	}

	public void setFeature(String feature) {
		this.feature = feature;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
}
