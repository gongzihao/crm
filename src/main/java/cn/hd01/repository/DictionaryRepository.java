package cn.hd01.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cn.hd01.repository.entity.Dictionary;

@Repository
public interface DictionaryRepository extends JpaRepository<Dictionary, Integer> {
	Page<Dictionary> findAll(Specification<Dictionary> spec, Pageable pageable);

	List<Dictionary> findByType(String type);

}
