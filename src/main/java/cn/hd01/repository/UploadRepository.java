package cn.hd01.repository;

import cn.hd01.repository.entity.Upload;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author cyb
 * @date 2017/5/10.
 */
public interface UploadRepository extends JpaRepository<Upload, Integer> {

    @Transactional
    void deleteByTypeAndRefId(String type, Integer refId);

    List<Upload> findByTypeAndRefIdOrderBySortAsc(String type, Integer refId);
}
