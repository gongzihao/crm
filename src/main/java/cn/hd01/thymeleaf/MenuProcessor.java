package cn.hd01.thymeleaf;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;
import org.thymeleaf.Arguments;
import org.thymeleaf.Configuration;
import org.thymeleaf.dom.Element;
import org.thymeleaf.dom.Node;
import org.thymeleaf.dom.Text;
import org.thymeleaf.processor.attr.AbstractChildrenModifierAttrProcessor;
import org.thymeleaf.spring4.context.SpringWebContext;
import org.thymeleaf.standard.expression.IStandardExpression;
import org.thymeleaf.standard.expression.IStandardExpressionParser;
import org.thymeleaf.standard.expression.StandardExpressions;

import cn.hd01.repository.entity.Permissions;
import cn.hd01.service.PermissionsService;
import cn.hd01.util.TreeNode;

public class MenuProcessor extends AbstractChildrenModifierAttrProcessor {

	public MenuProcessor() {
		super("menu");
	}

	@Override
	public int getPrecedence() {
		return 0;
	}

	@Override
	protected List<Node> getModifiedChildren(Arguments arguments, Element element, String attributeName) {
		String attributeValue = element.getAttributeValue(attributeName);
		Configuration configuration = arguments.getConfiguration();
		IStandardExpressionParser parser = StandardExpressions.getExpressionParser(configuration);
		IStandardExpression expression = parser.parseExpression(configuration, arguments, attributeValue);
		Integer role = (Integer) expression.execute(configuration, arguments);

		SpringWebContext context = (SpringWebContext) arguments.getContext();
		String basepath = context.getHttpServletRequest().getContextPath();
		String currpath = context.getHttpServletRequest().getRequestURI();
		
		System.err.println(context.getHttpServletRequest().getAttribute("menu"));
		
		PermissionsService service = context.getApplicationContext().getBean(PermissionsService.class);
		TreeNode<Permissions> permissionsTree = service.findMenuByRoleId(role);

		List<Node> nodes = new ArrayList<Node>();
		for (TreeNode<Permissions> node : permissionsTree.getChildren()) {
			nodes.add(build(node, basepath, currpath));
		}
		return nodes;
	}

	private Node build(TreeNode<Permissions> node, String basepath, String currpath) {
		Permissions p = node.getData();

		Element container = new Element("li");
		Element a = new Element("a");
		
		String url = url(p.getUrl(), basepath);
		a.setAttribute("href", url);

		if(currpath.startsWith(url)){
			container.setAttribute("class", "active");
		}
		
		if (StringUtils.hasText(p.getImg())) {
			Element icon = new Element("i");
			icon.setAttribute("class", p.getImg());
			a.addChild(icon);
		}

		Element text = new Element("span");
		text.addChild(new Text(p.getName()));
		a.addChild(text);

		container.addChild(a);

		Element ul = new Element("ul");

		List<Node> childs = new ArrayList<Node>();
		for (TreeNode<Permissions> child : node.getChildren()) {
			childs.add(build(child, basepath, currpath));
		}

		if (!childs.isEmpty()) {
			container.setAttribute("class", "active");
			ul.setChildren(childs);
			container.addChild(ul);
		}

		return container;
	}

	private String url(String menu, String basepath) {
		if (!StringUtils.hasText(menu)) {
			return "javascript:void(0);";
		}

		return basepath + (menu.startsWith("/") ? "" : "/") + menu;
	}

}
