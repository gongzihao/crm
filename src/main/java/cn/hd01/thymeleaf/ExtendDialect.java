package cn.hd01.thymeleaf;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Component;
import org.thymeleaf.dialect.AbstractDialect;
import org.thymeleaf.processor.IProcessor;

@Component
public class ExtendDialect extends AbstractDialect {

	/*
	 * Default prefix: this is the prefix that will be used for this dialect
	 * unless a different one is specified when adding the dialect to the
	 * Template Engine. 默认的用于该方言的前缀。可以再添加到模板引擎时进行了制定。
	 */
	public String getPrefix() {
		return "extend";
	}

	/*
	 * 定义了2个属性处理器 'classforposition' 和 'remarkforposition'. 以及一个元素处理器'headlines'
	 */
	@Override
	public Set<IProcessor> getProcessors() {
		final Set<IProcessor> processors = new HashSet<IProcessor>();
		processors.add(new MenuProcessor());
		processors.add(new PermissionsProcessor());
		processors.add(new HasPermissionProcessor());
		processors.add(new LacksPermissionProcessor());
		return processors;
	}

}
