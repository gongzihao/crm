//package cn.hd01.service;
//
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;
//
//import cn.hd01.repository.entity.MallUser;
//
//public interface MallUserService extends BaseService<MallUser, Integer> {
//
//    MallUser findByOauthId(Integer oauthId);
//
//    MallUser findByMobile(String mobile);
//
//    Page<MallUser> findAll(MallUser user, Pageable pageable);
//
//    void updatePassword(Integer id, String password);
//
//    void modify(MallUser user);
//}
