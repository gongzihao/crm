package cn.hd01.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface BaseService<T, ID extends Serializable> {
	public Page<T> findAll(Pageable pageable);

	public <S extends T> S save(S entity);

	public T findOne(ID id);

	public boolean exists(ID id);

	public long count();

	public void delete(ID id);

	public void delete(T entity);

	public void delete(Iterable<? extends T> entities);

	public void deleteAll();

	public List<T> findAll();

	public List<T> findAll(Iterable<ID> ids);

	public <S extends T> List<S> save(Iterable<S> entities);

	public void deleteInBatch(Iterable<T> entities);

	public void deleteAllInBatch();

	public T getOne(ID id);
}
