package cn.hd01.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.hd01.repository.entity.Role;

public interface RoleService extends BaseService<Role, Integer> {
	public Page<Role> find(String name, Pageable pageable);
	
	public boolean exists(String name);
}
