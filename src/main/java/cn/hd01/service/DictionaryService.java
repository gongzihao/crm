package cn.hd01.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.hd01.repository.entity.Dictionary;
import cn.hd01.util.DictionaryType;

public interface DictionaryService extends BaseService<Dictionary, Integer> {

	public Page<Dictionary> find(Dictionary dict, Pageable pageable);

	public List<Dictionary> findByType(DictionaryType type);

}
