package cn.hd01.service;

import cn.hd01.repository.entity.Car;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author cyb
 * @date 2017/5/31.
 */
public interface CarService extends BaseService<Car, Integer>{

    Page<Car> findAll(Car car, Pageable pageable);
}
