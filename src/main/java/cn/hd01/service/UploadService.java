package cn.hd01.service;

import cn.hd01.repository.entity.Upload;

import org.springframework.web.multipart.MultipartFile;

import java.io.OutputStream;
import java.util.List;

/**
 * @author cyb
 * @date 2017/5/10.
 */
public interface UploadService extends BaseService<Upload, Integer> {

	List<Upload> saveUploadAndFile(MultipartFile[] files, String type, Integer refId);

	void deleteByTypeAndRefId(String type, Integer refId);

	List<Upload> findByTypeAndRefIdOrderBySortAsc(String type, Integer refId);

	void download(Integer id, OutputStream os);
}
