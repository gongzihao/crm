package cn.hd01.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.hd01.repository.entity.CustomerFeedBack;
import cn.hd01.repository.entity.StaffFeedback;
import cn.hd01.repository.entity.TelReservation;

public interface TelReserveService extends BaseService<TelReservation, Integer> {

	List<TelReservation> findByStaffId(Integer staffId);

	Page<TelReservation> findAll(Integer customerId, Pageable pageable);

	void customerFeedBack(CustomerFeedBack cusomerFeedBack);
	
	CustomerFeedBack customerFeedBack(Integer customerId);

	void staffFeedback(StaffFeedback staffFeedback);
	
	StaffFeedback staffFeedback(Integer customerId);
	
	void updateState(Integer id);
}
