package cn.hd01.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.hd01.repository.entity.CheckData;
import cn.hd01.repository.entity.Investigate;
import cn.hd01.repository.entity.StoreVisit;
import cn.hd01.repository.entity.VisitFeedback;

public interface StoreVisitService extends BaseService<StoreVisit, Integer> {
	List<StoreVisit> findByStaffId(Integer staffId);

	List<StoreVisit> findByCreateBy(Integer staffId);

	Page<StoreVisit> findAll(Integer customerId, Pageable pageable);

	void updateCheckData(CheckData data);

	void feedback(VisitFeedback feedback);

	void investigate(Investigate investigate);

}
