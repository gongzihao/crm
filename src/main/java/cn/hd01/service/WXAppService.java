package cn.hd01.service;

import cn.hd01.repository.entity.WXApp;
import com.github.sd4324530.fastweixin.api.config.ApiConfig;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface WXAppService extends BaseService<WXApp, Integer> {
    WXApp findByWeixinId(String weixinId);

    Page<WXApp> findAll(WXApp wxapp, Pageable pageable);

    WXApp findByDomain(String domain);

    ApiConfig getApiConfig(WXApp app);

    WXApp findByMchId(String mchId);
}
