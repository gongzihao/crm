package cn.hd01.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.hd01.repository.entity.StoreReservation;

public interface StoreReserveService extends BaseService<StoreReservation, Integer> {

	Page<StoreReservation> findAll(Integer customerId, Pageable pageable);

}
