package cn.hd01.service;

import cn.hd01.repository.entity.VipLevel;

/**
 * vip service
 *
 * @author cyb
 * @date 2017/4/25.
 */
public interface VipService extends BaseService<VipLevel, Integer> {

}
