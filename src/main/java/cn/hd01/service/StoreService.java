package cn.hd01.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.hd01.repository.entity.Store;

public interface StoreService extends BaseService<Store, Integer> {
	public Page<Store> find(Store u, Pageable pageable);

	public boolean exists(String name);
}
