package cn.hd01.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.hd01.repository.entity.ViewHistory;

public interface ViewHistoryService extends BaseService<ViewHistory, Integer> {
	Page<ViewHistory> findByUserId(Integer userId, Pageable pageable);

	void view(Integer userId, Integer type, Integer refId);
}
