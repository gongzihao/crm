package cn.hd01.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.hd01.repository.entity.Customer;
import cn.hd01.web.entity.CustomerQuery;

public interface CustomerService extends BaseService<Customer, Integer> {
	public Page<Customer> find(CustomerQuery u, Pageable pageable);
}
