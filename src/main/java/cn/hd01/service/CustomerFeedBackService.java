package cn.hd01.service;

import cn.hd01.repository.entity.CustomerFeedBack;

public interface CustomerFeedBackService extends BaseService<CustomerFeedBack, Integer> {
	CustomerFeedBack findbyReservationId(Integer reservationId);

}
