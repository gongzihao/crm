package cn.hd01.service.impl;

import cn.hd01.repository.OrderRepository;
import cn.hd01.repository.entity.Order;
import cn.hd01.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author cyb
 * @date 2017/4/26.
 */
@Service
public class OrderServiceImpl extends BaseServiceImpl<Order, String> implements OrderService {

    private final OrderRepository repository;

    @Autowired
    public OrderServiceImpl(OrderRepository repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public boolean updateStatusByOrderId(Integer status, String orderId) {
        return repository.updateStatusByOrderId(status, orderId) == 1;
    }

    @Override
    public Order findByUserIdAndType(Integer userId, Integer type) {
        return repository.findByUserIdAndType(userId, type);
    }

    @Override
    public Order findByTypeAndProductId(Integer type, String productId) {
        return repository.findByTypeAndProductId(type, productId);
    }

    @Override
    public Order findByOrderId(String orderId) {
        return repository.findByOrderId(orderId);
    }
}
