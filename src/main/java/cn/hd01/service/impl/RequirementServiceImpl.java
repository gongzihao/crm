package cn.hd01.service.impl;

import cn.hd01.repository.RequirementRepository;
import cn.hd01.repository.entity.Requirement;
import cn.hd01.repository.entity.User;
import cn.hd01.service.RequirementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by beta.chen
 * Time:2017/5/7 21:16
 */
@Service
public class RequirementServiceImpl extends BaseServiceImpl<Requirement, Integer> implements RequirementService {

    private final RequirementRepository repository;

    @Autowired
    public RequirementServiceImpl(RequirementRepository repository) {
        super(repository);
        this.repository = repository;
    }


    @Override
    public Page<Requirement> find(final Requirement requirement, Pageable pageable) {
        if (requirement == null) {
            return repository.findAll(pageable);
        }

        return repository.findAll(Specifications.where(new Specification<Requirement>() {
            @Override
            public Predicate toPredicate(Root<Requirement> r, CriteriaQuery<?> q, CriteriaBuilder
                    cb) {
                List<Predicate> predicates = new ArrayList<Predicate>();

                if (requirement.getOwnerId() != null) {
                    predicates.add(cb.equal(r.get("ownerId").as(Integer.class), requirement.getOwnerId()));
                }

                if (requirement.getDesignerId() != null) {
                    predicates.add(cb.equal(r.get("designerId").as(Integer.class), requirement.getDesignerId()));
                }

                if (requirement.getIndustry() != null) {
                    predicates.add(cb.equal(r.get("industry").as(Integer.class), requirement.getIndustry()));
                }
                
                if (StringUtils.hasText(requirement.getDistrict())) {
					predicates.add(cb.equal(r.get("district").as(String.class), requirement.getDistrict()));
				}
                
                if (requirement.getStyle() != null) {
                    predicates.add(cb.equal(r.get("style").as(Integer.class), requirement.getStyle()));
                }

                if (requirement.getStatus() != null) {
                    predicates.add(cb.equal(r.get("status").as(Integer.class), requirement.getStatus()));
                }

                if (StringUtils.hasText(requirement.getTitle())) {
                    predicates.add(cb.like(r.get("title").as(String.class), "%" + requirement.getTitle() + "%"));
                }

                return cb.and(predicates.toArray(new Predicate[0]));
            }
        }), pageable);
    }

    /**
     * 管理员审核，修改状态
     */
    @Override
    public int examRequirement(Integer id, Integer status, Double price) {
        return repository.updateStatusAndPrice(status, price, id, 1);
    }

    /**
     * 审核并接单
     */
    @Override
    public int examAndPick(Integer id, User user, Double price){
        return repository.examAndPick(5, new Date(), user.getId(), user.getName(), price, id, 1);
    }

    /**
     * 设计师接单，修改接单人、状态
     */
    @Override
    public int pick(Integer id, User user) {
        return repository.pickRequirement(4, new Date(), user.getId(), user.getName(), id, 2);
    }

    /**
     * 设计师 提交完成进度
     */
//    @Override
//    public int pickCommit(Integer id) {
//        return repository.updateStatus(4, id, 3);
//    }

    @Override
    public int receivedRequirement(Integer id) {
        return repository.updateStatus(6, id, 4);
    }

    /**
     * 支付定金回调
     * @param id
     * @param totalFee 支付金额，单位分
     * @return
     */
    @Override
    public int payDepositsSuccess(Integer id, Integer totalFee) {
        Double sumMoney = totalFee / 100.0;
        return repository.updateStatusAndAlreadyPaid(1, sumMoney, id, 0);
    }

    /**
     * 支付尾款回调
     * @param id
     * @param totalFee 支付金额，单位分
     * @return
     */
    @Override
    public int payBalanceSuccess(Integer id, Integer totalFee) {
        Double sumMoney = totalFee / 100.0;
        int status = 2;
        Requirement r =  repository.findOne(id);
        if (r != null && r.getDesigner() != null){
            status = 4;
        }
        return repository.updateStatusAndAlreadyPaid(status, sumMoney, id, 5);
    }

}
