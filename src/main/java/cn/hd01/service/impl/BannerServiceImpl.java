package cn.hd01.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import cn.hd01.repository.BannerRepository;
import cn.hd01.repository.entity.Banner;
import cn.hd01.service.BannerService;

@Service
public class BannerServiceImpl extends BaseServiceImpl<Banner, Integer> implements BannerService {

	private BannerRepository repository;

	@Autowired
	public BannerServiceImpl(BannerRepository repository) {
		super(repository);
		this.repository = repository;
	}

	@Override
	public Page<Banner> findByType(Integer type, Pageable pageable) {
		return repository.findByType(type, pageable);
	}

	@Override
	public List<Banner> findByType(Integer type) {
		return repository.findByTypeOrderBySeqAsc(type);
	}

	@Override
	public Banner findByTypeAndRefId(Integer type, Integer refId) {
		return repository.findByTypeAndRefId(type, refId);
	}
}
