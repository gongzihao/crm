package cn.hd01.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import cn.hd01.repository.ProductRepository;
import cn.hd01.repository.entity.Product;
import cn.hd01.service.ProductService;

/**
 * 产品服务 实现
 *
 * @author cyb
 * @date 2017/5/5.
 */
@Service
public class ProductServiceImpl extends BaseServiceImpl<Product, Integer> implements ProductService {

    private final ProductRepository repository;

    @Autowired
    public ProductServiceImpl(ProductRepository repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public Page<Product> findAll(final Product product, Pageable pageable) {
        if (product == null) {
            return repository.findAll(pageable);
        }

        return repository.findAll(Specifications.where(new Specification<Product>() {
            @Override
            public Predicate toPredicate(Root<Product> r, CriteriaQuery<?> q, CriteriaBuilder
                    cb) {
                List<Predicate> predicates = new ArrayList<>();

                if (StringUtils.hasText(product.getName())) {
                    predicates.add(cb.like(r.get("name").as(String.class), "%" + product.getName() + "%"));
                }
                
                if (product.getType() != null) {
					predicates.add(cb.equal(r.get("type").as(Integer.class), product.getType()));
                }
                
                if (product.getHot() != null) {
					predicates.add(cb.equal(r.get("hot").as(Boolean.class), product.getHot()));
                }
                
                if (product.getRecommend() != null) {
					predicates.add(cb.equal(r.get("recommend").as(Boolean.class), product.getRecommend()));
                }

                return cb.and(predicates.toArray(new Predicate[0]));
            }
        }), pageable);
    }

    @Override
    public void updateCoverPic(Integer id, Integer picId) {
        repository.updateCoverPic(picId, id);
    }

    @Override
    public void updateHot(Integer id, Boolean hot) {
        repository.updateHot(id, hot);
    }

    @Override
    public void updateRecommend(Integer id, Boolean recommend) {
        repository.updateRecommend(id, recommend);
    }
}
