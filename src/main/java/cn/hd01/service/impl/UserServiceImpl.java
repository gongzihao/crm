package cn.hd01.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import cn.hd01.repository.UserRepository;
import cn.hd01.repository.entity.User;
import cn.hd01.service.UserService;

@Service
public class UserServiceImpl extends BaseServiceImpl<User, Integer> implements UserService {

	private UserRepository repository;

	@Autowired
	public UserServiceImpl(UserRepository repository) {
		super(repository);
		this.repository = repository;
	}

	@Override
	public User find(String name, String passowrd) {
		return repository.findFirstByNameOrMobileAndPassword(name, passowrd);
	}

	@Override
	public Page<User> findAdmin(final User u, Pageable pageable){
		if (u == null) {
			return repository.findAll(pageable);
		}

		return repository.findAll(Specifications.where(new Specification<User>() {
			@Override
			public Predicate toPredicate(Root<User> r, CriteriaQuery<?> q, CriteriaBuilder cb) {
				List<Predicate> predicates = new ArrayList<Predicate>();

				if (StringUtils.hasText(u.getName())) {
					predicates.add(cb.like(r.get("name").as(String.class), "%" + u.getName() + "%"));
				}

				if (StringUtils.hasText(u.getMobile())) {
					predicates.add(cb.like(r.get("mobile").as(String.class), "%" + u.getMobile() + "%"));
				}

				if (StringUtils.hasText(u.getRoleName())) {
					predicates.add(cb.equal(r.get("roleName").as(String.class), u.getRoleName()));
				}

				predicates.add(cb.equal(r.get("type").as(Integer.class), 3));

				return cb.and(predicates.toArray(new Predicate[] {}));
			}
		}), pageable);
	}

	@Override
	public Page<User> findClient(final User u, Pageable pageable) {
		if (u == null) {
			return repository.findAll(pageable);
		}

		return repository.findAll(Specifications.where(new Specification<User>() {
			@Override
			public Predicate toPredicate(Root<User> r, CriteriaQuery<?> q, CriteriaBuilder cb) {
				List<Predicate> predicates = new ArrayList<Predicate>();

				if (StringUtils.hasText(u.getName())) {
					predicates.add(cb.like(r.get("name").as(String.class), "%" + u.getName() + "%"));
				}

				if (StringUtils.hasText(u.getMobile())) {
					predicates.add(cb.like(r.get("mobile").as(String.class), "%" + u.getMobile() + "%"));
				}

				if (StringUtils.hasText(u.getRoleName())) {
					predicates.add(cb.equal(r.get("roleName").as(String.class), u.getRoleName()));
				}

			    if (u.getType() != null) {
                    predicates.add(cb.equal(r.get("type").as(Integer.class), u.getType()));
                } else{
					CriteriaBuilder.In<Integer> in = cb.in(r.get("type").as(Integer.class));
					in.value(0).value(1).value(2);

					predicates.add(in);
				}

				return cb.and(predicates.toArray(new Predicate[] {}));
			}
		}), pageable);
	}

	@Override
	public boolean exists(String name) {
		return repository.countByName(name) != 0;
	}

	@Override
	public void updatePassword(Integer id, String password) {
		repository.updatePwd(id, password);
	}

	@Override
	public User findByOauthId(Integer oauthId) {
		return repository.findByOauthId(oauthId);
	}

	@Override
	public User findByMobile(String mobile) {
		return repository.findByMobile(mobile);
	}

	@Override
	public void updatePoint(Integer id, Integer point) {
		repository.updatePoint(id, point);
	}

	@Override
	public void updateUser(User user){
		repository.updateUser(user.getName(), user.getMobile(),user.getEmail(), user.getRoleId(), user.getRoleName(), user.getId());
	}

	@Override
	public void updateRightsPay(Integer id, Integer rightsPay) {
		repository.updateRightsPay(rightsPay, id);
	}
}
