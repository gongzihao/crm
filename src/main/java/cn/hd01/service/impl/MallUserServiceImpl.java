//package cn.hd01.service.impl;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;
//import org.springframework.data.jpa.domain.Specification;
//import org.springframework.data.jpa.domain.Specifications;
//import org.springframework.stereotype.Service;
//import org.springframework.util.StringUtils;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.persistence.criteria.CriteriaBuilder;
//import javax.persistence.criteria.CriteriaQuery;
//import javax.persistence.criteria.Predicate;
//import javax.persistence.criteria.Root;
//
//import cn.hd01.repository.MallUserRepository;
//import cn.hd01.repository.entity.MallUser;
//import cn.hd01.service.MallUserService;
//
//@Service
//public class MallUserServiceImpl extends BaseServiceImpl<MallUser, Integer> implements
//        MallUserService {
//
//    private final MallUserRepository repository;
//
//    @Autowired
//    public MallUserServiceImpl(MallUserRepository repository) {
//        super(repository);
//        this.repository = repository;
//    }
//
//
//    @Override
//    public MallUser findByOauthId(Integer oauthId) {
//        return repository.findByOauthId(oauthId);
//    }
//
//    @Override
//    public MallUser findByMobile(String mobile) {
//        return repository.findByMobile(mobile);
//    }
//
//    @Override
//    public Page<MallUser> findAll(final MallUser user, Pageable pageable) {
//        if (user == null) {
//            return repository.findAll(pageable);
//        }
//
//        return repository.findAll(Specifications.where(new Specification<MallUser>() {
//            @Override
//            public Predicate toPredicate(Root<MallUser> r, CriteriaQuery<?> q,
//                                         CriteriaBuilder cb) {
//                List<Predicate> predicates = new ArrayList<>();
//
//                if (StringUtils.hasText(user.getName())) {
//                    predicates.add(cb.like(r.get("name").as(String.class), "%" + user.getName()
//                            + "%"));
//                }
//
//                if (StringUtils.hasText(user.getMobile())) {
//                    predicates.add(cb.equal(r.get("mobile").as(String.class), user.getMobile()));
//                }
//
//                if (user.getType() != null) {
//                    predicates.add(cb.equal(r.get("type").as(Integer.class), user.getType()));
//                }
//
//                return cb.and(predicates.toArray(new Predicate[0]));
//            }
//        }), pageable);
//    }
//
//    @Override
//    public void updatePassword(Integer id, String password){
//        repository.updatePassword(id, password);
//    }
//
//    @Override
//    public void modify(MallUser user) {
//        repository.modify(user.getId(), user.getPoint());
//    }
//}
