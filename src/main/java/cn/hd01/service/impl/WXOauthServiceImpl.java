package cn.hd01.service.impl;

import cn.hd01.repository.WXOauthRepository;
import cn.hd01.repository.entity.WXOauth;
import cn.hd01.service.WXOauthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by beta.chen
 * Time:2017/5/14 20:48
 */
@Service
public class WXOauthServiceImpl extends BaseServiceImpl<WXOauth, Integer> implements WXOauthService {

    private final WXOauthRepository repository;

    @Autowired
    public WXOauthServiceImpl(WXOauthRepository repository) {
        super(repository);
        this.repository = repository;
    }


    @Override
    public WXOauth findByWeixinIdAndOpenId(String wxAppId, String openId) {
        return repository.findByWeixinIdAndOpenId(wxAppId, openId);
    }
}
