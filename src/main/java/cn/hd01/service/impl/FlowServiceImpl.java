package cn.hd01.service.impl;

import cn.hd01.repository.FlowRepository;
import cn.hd01.repository.entity.Flow;
import cn.hd01.service.FlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author cyb
 * @date 2017/5/23.
 */
@Service
public class FlowServiceImpl extends BaseServiceImpl<Flow, Long> implements FlowService {

    private final FlowRepository repository;

    @Autowired
    public FlowServiceImpl(FlowRepository repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public List<Flow> findFlows(String type, Integer refId) {
        return repository.findByTypeAndRefIdOrderByCreateTime(type, refId);
    }

    @Override
    public Flow findLastFlow(String type, Integer refId) {
        return repository.findFirstByTypeAndRefIdOrderByCreateTimeDesc(type, refId);
    }
}
