package cn.hd01.service.impl;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;

import cn.hd01.repository.StoreVisitRepository;
import cn.hd01.repository.entity.CheckData;
import cn.hd01.repository.entity.Investigate;
import cn.hd01.repository.entity.StoreVisit;
import cn.hd01.repository.entity.VisitFeedback;
import cn.hd01.service.StoreVisitService;

@Service
public class StoreVisitServiceImpl extends BaseServiceImpl<StoreVisit, Integer> implements StoreVisitService {

	private StoreVisitRepository repository;

	@Autowired
	public StoreVisitServiceImpl(StoreVisitRepository repository) {
		super(repository);
		this.repository = repository;
	}

	@Override
	public Page<StoreVisit> findAll(final Integer customerId, Pageable pageable) {
		return repository.findAll(Specifications.where(new Specification<StoreVisit>() {
			@Override
			public Predicate toPredicate(Root<StoreVisit> r, CriteriaQuery<?> q, CriteriaBuilder cb) {
				return cb.equal(r.get("customerId").as(Integer.class), customerId);
			}
		}), pageable);
	}

	@Override
	public List<StoreVisit> findByCreateBy(Integer staffId) {
		return repository.findByCreateBy(staffId);
	}

	@Override
	@Transactional
	public void updateCheckData(CheckData data) {
		repository.updateCheckData(data.getType(), data.getVisitId());

	}

	@Override
	public List<StoreVisit> findByStaffId(Integer staffId) {
		return repository.findByStaffId(staffId);
	}

	@Override
	@Transactional
	public void feedback(VisitFeedback feedback) {
		repository.isFeedback(feedback.getVisitId());

	}

	@Override
	@Transactional
	public void investigate(Investigate investigate) {
		repository.isFeedback(investigate.getVisitId());

	}

}
