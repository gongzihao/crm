package cn.hd01.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.hd01.repository.CustomerFeedBackRepository;
import cn.hd01.repository.entity.CustomerFeedBack;
import cn.hd01.service.CustomerFeedBackService;

@Service
public class CustomerFeedBackServiceImpl extends BaseServiceImpl<CustomerFeedBack, Integer>
		implements CustomerFeedBackService {

	private CustomerFeedBackRepository repository;

	@Autowired
	public CustomerFeedBackServiceImpl(CustomerFeedBackRepository repository) {
		super(repository);
		this.repository = repository;
	}

	@Override
	public CustomerFeedBack findbyReservationId(Integer reservationId) {
		return repository.findByReservationId(reservationId);
	}

}
