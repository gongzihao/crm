package cn.hd01.service.impl;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;

import cn.hd01.repository.StoreReserveRepository;
import cn.hd01.repository.entity.StoreReservation;
import cn.hd01.service.StoreReserveService;

@Service
public class StoreReserveServiceImpl extends BaseServiceImpl<StoreReservation, Integer> implements StoreReserveService {

	private StoreReserveRepository repository;

	@Autowired
	public StoreReserveServiceImpl(StoreReserveRepository repository) {
		super(repository);
		this.repository = repository;
	}

	@Override
	public Page<StoreReservation> findAll(final Integer customerId, Pageable pageable) {
		return repository.findAll(Specifications.where(new Specification<StoreReservation>() {
			@Override
			public Predicate toPredicate(Root<StoreReservation> r, CriteriaQuery<?> q, CriteriaBuilder cb) {
				return cb.equal(r.get("customerId").as(Integer.class), customerId);
			}
		}), pageable);
	}
}
