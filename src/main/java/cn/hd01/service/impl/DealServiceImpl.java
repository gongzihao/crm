package cn.hd01.service.impl;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;

import cn.hd01.repository.DealRepository;
import cn.hd01.repository.entity.Deal;
import cn.hd01.service.DealService;

@Service
public class DealServiceImpl extends BaseServiceImpl<Deal, Integer> implements DealService {

	private DealRepository repository;

	@Autowired
	public DealServiceImpl(DealRepository repository) {
		super(repository);
		this.repository = repository;
	}

	@Override
	public Page<Deal> findAll(final Integer customerId, Pageable pageable) {
		return repository.findAll(Specifications.where(new Specification<Deal>() {
			@Override
			public Predicate toPredicate(Root<Deal> r, CriteriaQuery<?> q, CriteriaBuilder cb) {
				return cb.equal(r.get("customerId").as(Integer.class), customerId);
			}
		}), pageable);
	}
}
