package cn.hd01.service.impl;

import cn.hd01.repository.VipLevelRepository;
import cn.hd01.repository.entity.VipLevel;
import cn.hd01.service.VipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * vip service实现
 *
 * @author cyb
 * @date 2017/4/25.
 */
@Service
public class VipServiceImpl extends BaseServiceImpl<VipLevel, Integer> implements VipService {

    private final VipLevelRepository repository;

    @Autowired
    public VipServiceImpl(VipLevelRepository repository) {
        super(repository);
        this.repository = repository;
    }
}
