package cn.hd01.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import cn.hd01.repository.RoleRepository;
import cn.hd01.repository.entity.Role;
import cn.hd01.service.RoleService;

@Service
public class RoleServiceImpl extends BaseServiceImpl<Role, Integer> implements RoleService {

	private RoleRepository repository;

	@Autowired
	public RoleServiceImpl(RoleRepository repository) {
		super(repository);
		this.repository = repository;
	}

	@Override
	public Page<Role> find(String name, Pageable pageable) {
		if (StringUtils.isEmpty(name)) {
			return repository.findAll(pageable);
		}

		return repository.findByNameContaining(name, pageable);
	}

	@Override
	public boolean exists(String name) {
		return repository.countByName(name) != 0;

	}

}
