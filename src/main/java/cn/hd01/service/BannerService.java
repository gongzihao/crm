package cn.hd01.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.hd01.repository.entity.Banner;

public interface BannerService extends BaseService<Banner, Integer> {
	Page<Banner> findByType(Integer type, Pageable pageable);
	
	List<Banner> findByType(Integer type);

	Banner findByTypeAndRefId(Integer type, Integer refId);
}
