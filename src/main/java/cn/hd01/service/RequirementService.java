package cn.hd01.service;

import cn.hd01.repository.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.hd01.repository.entity.Requirement;

/**
 * Created by beta.chen
 * Time:2017/5/7 21:15
 */
public interface RequirementService extends BaseService<Requirement, Integer> {
    Page<Requirement> find(Requirement requirement, Pageable pageable);

    /**
     * 管理员审核，修改状态、价格
     */
    int examRequirement(Integer id, Integer status, Double price);

    /**
     * 设计师接单，修改接单人、状态
     */
    int examAndPick(Integer id, User user, Double price);

    /**
     * 设计师接单，修改接单人、状态
     */
    int pick(Integer id, User user);

    /**
     * 设计师 提交完成进度
     */
//    int pickCommit(Integer id);

    /**
     * 确认验收订单
     */
    int receivedRequirement(Integer id);

    /**
     * 支付定金回调
     * @param id
     * @param totalFee 支付金额，单位分
     * @return
     */
    int payDepositsSuccess(Integer id, Integer totalFee);

    /**
     * 支付尾款回调
     * @param id
     * @param totalFee 支付金额，单位分
     * @return
     */
    int payBalanceSuccess(Integer id, Integer totalFee);
}
