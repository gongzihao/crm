package cn.hd01.service;

import cn.hd01.repository.entity.Style;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author cyb
 * @date 2017/5/22.
 */
public interface StyleService extends BaseService<Style, Integer> {

    Page<Style> findAll(Style style, Pageable pageable);

    void updateCoverPic(Integer id, Integer picId);

    void updateHot(Integer id, Boolean hot);

    void updateRecommend(Integer id, Boolean recommend);
}
