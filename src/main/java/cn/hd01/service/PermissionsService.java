package cn.hd01.service;

import java.util.List;

import cn.hd01.repository.entity.Permissions;
import cn.hd01.repository.entity.RolePermissions;
import cn.hd01.util.TreeNode;

public interface PermissionsService extends BaseService<Permissions, Integer> {

	public TreeNode<Permissions> getPermissionsTree();

	public TreeNode<Permissions> findMenuByRoleId(int roleid);

	public List<Permissions> findPermissionsCached(int roleid);

	public List<RolePermissions> findPermissions(int roleid);

	public void save(int id, List<Integer> pIds);
}
