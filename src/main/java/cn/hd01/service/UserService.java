package cn.hd01.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.hd01.repository.entity.User;

public interface UserService extends BaseService<User, Integer> {
	public User find(String name, String passowrd);

	public boolean exists(String name);

	public void updatePassword(Integer id, String password);

	public Page<User> findAdmin(User u, Pageable pageable);

	public Page<User> findClient(User u, Pageable pageable);

	User findByOauthId(Integer oauthId);

	User findByMobile(String mobile);

	void updatePoint(Integer id, Integer point);

    void updateUser(User u);

    void updateRightsPay(Integer id, Integer rightsPay);
}
