package cn.hd01.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.hd01.repository.entity.Product;

/**
 * @author cyb
 * @date 2017/5/5.
 */
public interface ProductService extends BaseService<Product, Integer> {

    Page<Product> findAll(Product product, Pageable pageable);

    void updateCoverPic(Integer id, Integer picId);

    void updateHot(Integer id, Boolean hot);

    void updateRecommend(Integer id, Boolean recommend);
}
