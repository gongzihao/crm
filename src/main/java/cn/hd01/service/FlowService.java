package cn.hd01.service;

import cn.hd01.repository.entity.Flow;

import java.util.List;

/**
 * @author cyb
 * @date 2017/5/23.
 */
public interface FlowService extends BaseService<Flow, Long> {

    List<Flow> findFlows(String type, Integer refId);

    Flow findLastFlow(String type, Integer refId);
}
