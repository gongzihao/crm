package cn.hd01.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.hd01.repository.entity.Deal;

public interface DealService extends BaseService<Deal, Integer> {
	Page<Deal> findAll(Integer customerId, Pageable pageable);
}
