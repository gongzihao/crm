package cn.hd01.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.hd01.repository.entity.Staff;

public interface StaffService extends BaseService<Staff, Integer> {
	/**
	 * @Cacheable
	 * 
	 * @param oauthId
	 *            认证id
	 * @return
	 */
	public Staff findByOauthId(Integer oauthId);

	public Staff findByNameAndPassword(String name, String password);

	public boolean exists(String phone);

	public List<Staff> findByType(String type);

	public Page<Staff> find(Staff u, Pageable pageable);
}
