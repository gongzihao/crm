package cn.hd01.service;

import cn.hd01.repository.entity.Order;

/**
 * @author cyb
 * @date 2017/4/26.
 */
public interface OrderService extends BaseService<Order, String> {

    boolean updateStatusByOrderId(Integer status, String orderId);

    Order findByUserIdAndType(Integer userId, Integer type);

    Order findByTypeAndProductId(Integer type, String productId);

    Order findByOrderId(String orderId);
}
