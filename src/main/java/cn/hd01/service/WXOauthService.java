package cn.hd01.service;

import cn.hd01.repository.entity.WXOauth;

/**
 * Created by beta.chen
 * Time:2017/5/14 20:47
 */
public interface WXOauthService extends BaseService<WXOauth, Integer>{
    WXOauth findByWeixinIdAndOpenId(String weixinId, String openid);
}
