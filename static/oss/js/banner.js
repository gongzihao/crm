$('.datatable-basic').DataTable({
    "columnDefs": [
		{
		    width: '100px',
		    targets: 0
		},
        {
            width: '300px',
            targets: 1
        }
    ],
    "ajax": {
        "url": contextpath + "/banner/list/" + type,
        "data": function (d) {
            var query = $("#query").serializeObject();
            query["size"] = d.length;
            query["page"] = (d.start / d.length);
            return query;
        },
        "dataFilter": function (data) {
            var json = jQuery.parseJSON(data);
            json.recordsTotal = json.totalElements;
            json.recordsFiltered = json.totalElements;
            json.data = json.content;

            return JSON.stringify(json);
        }
    },
    "columns": [
        {
            render: function (data, type, row, meta) {
                return meta.row + 1;
            }
        },
        {
            render: function (data, type, row, meta) {
            	var t = row["type"] == 1? "product":"style";
                return "<img src='/mall/upload/img/"+row["imgId"]+"' width='200px' height='120px' />";
            }
        },
        {
            render: function (data, type, row, meta) {
            	return row["type"] == 1? "产品":"方案";
            }
        },
        {
            render: function (data, type, row, meta) {
            	var t = row["type"] == 1? "product":"style";
            	return '<a href="/mall/'+t+'/view?id='+row["refId"]+'" >'+'查看' +'</a>';
            }
        },
        {
            render: function (data, type, row, meta) {
            	return '<a href="###" class="seq" data-type="number" data-inputclass="form-control" data-pk="'+row["id"]+'">'+row["seq"]+'</a>';
            }
        },
        {"data": "createTime"},
        {
            render: function (data, type, row, meta) {
                return $("#btnGroup").html();
            }
        }
    ],
    "drawCallback": function (settings) {
        $(".btn-del").click(function () {
            var row = $('.datatable-basic').DataTable()
                .row($(this).closest("tr")).data();
            swal({
                     title: "删除",
                     text: "确定要删除 ?",
                     type: "warning",
                     showCancelButton: true,
                     confirmButtonColor: "#DD6B55",
                     confirmButtonText: "确定",
                     cancelButtonText: "取消",
                     closeOnConfirm: false
                 }, function () {
                $.get(contextpath + "/banner/delete/" + row.id,
                      function (d) {
                          $('.datatable-basic').DataTable().draw();
                          swal("删除成功", "", "success");
                      })
            });

        });
        
        $('.seq').editable({
            url: contextpath+'/banner/seq',
            title: '顺序值越小越靠前'
        });
    }
});

