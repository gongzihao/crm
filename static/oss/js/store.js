$('.datatable-basic').DataTable({
	"columnDefs": [
         {
             orderable: false,
             width: '150px',
             targets: 4
         }
     ],
	 "ajax" : {
		"url" : contextpath + "/store/list",
		"data" : function(d) {
            var query = $("#query").serializeObject();
            query["size"] = d.length;
            query["page"] = (d.start/d.length);
			return query;
		},
		"dataFilter": function(data){
            var json = jQuery.parseJSON( data );
            json.recordsTotal = json.totalElements;
            json.recordsFiltered = json.totalElements;
            json.data = json.content;
 
            return JSON.stringify( json );
        }
	 },
	 "columns": [
		 { 
        	 render:function(data,type, row, meta){
        		 return '<a href="###" data-popup="tooltip" title="'+row["comment"] +'" data-placement="bottom">'+row["name"] +'</a>'
        	 }
         },
         { "data": "phone" },
         { "data": "address" },
         { "data": "manager" },
         {
            render : function(data,type, row, meta) {
                return $("#btnGroup").html();
            }
        }
     ],
     "drawCallback": function( settings ) {
		 $(".btn-edit").click(function(){
			 resetForm();
		    $("#edit").fill($('.datatable-basic').DataTable().row($(this).closest("tr")).data());
		 });
		 
		 $(".btn-del").click(function(){
				var row = $('.datatable-basic').DataTable().row($(this).closest("tr")).data();
				swal({
					title : "删除",
					text : "确定要删除门店 "+row.name +" ?",
					type : "warning",
					showCancelButton : true,
					confirmButtonColor : "#DD6B55",
					confirmButtonText : "确定",
					cancelButtonText: "取消",
					closeOnConfirm : false
				},function() {
					$.get(contextpath + "store/delete/" + row.id, function(d){
						$('.datatable-basic').DataTable().draw();
						swal("删除门店成功","", "success");
					})
				});
				
			 });
		 
		 $('[data-popup=tooltip]').tooltip();
      }
});

function resetForm(){
	$("#edit").validate().resetForm();
	$("#edit")[0].reset();
}

$(function(){
	
	$(".btn-add").click(resetForm);
	 
	$("#submitEdit").click(function(){
		if($("#edit").valid()){
			$.post(contextpath+"store/save", $("#edit").serialize(), function(){
				$('.datatable-basic').DataTable().draw();
				swal("编辑门店成功", "", "success");
			});
			$('.close').click();
		}
	});
	 
	$("#submit").click(function(){
		$('.datatable-basic').DataTable().draw();
	});

	$('select').select2({
	    minimumResultsForSearch: Infinity,
	});	
	
})
