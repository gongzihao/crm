
function resetForm(){
	$("#edit").validate().reset();
	$("#edit")[0].reset();
	$(".select").trigger("change");
}

$(function(){
	$("#edit").validate();
	$("#btnReset").click(resetForm);
	
	$("#submitEdit").click(function(){
		if($("#edit").valid()){
			$.post(contextpath+"customer/save", $("#edit").serialize(), function(){
				swal("客户资料保存成功", "", "success");
				resetForm();
			})
		}
	});

	$('select').select2({
	    minimumResultsForSearch: Infinity
	});	

	$('#birthday').daterangepicker({
		"autoApply" : true,
		"singleDatePicker": true,
		"parentEl": "#edit_dialog",
		"locale" : {
			format : 'YYYYMMDD'
		}
	});
	
	// Setup all runtimes
	 var option = {
        url: contextpath+'customer/upload/',
        chunk_size: 0,
        filters: {
            max_file_size: '20MB',
            mime_types: [{
                title: "excel 文件",
                extensions: "xls,txt"
            }],
            prevent_duplicates : true
        },
        init:{
        	UploadComplete: function(up, files) {
        		$("#uploadinfo").html("所有文件上传完成，可以在‘导入进度’页面查看进度， <a class='text-danger' id='continue'>点击此处</a>继续上传");
        		$("#continue").click(function(){
        			$("#uploadinfo").hide();
        			$("#info").show();
        			$(".file-uploader").pluploadQueue(option);
        		})
            },
            FileUploaded : function(uploader,file,respnse){
            	$("#uploadinfo").text("文件" + file.name + " 上传成功");
            },
            Error:function(uploader,errObject){
            	$("#uploadinfo").text("文件上传失败");
            },
            UploadFile:function(uploader,file){
            	$("#info").hide();
            	$("#uploadinfo").show();
            	$("#uploadinfo").text("开始上传文件" + file.name);
            }
            
        }
    }
	 
    $(".file-uploader").pluploadQueue(option);
	
})
