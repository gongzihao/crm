$('.datatable-basic').DataTable({
    "columnDefs": [
        {
            width: '200px',
            targets: 3
        }
    ],
    "ajax": {
        "url": contextpath + "/vip/list",
        "dataFilter": function (data) {
            var json = jQuery.parseJSON(data);
            json.recordsTotal = json.totalElements;
            json.recordsFiltered = json.totalElements;
            json.data = json.content;

            return JSON.stringify(json);
        }
    },
    "columns": [
        {"data": "level"},
        {"data": "name"},
        {"data": "requirePoint"},
        {"data": "discount"},
        {
            render: function (data, type, row, meta) {
                return $("#btnGroup").html();
            }
        }
    ],
    "drawCallback": function (settings) {
        $(".btn-edit").click(function () {
            $("#edit").fill($('.datatable-basic').DataTable().row($(this).closest("tr")).data());
        });

        $(".btn-del").click(function () {
            var row = $('.datatable-basic').DataTable().row($(this).closest("tr")).data();
            swal({
                title: "删除",
                text: "确定要删除会员等级 " + row.name + " ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确定",
                cancelButtonText: "取消",
                closeOnConfirm: false
            }, function () {
                $.get(contextpath + "vip/delete/" + row.id, function (d) {
                    $('.datatable-basic').DataTable().draw();
                    swal("删除会员等级成功", "", "success");
                })
            });

        });

        $('[data-popup=tooltip]').tooltip();
    }
});

$(function () {

    $(".btn-add").click(function () {
        $("#edit").validate().resetForm();
        $("#edit")[0].reset();
    });

    $("#submitEdit").click(function () {
        if ($("#edit").valid()) {
            $.post(contextpath + "vip/save", $("#edit").serialize(), function () {
                $('.datatable-basic').DataTable().draw();
                swal("编辑会员等级成功", "", "success");
            })
            $('.close').click();
        }
    });

    $("#submit").click(function () {
        $('.datatable-basic').DataTable().draw();
    });

    $('select').select2({
        minimumResultsForSearch: Infinity
    });

})