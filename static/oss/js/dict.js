$('.datatable-basic').DataTable({
	"columnDefs": [
         {
             width: '200px',
             targets: 3
         }
     ],
	 "ajax" : {
		"url" : contextpath + "/dict/list",
		"data" : function(d) {
            var query = $("#query").serializeObject();
            query["size"] = d.length;
            query["page"] = (d.start/d.length);
			return query;
		},
		"dataFilter": function(data){
            var json = jQuery.parseJSON( data );
            json.recordsTotal = json.totalElements;
            json.recordsFiltered = json.totalElements;
            json.data = json.content;
 
            return JSON.stringify( json );
        }
	 },
	 "columns": [
	     { "data": "type" },
         { "data": "name" },
         { 
        	 render:function(data,type, row, meta){
        		 return '<a href="###" data-popup="tooltip" title="'+row["comment"] +'" data-placement="bottom">'+row["value"] +'</a>'
        	 }
         },
         {
            render : function(data,type, row, meta) {
            	if(row["readOnly"]){
            		return "";
            	}
            	return $("#btnGroup").html();
            }
        }
     ],
     "drawCallback": function( settings ) {
		 $(".btn-edit").click(function(){
		    $("#edit").fill($('.datatable-basic').DataTable().row($(this).closest("tr")).data());
		    $(".select").trigger("change");
		 });
		 
		 $(".btn-del").click(function(){
				var row = $('.datatable-basic').DataTable().row($(this).closest("tr")).data();
				swal({
					title : "删除",
					text : "确定要删除配置 "+row.name + " " + row.value +" ?",
					type : "warning",
					showCancelButton : true,
					confirmButtonColor : "#DD6B55",
					confirmButtonText : "确定",
					cancelButtonText: "取消",
					closeOnConfirm : false
				},function() {
					$.get(contextpath + "dict/delete/" + row.id, function(d){
						$('.datatable-basic').DataTable().draw();
						swal("删除配置成功","", "success");
					})
				});
				
			 });
		 
		 $('[data-popup=tooltip]').tooltip();
      }
});

$(function(){
	
	$(".btn-add").click(function(){
		$("#edit").validate().resetForm();
		$("#edit")[0].reset();
	});
	 
	$("#submitEdit").click(function(){
		if($("#edit").valid()){
			$.post(contextpath+"dict/save", $("#edit").serialize(), function(){
				$('.datatable-basic').DataTable().draw();
				swal("编辑配置成功", "", "success");
			})
			$('.close').click();
		}
	});
	 
	$("#submit").click(function(){
		$('.datatable-basic').DataTable().draw();
	});

	$('select').select2({
	    minimumResultsForSearch: Infinity
	});	
	
})
