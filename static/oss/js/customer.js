$('.datatable-basic').DataTable({
	 "ajax" : {
		"url" : contextpath + "/customer/list",
		"data" : function(d) {
            var query = $("#query").serializeObject();
            query["size"] = d.length;
            query["page"] = (d.start/d.length);
			return query;
		},
		"dataFilter": function(data){
            var json = jQuery.parseJSON( data );
            json.recordsTotal = json.totalElements;
            json.recordsFiltered = json.totalElements;
            json.data = json.content;
 
            return JSON.stringify( json );
        }
	 },
	 "columns": [
		 {
            render : function(data,type, row, meta) {
                return '<a href="'+contextpath+'customer/detail/'+row["id"]+'">'+row["name"]+'</a>';
            }
	     },
         { "data": "phone" },
         { "data": "weixin" },
         { "data": "address" },
         { "data": "hospital" },
         { "data": "birthday" },
         { "data": "sex" },
         { "data": "store" },
         { "data": "source" },
         { "data": "creatTime" }
         
     ],
     "drawCallback": function( settings ) {
		 $(".btn-detail").click(function(){
		    console.log($('.datatable-basic').DataTable().row($(this).closest("tr")).data());
		 });
      }
});

$(function(){
	
	$(".btn-add").click(function(){
		$("#edit")[0].reset();
	});
	 
	$("#submitEdit").click(function(){
		$.post(contextpath+"customer/save", $("#edit").serialize(), function(){
			$('.datatable-basic').DataTable().draw();
			$('.close').click();
		})
	});
	 
	$("#submit").click(function(){
		$('.datatable-basic').DataTable().draw();
	});

	$('select').select2({
	    minimumResultsForSearch: Infinity
	});	

	$('#creatTimeRange').daterangepicker({
		"timePicker" : true,
		"timePicker24Hour" : true,
		"timePickerSeconds" : true,
		"autoApply" : true,
		"locale" : {
			format : 'YYYY/MM/DD HH:mm:ss'
		}
	}, function(start, end, label) {
		$("#beginCreateTime").val(start.format('YYYYMMDDHHmmss'));
		$("#endCreateTime").val(end.format('YYYYMMDDHHmmss'));
	});
	
	$('#birthday').daterangepicker({
		"autoApply" : true,
		"singleDatePicker": true,
		"parentEl": "#edit_dialog",
		"locale" : {
			format : 'YYYYMMDD'
		}
	});
	
})
