$('.datatable-basic').DataTable({
	"columnDefs": [
         {
             width: '150px',
             targets: 5
         }
     ],
	 "ajax" : {
		"url" : contextpath + "/telreserve/"+$("#customerId").val()+"/list",
		"data" : function(d) {
            var query = {};
            query["size"] = d.length;
            query["page"] = (d.start/d.length);
			return query;
		},
		"dataFilter": function(data){
            var json = jQuery.parseJSON( data );
            json.recordsTotal = json.totalElements;
            json.recordsFiltered = json.totalElements;
            json.data = json.content;
 
            return JSON.stringify( json );
        }
	 },
	 "columns": [
         { "data": "customerName" },
         { "data": "agree" },
         { "data": "reserveTime" },
         { "data": "staffName" },
         { "data": "state" },
         { "data": "comment" },
         {
            render : function(data,type, row, meta) {
                if(row["customerFeedback"]){
                	return "<a href='###' id='customerFeedback' data-toggle='modal' data-target='#cf_dialog'>查看</a>"
                }
                return "";
            }
         },
         {
             render : function(data,type, row, meta) {
            	 if(row["staffFeedback"]){
                 	return "<a href='###' id='staffFeedback' data-toggle='modal' data-target='#sf_dialog'>查看</a>"
                 }
                 return "";
             }
          }
     ],
     "drawCallback": function( settings ) {
    	 $("#customerFeedback").click(function(){
 		    var data = $('.datatable-basic').DataTable().row($(this).closest("tr")).data();
 		    var url = contextpath+"telreserve/customerFeedback/" + data['id'];
 		    $.get(url, function(d){
 		    	var path = $("#path").val();
 		    	$("#score").raty({ readOnly: true, score: d['score'], "path":path });
 		    	$("#satisfaction").raty({ readOnly: true, score: d['satisfaction'], "path":path })
 		    	$("#clothing").raty({ readOnly: true, score: d['clothing'], "path":path })
 		    	$("#communication").raty({ readOnly: true, score: d['communication'], "path":path })
 		    	$("#introduction").raty({ readOnly: true, score: d['introduction'], "path":path })
 		    	$("#professional").raty({ readOnly: true, score: d['professional'], "path":path })
 		    	$("#comment").html(d['comment'])
 		    })
 		 });
    	 $("#staffFeedback").click(function(){
  		    var data = $('.datatable-basic').DataTable().row($(this).closest("tr")).data();
  		    var url = contextpath+"telreserve/staffFeedback/" + data['id'];
  		    $.get(url, function(d){
  		    	$.each(d, function(k,v){
  		    		$("#sf_dialog").find("#"+k).html(v);
  		    	});
  		    })
  		 });
      }
});

$(function(){
	
	$(".btn-add").click(function(){
		$("#edit")[0].reset();
		$(".select").trigger("change");
	});
	 
	$("#submitEdit").click(function(){
		$.post(contextpath+"telreserve/save", $("#edit").serialize(), function(){
			$('.datatable-basic').DataTable().draw();
			$('.close').click();
			swal("新增用户成功","", "success");
		})
	});
	 
	$('select').select2({
	    minimumResultsForSearch: Infinity
	});
	
	$('select:first').change(function(){
		$(this).val()=='true' ? $("#detail").show() : $("#detail").hide();
	})
	
	$('#reserveTime').daterangepicker({
		"timePicker" : true,
		"timePicker24Hour" : true,
		"timePickerSeconds" : true,
		"singleDatePicker": true,
		"parentEl": "#edit_dialog",
		"locale" : {
			format : 'YYYY-MM-DD HH:mm:ss'
		}
	});
	
})
