$('.datatable-basic').DataTable({
    "columnDefs": [
        {
            width: '250px',
            targets: 0
        }
    ],
    "ajax" : {
        "url" : contextpath + "requirement/my/list",
        "data" : function(d) {
            var query = $("#query").serializeObject();
            query["size"] = d.length;
            query["page"] = (d.start/d.length);
            return query;
        },
        "dataFilter": function(data){
            var json = jQuery.parseJSON( data );
            json.recordsTotal = json.totalElements;
            json.recordsFiltered = json.totalElements;
            json.data = json.content;

            return JSON.stringify( json );
        }
    },
    "columns": [
        {
            render: function (data, type, row, meta) {
                if (row["title"].length > 20){
                    var shortTitle = row["title"].substring(0, 15) + "...";
                    return '<a href="###" data-popup="tooltip" title="'+row["title"] +'" data-placement="bottom">'+shortTitle +'</a>';
                }
                return '<a href="###" data-popup="tooltip" title="'+row["title"] +'" data-placement="bottom">'+row["title"] +'</a>';
            }
        },
        {
            render: function (data, type, row, meta) {
                for (var i = 0; i < majors.length; i++) {
                    if (row["industry"] == majors[i].value) {
                        return majors[i].name;
                    }
                }
                return "未知";
            }
        },
        {
            render: function (data, type, row, meta) {
                for (var i = 0; i < styles.length; i++) {
                    if (row["style"] == styles[i].value) {
                        return styles[i].name;
                    }
                }
                return "未知";
            }
        },
        {"data": "size"},
        {"data": "budget"},
        {"data": "price"},
        {"data": "qq"},
        {"data": "weixin"},
        {
            render: function(data, type, row, meta){
                return row["province"] + row["city"] + row["district"];
            }
        },
        {"data": "createTime"},
        {
            render: function (data, type, row, meta) {
                for (var i = 0; i < statusList.length; i++) {
                    if (row["status"] == statusList[i].value) {
                        return '<a href="###" data-popup="tooltip" title="'+statusList[i].comment +'" data-placement="bottom">'+statusList[i].name +'</a>';
                    }
                }
                return "未知";
            }
        },
        {"data": "designer"},
        {
            render: function (data, type, row, meta) {
                var content = $("#btnGroup").html();

                if (row["status"] == 0){
                    content += "<button type='button' class='btn bg-primary btn-icon  btn-pay'>支付定金</button>";
                    content += "<button type='button' class='btn bg-danger btn-icon  btn-del'>删除</button>";
                // } else if (row["status"] == 4){
                //     content += "<button type='button' class='btn bg-success btn-icon  btn-received'>验收</button>";
                } else if (row["status"] == 5){
                    content += "<button type='button' class='btn bg-success btn-icon  btn-balance'>支付尾款</button>";
                }
                return content;
            }
        }
    ],
    "drawCallback": function( settings ) {
        $(".btn-edit").click(function(){
            var row = $('.datatable-basic').DataTable().row($(this).closest("tr")).data();

            window.location.href = contextpath + "requirement/my/detail/" + row.id;

            // $("#edit").validate().resetForm();
            // $("#uploadDiv").empty();
            // var data = $('.datatable-basic').DataTable().row($(this).closest("tr")).data();
            // $.get(contextpath + "upload/get/requirement/"+ data.id, function (result) {
            //     for (u in result){
            //         $("#uploadDiv").append("<a target='_blank' href='"+ contextpath + result[u].url+"'><span>"+result[u].fileName+"</span></a><br/>");
            //     }
            // });
            //
            // $("#commitDiv").empty();
            // $.get(contextpath + "upload/get/requirement_product/"+ data.id, function (result) {
            //     for (u in result){
            //         $("#commitDiv").append("<a target='_blank' href='"+ contextpath + result[u].url+"'><span>"+result[u].fileName+"</span></a><br/>");
            //     }
            // });
            // $("#edit").fill(data);
            // $(".select").trigger("change");
        });

        $(".btn-del").click(function(){
            var row = $('.datatable-basic').DataTable().row($(this).closest("tr")).data();
            swal({
                title : "删除",
                text : "确定删除 "+row.title+" ?",
                type : "warning",
                showCancelButton : true,
                confirmButtonColor : "#DD6B55",
                confirmButtonText : "确定",
                cancelButtonText: "取消",
                closeOnConfirm : false
            },function() {
                $.get(contextpath + "requirement/delete/" + row.id, function(d){
                    $('.datatable-basic').DataTable().draw();
                    swal("删除成功","", "success");
                })
            });

        });

        // $(".btn-received").click(function () {
        //     var row = $('.datatable-basic').DataTable().row($(this).closest("tr")).data();
        //     swal({
        //         title : "验收订单",
        //         text : "确定验收 "+row.title+" ?",
        //         type : "warning",
        //         showCancelButton : true,
        //         confirmButtonColor : "#DD6B55",
        //         confirmButtonText : "确定",
        //         cancelButtonText: "取消",
        //         closeOnConfirm : false
        //     },function() {
        //         $.post(contextpath + "requirement/received/" + row.id, function(d){
        //             $('.datatable-basic').DataTable().draw();
        //             swal("验收成功","", "success");
        //         })
        //     });
        // });

        $(".btn-pay").click(function () {
            var row = $('.datatable-basic').DataTable().row($(this).closest("tr")).data();
            $("#sumYuan").text(row.deposits);
            swal({
                title : "支付定金",
                text : "确定支付定金 <span style='color:#F44336;font-size: large;font-weight: bold'>"+row.deposits+"</span>元 ?",
                type : "warning",
                showCancelButton : true,
                confirmButtonColor : "#DD6B55",
                confirmButtonText : "确定",
                cancelButtonText: "取消",
                closeOnConfirm : false,
                html: true
            },function() {
                $.post(contextpath + "requirement/payDeposits/" + row.id, function(data){
                    if (data.code == 1){
                        swal.close();
                        $("#qrcodeImage").attr("src", data.msg);
                        $('#qrcode_dialog').modal('show');
                    } else{
                        swal(data.msg,"", "error");
                    }

                })
            });
        });

        $(".btn-balance").click(function () {
            var row = $('.datatable-basic').DataTable().row($(this).closest("tr")).data();
            var sum = (row.price - row.deposits).toFixed(2);
            $("#sumYuan").text(sum);
            swal({
                title : "支付尾款",
                text : "确定支付尾款 <span style='color:#F44336;font-size: large;font-weight: bold'>"+ sum +"</span>元 ?",
                type : "warning",
                showCancelButton : true,
                confirmButtonColor : "#DD6B55",
                confirmButtonText : "确定",
                cancelButtonText: "取消",
                closeOnConfirm : false,
                html: true
            },function() {
                $.post(contextpath + "requirement/payBalance/" + row.id, function(data){
                    if (data.code == 1){
                        swal.close();
                        $("#qrcodeImage").attr("src", data.msg);
                        $('#qrcode_dialog').modal('show');
                    } else{
                        swal(data.msg,"", "error");
                    }

                })
            });
        });

        $('[data-popup=tooltip]').tooltip();
    }
});

$(function(){

    // $(".btn-add").click(function(){
    //     $("#edit").validate().resetForm();
    //     $("#edit")[0].reset();
    // });
    //
    // $("#submitEdit").click(function(){
    //     if($("#edit").valid()){
    //         $.post(contextpath+"requirement/my/save", $("#edit").serialize(), function(){
    //             $('.datatable-basic').DataTable().draw();
    //             $('.close').click();
    //             swal("保存成功","", "success");
    //         })
    //     }
    // });

    $("#querySubmit").click(function(){
        $('.datatable-basic').DataTable().draw();
    });
    
    $("#payClose").click(function () {
        $('.datatable-basic').DataTable().draw();
    })

    $(":input").inputmask();

    $('.select').select2({
        minimumResultsForSearch: Infinity
    });

    $("#distpicker").distpicker({
        placeholder: false,
        province: "广东省",
        city: "深圳市",
        district: "南山区"
    });
})
