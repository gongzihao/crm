$(function () {
    $('.btn-back').click(function () {
        window.history.back();
    });

    $('.select').select2({
        minimumResultsForSearch: Infinity
    });

    var from = 0;
    var values = new Array("开始");
    for(var i = 0; i < flows.length; i++){
        values[i+1] = flows[i].name;

        if (lastFlow != null){
            if (lastFlow.name == flows[i].name){
                from = i + 1;
            }
        }
    }

    var $flow = $("#ion-flow");
    var disable = !(lastFlow == null || r.designerId != lastFlow.adderId);
    $('.btn-next').attr("disabled",disable);

    $flow.ionRangeSlider({
        grid: true,
        grid_num: 6,
        from: from,
        from_min: from,
        from_max: from +1,
        hide_min_max: true,
        values: values,
        disable: disable,
        onFinish: function (obj) {
            console.log(obj);
            if (obj.from == from){
                slider.update({from:from});
                return;
            }

            swal({
                title : '提交进度',
                text  : '<span class="text-danger text-size-large text-bold">' + obj.from_value + '</span> 完成?',
                type  : 'warning',
                showCancelButton : true,
                confirmButtonColor : "#DD6B55",
                confirmButtonText : "确定",
                cancelButtonText: "取消",
                closeOnConfirm : false,
                html: true
            }, confirmFlow);
        }
    });

    var slider = $("#ion-flow").data("ionRangeSlider");

    confirmFlow = function(isConfirm) {
        if (isConfirm){
            $.post(contextpath + "requirement/flow/" + $('#rId').val(), function (data) {
                slider.update({disable:true});
                $('.btn-next').attr("disabled",true);
                if (data.code == 1){
                    swal(data.msg, "", "success");
                } else {
                    swal(data.msg, "", "error");
                }
            })
        } else{
            slider.update({from:from});
        }
    }

    $('.btn-next').click(function () {
        swal({
            title : '提交进度',
            text  : '<span class="text-danger text-size-large text-bold">' + values[from+1] + '</span> 完成?',
            type  : 'warning',
            showCancelButton : true,
            confirmButtonColor : "#DD6B55",
            confirmButtonText : "确定",
            cancelButtonText: "取消",
            closeOnConfirm : false,
            html: true
        }, function (isConfirm) {
            if (isConfirm){
                $.post(contextpath + "requirement/flow/" + $('#rId').val(), function (data) {
                    $('.btn-next').attr("disabled",true);
                    if (data.code == 1){
                        slider.update({disable:true, from:from+1});
                        swal(data.msg, "", "success");
                    } else {
                        swal(data.msg, "", "error");
                    }
                })
            }
        });
    })
});