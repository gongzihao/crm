$('.datatable-basic').DataTable({
    "columnDefs": [
        {
            width: '250px',
            targets: 1
        }
    ],
    "ajax" : {
        "url" : contextpath + "requirement/exam/list",
        "data" : function(d) {
            var query = $("#query").serializeObject();
            query["size"] = d.length;
            query["page"] = (d.start/d.length);
            return query;
        },
        "dataFilter": function(data){
            var json = jQuery.parseJSON( data );
            json.recordsTotal = json.totalElements;
            json.recordsFiltered = json.totalElements;
            json.data = json.content;

            return JSON.stringify( json );
        }
    },
    "columns": [
        {"data": "ownerName"},
        {
            render: function (data, type, row, meta) {
                if (row["title"].length > 20){
                    var shortTitle = row["title"].substring(0, 15) + "...";
                    return '<a href="###" data-popup="tooltip" title="'+row["title"] +'" data-placement="bottom">'+shortTitle +'</a>';
                }
                return '<a href="###" data-popup="tooltip" title="'+row["title"] +'" data-placement="bottom">'+row["title"] +'</a>';
            }
        },
        {
            render: function (data, type, row, meta) {
                for (var i = 0; i < majors.length; i++) {
                    if (row["industry"] == majors[i].value) {
                        return majors[i].name;
                    }
                }
                return "未知";
            }
        },
        {
            render: function (data, type, row, meta) {
                for (var i = 0; i < styles.length; i++) {
                    if (row["style"] == styles[i].value) {
                        return styles[i].name;
                    }
                }
                return "未知";
            }
        },
        {"data": "size"},
        {"data": "budget"},
        {"data": "qq"},
        {"data": "weixin"},
        {
            render: function(data, type, row, meta){
                return row["province"] + row["city"] + row["district"];
            }
        },
        {"data": "createTime"},
        {
            render: function (data, type, row, meta) {
                for (var i = 0; i < statusList.length; i++) {
                    if (row["status"] == statusList[i].value) {
                        return '<a href="###" data-popup="tooltip" title="'+statusList[i].comment +'" data-placement="bottom">'+statusList[i].name +'</a>';
                    }
                }
                return "未知";
            }
        },
        {
            render: function (data, type, row, meta) {
                return "<a href=" + contextpath +"requirement/exam/detail/" + row["id"] + " class='btn bg-info btn-icon'>审核</a>";
                // return $("#btnGroup").html();
            }
        }
    ],
    "drawCallback": function( settings ) {
        $('[data-popup=tooltip]').tooltip();
    }
});

$('.datatable-basic-all').DataTable({
    "columnDefs": [
        {
            width: '250px',
            targets: 1
        }
    ],
    "ajax" : {
        "url" : contextpath + "requirement/allList",
        "data" : function(d) {
            var query = $("#queryAllForm").serializeObject();
            query["size"] = d.length;
            query["page"] = (d.start/d.length);
            return query;
        },
        "dataFilter": function(data){
            var json = jQuery.parseJSON( data );
            json.recordsTotal = json.totalElements;
            json.recordsFiltered = json.totalElements;
            json.data = json.content;

            return JSON.stringify( json );
        }
    },
    "columns": [
        {"data": "ownerName"},
        {
            render: function (data, type, row, meta) {
                if (row["title"].length > 20){
                    var shortTitle = row["title"].substring(0, 15) + "...";
                    return '<a href="###" data-popup="tooltip" title="'+row["title"] +'" data-placement="bottom">'+shortTitle +'</a>';
                }
                return '<a href="###" data-popup="tooltip" title="'+row["title"] +'" data-placement="bottom">'+row["title"] +'</a>';
            }
        },
        {
            render: function (data, type, row, meta) {
                for (var i = 0; i < majors.length; i++) {
                    if (row["industry"] == majors[i].value) {
                        return majors[i].name;
                    }
                }
                return "未知";
            }
        },
        {
            render: function (data, type, row, meta) {
                for (var i = 0; i < styles.length; i++) {
                    if (row["style"] == styles[i].value) {
                        return styles[i].name;
                    }
                }
                return "未知";
            }
        },
        {"data": "size"},
        {"data": "budget"},
        {"data": "price"},
        {"data": "qq"},
        {"data": "weixin"},
        {
            render: function(data, type, row, meta){
                return row["province"] + row["city"] + row["district"];
            }
        },
        {"data": "createTime"},
        {
            render: function (data, type, row, meta) {
                for (var i = 0; i < statusList.length; i++) {
                    if (row["status"] == statusList[i].value) {
                        return '<a href="###" data-popup="tooltip" title="'+statusList[i].comment +'" data-placement="bottom">'+statusList[i].name +'</a>';
                    }
                }
                return "未知";
            }
        },
        {"data": "designer"},
        {
            render: function (data, type, row, meta) {
                return $("#btnGroup").html();
            }
        }
    ],
    "drawCallback": function( settings ) {
        $(".btn-all-edit").click(function(){
            $("#edit").validate().resetForm();
            $("#uploadDiv").empty();
            var data = $('.datatable-basic-all').DataTable().row($(this).closest("tr")).data();
            $.get(contextpath + "upload/get/requirement/"+ data.id, function (result) {
                for (u in result){
                    $("#uploadDiv").append("<a target='_blank' href='"+ contextpath + result[u].url+"'><span>"+result[u].fileName+"</span></a><br/>");
                }
            });

            $("#edit").fill(data);
            $(".select").trigger("change");
        });

        $('[data-popup=tooltip]').tooltip();
    }
});

$(function(){

    $("#querySubmit").click(function(){
        $('.datatable-basic').DataTable().draw();
    });

    $("#queryAllSubmit").click(function(){
        $('.datatable-basic-all').DataTable().draw();
    });

    $('.select').select2({
        minimumResultsForSearch: Infinity
    });
})
