$('.datatable-basic').DataTable({
    "columnDefs": [
        {
            width: '250px',
            targets: 1
        }
    ],
    "ajax" : {
        "url" : contextpath + "requirement/pick/list",
        "data" : function(d) {
            var query = $("#queryMyForm").serializeObject();
            query["size"] = d.length;
            query["page"] = (d.start/d.length);
            return query;
        },
        "dataFilter": function(data){
            var json = jQuery.parseJSON( data );
            json.recordsTotal = json.totalElements;
            json.recordsFiltered = json.totalElements;
            json.data = json.content;

            return JSON.stringify( json );
        }
    },
    "columns": [
        {"data": "ownerName"},
        {
            render: function (data, type, row, meta) {
                if (row["title"].length > 20){
                    var shortTitle = row["title"].substring(0, 15) + "...";
                    return '<a href="###" data-popup="tooltip" title="'+row["title"] +'" data-placement="bottom">'+shortTitle +'</a>';
                }
                return '<a href="###" data-popup="tooltip" title="'+row["title"] +'" data-placement="bottom">'+row["title"] +'</a>';
            }
        },
        {
            render: function (data, type, row, meta) {
                for (var i = 0; i < majors.length; i++) {
                    if (row["industry"] == majors[i].value) {
                        return majors[i].name;
                    }
                }
                return "未知";
            }
        },
        {
            render: function (data, type, row, meta) {
                for (var i = 0; i < styles.length; i++) {
                    if (row["style"] == styles[i].value) {
                        return styles[i].name;
                    }
                }
                return "未知";
            }
        },
        {"data": "size"},
        {"data": "budget"},
        {"data": "price"},
        {"data": "qq"},
        {"data": "weixin"},
        {
            render: function(data, type, row, meta){
                return row["province"] + row["city"] + row["district"];
            }
        },
        {"data": "createTime"},
        // {"data": "designer"},
        {"data": "dealTime"},
        {
            render: function (data, type, row, meta) {
                for (var i = 0; i < statusList.length; i++) {
                    if (row["status"] == statusList[i].value) {
                        return '<a href="###" data-popup="tooltip" title="'+statusList[i].comment +'" data-placement="bottom">'+statusList[i].name +'</a>';
                    }
                }
                return "未知";
            }
        },
        {
            render: function (data, type, row, meta) {
                var content = $("#myGroup").html();

                if (row["status"] == 4) {
                    content += $("#processGroup").html();
                }

                return content;
            }
        }
    ],
    "drawCallback": function( settings ) {
        $(".btn-my-edit").click(function(){
            var row = $('.datatable-basic').DataTable().row($(this).closest("tr")).data();

            window.location.href = contextpath + "requirement/pick/detail/" + row.id;

            // $("#edit").validate().resetForm();
            // $("#uploadDiv").empty();
            // var data = $('.datatable-basic').DataTable().row($(this).closest("tr")).data();
            // $.get(contextpath + "upload/get/requirement/"+ data.id, function (result) {
            //     for (u in result){
            //         $("#uploadDiv").append("<a target='_blank' href='"+ contextpath + result[u].url+"'><span>"+result[u].fileName+"</span></a><br/>");
            //     }
            // });
            //
            // $("#commitDiv").empty();
            // $.get(contextpath + "upload/get/requirement_product/"+ data.id, function (result) {
            //     for (u in result){
            //         $("#commitDiv").append("<a target='_blank' href='"+ contextpath + result[u].url+"'><span>"+result[u].fileName+"</span></a><br/>");
            //     }
            // });
            //
            // $("#edit").fill(data);
            // $(".select").trigger("change");
        });

        $(".btn-upload").click(function () {
            var data = $('.datatable-basic').DataTable().row($(this).closest("tr")).data();
            $("#rId").val(data.id);
            initplupload(data.id);
        });

        $('[data-popup=tooltip]').tooltip();
    }
});

$('.datatable-basic-all').DataTable({
    "columnDefs": [
        {
            width: '250px',
            targets: 1
        }
    ],
    "ajax" : {
        "url" : contextpath + "requirement/pick/allList",
        "data" : function(d) {
            var query = $("#queryAllForm").serializeObject();
            query["size"] = d.length;
            query["page"] = (d.start/d.length);
            return query;
        },
        "dataFilter": function(data){
            var json = jQuery.parseJSON( data );
            json.recordsTotal = json.totalElements;
            json.recordsFiltered = json.totalElements;
            json.data = json.content;

            return JSON.stringify( json );
        }
    },
    "columns": [
        {"data": "ownerName"},
        {
            render: function (data, type, row, meta) {
                if (row["title"].length > 20){
                    var shortTitle = row["title"].substring(0, 15) + "...";
                    return '<a href="###" data-popup="tooltip" title="'+row["title"] +'" data-placement="bottom">'+shortTitle +'</a>';
                }
                return '<a href="###" data-popup="tooltip" title="'+row["title"] +'" data-placement="bottom">'+row["title"] +'</a>';
            }
        },
        {
            render: function (data, type, row, meta) {
                for (var i = 0; i < majors.length; i++) {
                    if (row["industry"] == majors[i].value) {
                        return majors[i].name;
                    }
                }
                return "未知";
            }
        },
        {
            render: function (data, type, row, meta) {
                for (var i = 0; i < styles.length; i++) {
                    if (row["style"] == styles[i].value) {
                        return styles[i].name;
                    }
                }
                return "未知";
            }
        },
        {"data": "size"},
        {"data": "budget"},
        {"data": "price"},
        {"data": "qq"},
        {"data": "weixin"},
        {
            render: function(data, type, row, meta){
                return row["province"] + row["city"] + row["district"];
            }
        },
        {"data": "createTime"},
        {
            render: function (data, type, row, meta) {
                for (var i = 0; i < statusList.length; i++) {
                    if (row["status"] == statusList[i].value) {
                        return '<a href="###" data-popup="tooltip" title="'+statusList[i].comment +'" data-placement="bottom">'+statusList[i].name +'</a>';
                    }
                }
                return "未知";
            }
        },
        {
            render: function (data, type, row, meta) {
                return $("#allGroup").html();
            }
        }
    ],
    "drawCallback": function( settings ) {
        $(".btn-all-edit").click(function(){
            var row = $('.datatable-basic-all').DataTable().row($(this).closest("tr")).data();

            window.location.href = contextpath + "requirement/pick/detail/" + row.id;

            // $("#edit").validate().resetForm();
            // $("#uploadDiv").empty();
            // var data = $('.datatable-basic-all').DataTable().row($(this).closest("tr")).data();
            // $.get(contextpath + "upload/get/requirement/"+ data.id, function (result) {
            //     for (u in result){
            //         $("#uploadDiv").append("<a target='_blank' href='"+ contextpath + result[u].url+"'><span>"+result[u].fileName+"</span></a><br/>");
            //     }
            // });
            //
            // $("#edit").fill(data);
            // $(".select").trigger("change");
        });

        $(".btn-pick").click(function(){
            var data = $('.datatable-basic-all').DataTable().row($(this).closest("tr")).data();

            swal({
                title : "接单",
                text : "确定接单 "+data.title+" ?",
                type : "warning",
                showCancelButton : true,
                confirmButtonColor : "#DD6B55",
                confirmButtonText : "确定",
                cancelButtonText: "取消",
                closeOnConfirm : false
            },function() {
                $.post(contextpath + "requirement/pick/" + data.id, function (data) {
                    if (data == 1){
                        $('.datatable-basic-all').DataTable().draw();
                        swal("接单成功","", "success");
                    } else{
                        swal("接单失败","", "error");
                    }
                });
            });
        });

        $('[data-popup=tooltip]').tooltip();
    }
});

function initplupload(id){
    $(".file-uploader").pluploadQueue({
        url: contextpath + 'upload/add/requirement_product/' + id,
        chunk_size: 0,
        filters: {
            max_file_size: '20MB',
            // mime_types: [{title: "Image files", extensions: "jpg,gif,png,txt"},
            //     {title: "Zip files", extensions: "rar,zip"}],
            prevent_duplicates: true
        },
        init: {
            UploadComplete: function (up, files) {
                console.log("UploadComplete");
                console.log(files);
//                    $(".file-uploader").pluploadQueue(option);
            },
            FileUploaded: function (up, file, info) {
                console.log("FileUploaded");
                console.log(info);
            }
        }
    });
}

$(function(){

    $(".btn-add").click(function(){
        $("#edit").validate().resetForm();
        $("#edit")[0].reset();
    });

    $("#queryMy").click(function(){
        $('.datatable-basic').DataTable().draw();
    });

    $("#queryAll").click(function(){
        $('.datatable-basic-all').DataTable().draw();
    });

    $('.select').select2({
        minimumResultsForSearch: Infinity
    });

    // $("#submitEdit").click(function () {
    //     $.post(contextpath+"requirement/pick/commit", {id:$("#rId").val()}, function () {
    //         $('.datatable-basic').DataTable().draw();
    //         $('.close').click();
    //         swal("提交成功","", "success");
    //     });
    // });
})
