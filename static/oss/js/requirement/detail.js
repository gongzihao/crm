$(function(){

    $('.btn-back').click(function () {
        window.history.back();
    });

    $('.select').select2({
        minimumResultsForSearch: Infinity
    });

    var from = 0;
    var values = new Array("开始");
    for(var i = 0; i < flows.length; i++){
        values[i+1] = flows[i].name;

        if (lastFlow != null){
            if (lastFlow.name == flows[i].name){
                from = i + 1;
            }
        }
    }

    $("#ion-flow").ionRangeSlider({
        grid: true,
        grid_num: 6,
        from: from,
        from_min: from,
        from_max: from +1,
        hide_min_max: true,
        values: values,
        disable: true
    });
});
