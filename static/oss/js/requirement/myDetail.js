$(function(){

    $('.btn-back').click(function () {
        window.history.back();
    });

    $(":input").inputmask();

    $('.select').select2({
        minimumResultsForSearch: Infinity
    });

    $('#submitEdit').click(function () {
        if ($('#infoForm').valid()){
            $.post(contextpath + "requirement/my/save", $('#infoForm').serialize(), function (data) {
                if (data.code == 1){
                    swal(data.msg, '', 'success');
                } else{
                    swal(data.msg, '', 'error');
                }
            })
        }
    });

    $(".btn-upload").click(function () {
        initplupload($('#rId').val());
    });

    var from = 0;
    var values = new Array("开始");
    for(var i = 0; i < flows.length; i++){
        values[i+1] = flows[i].name;

        if (lastFlow != null){
            if (lastFlow.name == flows[i].name){
                from = i + 1;
            }
        }
    }

    var $flow = $("#ion-flow");
    var disable = lastFlow == null || r.ownerId == lastFlow.adderId;
    $('.btn-next').attr("disabled",disable);

    $flow.ionRangeSlider({
        grid: true,
        grid_num: 6,
        from: from,
        from_min: from,
        from_max: from +1,
        hide_min_max: true,
        values: values,
        disable: disable,
        onFinish: function (obj) {
            console.log(obj);
            swal({
                title : '提交进度',
                text  : '<span class="text-danger text-size-large text-bold">' + obj.from_value + '</span> 完成?',
                type  : 'warning',
                showCancelButton : true,
                confirmButtonColor : "#DD6B55",
                confirmButtonText : "确定",
                cancelButtonText: "取消",
                closeOnConfirm : false,
                html: true
            }, confirmFlow);
        }
    });

    var slider = $("#ion-flow").data("ionRangeSlider");

    confirmFlow = function(isConfirm) {
        if (isConfirm){
            $.post(contextpath + "requirement/flow/" + $('#rId').val(), function (data) {
                slider.update({disable:true});
                $('.btn-next').attr("disabled",true);
                if (data.code == 1){
                    swal(data.msg, "", "success");
                } else {
                    swal(data.msg, "", "error");
                }
            })
        } else{
            slider.update({from:from});
        }
    }

    $('.btn-next').click(function () {
        swal({
            title : '提交进度',
            text  : '<span class="text-danger text-size-large text-bold">' + values[from+1] + '</span> 完成?',
            type  : 'warning',
            showCancelButton : true,
            confirmButtonColor : "#DD6B55",
            confirmButtonText : "确定",
            cancelButtonText: "取消",
            closeOnConfirm : false,
            html: true
        }, function (isConfirm) {
            if (isConfirm){
                $.post(contextpath + "requirement/flow/" + $('#rId').val(), function (data) {
                    $('.btn-next').attr("disabled",true);
                    if (data.code == 1){
                        slider.update({disable:true, from:from+1});
                        swal(data.msg, "", "success");
                    } else {
                        swal(data.msg, "", "error");
                    }
                })
            }
        });
    })
})

function initplupload(id){
    $(".file-uploader").pluploadQueue({
        url: contextpath + 'upload/add/requirement/' + id,
        chunk_size: 0,
        filters: {
            max_file_size: '10MB',
            // mime_types: [{title: "Image files", extensions: "jpg,gif,png,txt"},
            //     {title: "Zip files", extensions: "rar,zip"}],
            prevent_duplicates: true
        },
        init: {
            UploadComplete: function (up, files) {
                console.log("UploadComplete");
                console.log(files);
//                    $(".file-uploader").pluploadQueue(option);
            },
            FileUploaded: function (up, file, info) {
                console.log("FileUploaded");
                console.log(info);

                $.get(contextpath + "upload/get/requirement/"+ $('#rId').val(), function (result) {
                    $("#uploadDiv").empty();
                    for (u in result){
                        $("#uploadDiv").append("<a target='_blank' href='"+ contextpath + result[u].url+"'><span>"+result[u].fileName+"</span></a><br/>");
                    }
                });
            }
        }
    });
}
