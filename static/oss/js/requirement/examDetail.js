$(function(){
    $("#examPick").click(function(){
        if (!$("#infoForm").valid()){
            return false;
        }

        var price = Number($("input[name='price']").val());
        if (price <= 0){
            swal("请确认输入合法的价格", "", "error");
            return;
        }

        swal({
            title : "审核通过并接单",
            text : "确定审核通过并接单?",
            type : "warning",
            showCancelButton : true,
            confirmButtonColor : "#DD6B55",
            confirmButtonText : "确定",
            cancelButtonText: "取消",
            closeOnConfirm : false
        },function() {
            exam(5, price, true);
        });
    });

    $("#examPass").click(function(){
        if (!$("#infoForm").valid()){
            return false;
        }

        var price = Number($("input[name='price']").val());
        if (price <= 0){
            swal("请确认输入合法的价格", "", "error");
            return;
        }

        swal({
            title : "审核通过",
            text : "确定审核通过?",
            type : "warning",
            showCancelButton : true,
            confirmButtonColor : "#DD6B55",
            confirmButtonText : "确定",
            cancelButtonText: "取消",
            closeOnConfirm : false
        },function() {
            exam(5, price, false);
        });
    });

    $("#examRefuse").click(function(){
        swal({
            title : "审核拒绝",
            text : "确定审核拒绝?",
            type : "warning",
            showCancelButton : true,
            confirmButtonColor : "#DD6B55",
            confirmButtonText : "确定",
            cancelButtonText: "取消",
            closeOnConfirm : false
        },function() {
            exam(7, 0, false);
        });
    });

    function exam(status, price, isPick){
        $.post(contextpath+"requirement/exam", {id: $("#rId").val(), status:status, price:price, pick: isPick}, function(data){
            // 禁用按钮
            $('.exam').attr('disabled',true);
            swal(data.msg,"", "success");
        });
    }

    $('.select').select2({
        minimumResultsForSearch: Infinity
    });
})
