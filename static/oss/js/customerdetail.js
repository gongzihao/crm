
$(function(){
	$("#submitBasic").click(function(){
		if($("#basicForm").valid()){
			$.post(contextpath+"customer/save/basic", $("#basicForm").serialize(), function(d){
				$.each(d, function(k,v){
					$("#basic_fieldset").find("#"+k).html(v);
				})
				swal("资料保存成功", "", "success");
			})
			$('.close').click();
		}
	});
	
	$("#submitChild").click(function(){
		if($("#childForm").valid()){
			$.post(contextpath+"customer/save/child", $("#childForm").serialize(), function(d){
				$.each(d, function(k,v){
					$("#child_fieldset").find("#"+k).html(v);
				})
				swal("资料保存成功", "", "success");
			})
			$('.close').click();
		}
	});
	
	$("#submitExt").click(function(){
		if($("#extForm").valid()){
			$.post(contextpath+"customer/save/ext", $("#extForm").serialize(), function(d){
				$.each(d, function(k,v){
					$("#ext_fieldset").find("#"+k).html(v);
				})
				swal("资料保存成功", "", "success");
			})
			$('.close').click();
		}
	});

	$('select').select2({
	    minimumResultsForSearch: Infinity
	});	

	$('#child_dialog #birthday').daterangepicker({
		"autoApply" : true,
		"singleDatePicker": true,
		"parentEl": "#child_dialog",
		"locale" : {
			format : 'YYYYMMDD'
		}
	});
	
})
