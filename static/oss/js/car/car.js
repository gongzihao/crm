$('.datatable-basic').DataTable({
                                    "columnDefs": [
                                        {
                                            // width: '180px',
                                            // targets: 4
                                        }
                                    ],
                                    "ajax": {
                                        "url": contextpath + "car/list",
                                        "data": function (d) {
                                            var query = $("#query").serializeObject();
                                            query["size"] = d.length;
                                            query["page"] = (d.start / d.length);
                                            return query;
                                        },
                                        "dataFilter": function (data) {
                                            var json = jQuery.parseJSON(data);
                                            json.recordsTotal = json.totalElements;
                                            json.recordsFiltered = json.totalElements;
                                            json.data = json.content;

                                            return JSON.stringify(json);
                                        }
                                    },
                                    "columns": [
                                        {
                                            render: function (data, type, row, meta) {
                                                return row["type"] == 1? "产品":"方案";
                                            }
                                        },
                                        {"data": "refName"},
                                        {"data": "createTime"},
                                        {
                                            render: function (data, type, row, meta) {
                                                return $("#btnGroup").html();
                                            }
                                        }
                                    ],

                                    "drawCallback": function (settings) {
                                        $(".btn-del").click(function () {
                                            var row = $('.datatable-basic').DataTable()
                                                .row($(this).closest("tr")).data();
                                            swal({
                                                title: "删除",
                                                text: "确定要删除 " + row.refName + " ?",
                                                type: "warning",
                                                showCancelButton: true,
                                                confirmButtonColor: "#DD6B55",
                                                confirmButtonText: "确定",
                                                cancelButtonText: "取消",
                                                closeOnConfirm: false
                                            }, function () {
                                                $.get(contextpath + "car/delete/" + row.id,
                                                    function (d) {
                                                        $('.datatable-basic').DataTable().draw();
                                                        swal("删除成功", "", "success");
                                                    })
                                            });

                                        });
                                    }
                                });

$(function () {

    $("#submit").click(function () {
        $('.datatable-basic').DataTable().draw();
    });

})