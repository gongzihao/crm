$('.datatable-basic').DataTable({
	"columnDefs": [
         {
             width: '150px',
             targets: 4
         }
     ],
	 "ajax" : {
		"url" : contextpath + "client/list",
		"data" : function(d) {
            var query = $("#query").serializeObject();
            query["size"] = d.length;
            query["page"] = (d.start/d.length);
			return query;
		},
		"dataFilter": function(data){
            var json = jQuery.parseJSON( data );
            json.recordsTotal = json.totalElements;
            json.recordsFiltered = json.totalElements;
            json.data = json.content;
 
            return JSON.stringify( json );
        }
	 },
	 "columns": [
         {"data": "name"},
         {"data": "sex"},
         {"data": "mobile"},
         {"data": "qq"},
         {"data": "weixin"},
         {"data": "email"},
         {"data": "invitateCode"},
         {"data": "city"},
         {
             render: function (data, type, row, meta) {
                 for (var i = 0; i < userTypes.length; i++) {
                     if (row["type"] == userTypes[i].value) {
                         return userTypes[i].name;
                     }
                 }
                 return "未知";
             }
         },
         {
             render: function (data, type, row, meta) {
                 for (var i = 0; i < majors.length; i++) {
                     if (row["major"] == majors[i].value) {
                         return majors[i].name;
                     }
                 }
                 return "未知";
             }
		 },
         {
             render: function (data, type, row, meta) {
                 return $("#btnGroup").html();
             }
         }
     ],
     "drawCallback": function( settings ) {
		 $(".btn-edit").click(function(){
			$("#edit").validate().resetForm();
		    $("#edit").fill($('.datatable-basic').DataTable().row($(this).closest("tr")).data());
		    $(".select").trigger("change");
		 });
		 
		 $(".btn-reset").click(function(){
			var row = $('.datatable-basic').DataTable().row($(this).closest("tr")).data();
			swal({
				title : "重置密码",
				text : "确定要重置用户 "+row.name+" 的密码?",
				type : "warning",
				showCancelButton : true,
				confirmButtonColor : "#DD6B55",
				confirmButtonText : "确定",
				cancelButtonText: "取消",
				closeOnConfirm : false
			},function() {
				$.get(contextpath + "client/resetPwd/" + row.id, function(d){
					swal("重置密码成功", d, "success");
				})
			});	
	     });
		 
		 $(".btn-del").click(function(){
			var row = $('.datatable-basic').DataTable().row($(this).closest("tr")).data();
			swal({
				title : "删除",
				text : "确定要删除用户 "+row.name+" ?",
				type : "warning",
				showCancelButton : true,
				confirmButtonColor : "#DD6B55",
				confirmButtonText : "确定",
				cancelButtonText: "取消",
				closeOnConfirm : false
			},function() {
				$.get(contextpath + "client/delete/" + row.id, function(d){
					$('.datatable-basic').DataTable().draw();
					swal("删除用户成功","", "success");
				})
			});
			
		 });
      }
});

$(function(){
	
	$(".btn-add").click(function(){
		$("#edit").validate().resetForm();
		$("#edit")[0].reset();
	});
	 
	$("#submitEdit").click(function(){
		if($("#edit").valid()){
			$.post(contextpath+"client/save", $("#edit").serialize(), function(){
				$('.datatable-basic').DataTable().draw();
				$('.close').click();
				swal("保存用户成功","", "success");
			})
		}
	});
	 
	$("#submit").click(function(){
		$('.datatable-basic').DataTable().draw();
	});

	$('select').select2({
	    minimumResultsForSearch: Infinity
	});	
	
})
