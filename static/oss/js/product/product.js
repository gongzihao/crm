$('.datatable-basic').DataTable({
                                    "columnDefs": [
                                        {
                                            // width: '180px',
                                            // targets: 4
                                        }
                                    ],
                                    "ajax": {
                                        "url": contextpath + "product/list",
                                        "data": function (d) {
                                            var query = $("#query").serializeObject();
                                            query["size"] = d.length;
                                            query["page"] = (d.start / d.length);
                                            return query;
                                        },
                                        "dataFilter": function (data) {
                                            var json = jQuery.parseJSON(data);
                                            json.recordsTotal = json.totalElements;
                                            json.recordsFiltered = json.totalElements;
                                            json.data = json.content;

                                            return JSON.stringify(json);
                                        }
                                    },
                                    "columns": [
                                        {"data": "name"},
                                        {
                                            render: function (data, type, row, meta) {
                                                for (var i = 0; i < types.length; i++) {
                                                    if (row["type"] == types[i].value) {
                                                        return types[i].name;
                                                    }
                                                }
                                                return "未知";
                                            }
                                        },
                                        {"data": "author"},
                                        {"data": "price"},
                                        {
                                            render: function (data, type, row, meta) {
                                                return row["hot"] ? "<span class='text-success'>是</span>": "<span class='text-danger'>否</span>";
                                            }
                                        },
                                        {
                                            render: function (data, type, row, meta) {
                                                return row["recommend"] ? "<span class='text-success'>是</span>": "<span class='text-danger'>否</span>";
                                            }
                                        },
                                        {"data": "createTime"},
                                        {
                                            render: function (data, type, row, meta) {
                                                return $("#btnGroup").html().setTokens({
                                                    'hotSet': row['hot']?'hide':'bg-success',
                                                    'hotCancel': !row['hot']?'hide':'bg-slate',
                                                    'recommendSet': row['recommend']?'hide':'bg-success',
                                                    'recommendCancel': !row['recommend']?'hide':'bg-slate'
                                                });
                                            }
                                        }
                                    ],

                                    "drawCallback": function (settings) {
                                        $(".btn-edit").click(function () {
                                            var row = $('.datatable-basic').DataTable().row($(this).closest("tr")).data();

                                            window.location.href = contextpath + "product/view?id=" + row.id;
                                        });

                                        $(".btn-del").click(function () {
                                            var row = $('.datatable-basic').DataTable()
                                                .row($(this).closest("tr")).data();
                                            swal({
                                                     title: "删除",
                                                     text: "确定要删除产品 " + row.name + " ?",
                                                     type: "warning",
                                                     showCancelButton: true,
                                                     confirmButtonColor: "#DD6B55",
                                                     confirmButtonText: "确定",
                                                     cancelButtonText: "取消",
                                                     closeOnConfirm: false
                                                 }, function () {
                                                $.get(contextpath + "product/delete/" + row.id,
                                                      function (d) {
                                                          $('.datatable-basic').DataTable().draw();
                                                          swal("删除产品成功", "", "success");
                                                      })
                                            });

                                        });

                                        $('.hot').click(function () {
                                            var row = $('.datatable-basic').DataTable().row($(this).closest("tr")).data();
                                            swal({
                                                title: "设置热门",
                                                text: "确定要将产品 " + row.name +" "+ $(this).text() +"?",
                                                type: "warning",
                                                showCancelButton: true,
                                                confirmButtonColor: "#DD6B55",
                                                confirmButtonText: "确定",
                                                cancelButtonText: "取消",
                                                closeOnConfirm: false
                                            }, function () {
                                                $.get(contextpath + "product/hot/" + row.id,
                                                    function () {
                                                        $('.datatable-basic').DataTable().draw();
                                                        swal("设置产品热门信息成功", "", "success");
                                                    })
                                            });

                                        });

                                        $('.recommend').click(function () {
                                            var row = $('.datatable-basic').DataTable().row($(this).closest("tr")).data();
                                            swal({
                                                title: "设置推荐",
                                                text: "确定要将产品 " + row.name +" "+ $(this).text() +"?",
                                                type: "warning",
                                                showCancelButton: true,
                                                confirmButtonColor: "#DD6B55",
                                                confirmButtonText: "确定",
                                                cancelButtonText: "取消",
                                                closeOnConfirm: false
                                            }, function () {
                                                $.get(contextpath + "product/recommend/" + row.id,
                                                    function () {
                                                        $('.datatable-basic').DataTable().draw();
                                                        swal("设置产品推荐信息成功", "", "success");
                                                    })
                                            });

                                        });
                                    }
                                });

$(function () {

    $("#submit").click(function () {
        $('.datatable-basic').DataTable().draw();
    });

})