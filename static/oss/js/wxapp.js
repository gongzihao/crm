var wxtype = {
	"0" : "订阅号",
	"1" : "服务号"
}

$('.datatable-basic').DataTable({
	"columnDefs": [
         {
             width: '100px',
             targets: 7
         }
     ],
	 "ajax" : {
		"url" : contextpath + "/wxapp/list",
		"data" : function(d) {
            var query = $("#query").serializeObject();
            query["size"] = d.length;
            query["page"] = (d.start/d.length);
			return query;
		},
		"dataFilter": function(data){
            var json = jQuery.parseJSON( data );
            json.recordsTotal = json.totalElements;
            json.recordsFiltered = json.totalElements;
            json.data = json.content;
 
            return JSON.stringify( json );
        }
	 },
	 "columns": [
         { "data": "name" },
         { "data": "weixinId" },
         { "data": "appId" },
         { "data": "appSecret" },
         { "data": "domain" },
         { "data": "token" },
         {
        	 render : function(data,type, row, meta) {
        		 return wxtype[row["type"]];
             }
         },
		 { "data": "mchId"},
         { "data": "shKey"},
         {
            render : function(data,type, row, meta) {
                return $("#btnGroup").html();
            }
        }
     ],
     "drawCallback": function( settings ) {
		 $(".btn-edit").click(function(){
			$("#edit").validate().resetForm();
		    $("#edit").fill($('.datatable-basic').DataTable().row($(this).closest("tr")).data());
		    $(".select").trigger("change");
		 });
		 
		 $(".btn-edit").click(function () {
             var row = $('.datatable-basic').DataTable().row($(this).closest("tr")).data();
             window.location.href = contextpath + "wxmenu?domain=" + row.domain;
         });
		 
		 $(".btn-del").click(function(){
			var row = $('.datatable-basic').DataTable().row($(this).closest("tr")).data();
			swal({
				title : "删除",
				text : "确定要删除微信号 "+row.name+" ?",
				type : "warning",
				showCancelButton : true,
				confirmButtonColor : "#DD6B55",
				confirmButtonText : "确定",
				cancelButtonText: "取消",
				closeOnConfirm : false
			},function() {
				$.get(contextpath + "wxapp/delete/" + row.id, function(d){
					$('.datatable-basic').DataTable().draw();
					swal("删除微信号成功","", "success");
				})
			});
			
		 });
      }
});

$(function(){
	
	$(".btn-add").click(function(){
		$("#edit").validate().resetForm();
		$("#edit")[0].reset();
	});
	 
	$("#submitEdit").click(function(){
		if($("#edit").valid()){
			$.post(contextpath+"wxapp/save", $("#edit").serialize(), function(){
				$('.datatable-basic').DataTable().draw();
				$('.close').click();
				swal("编辑微信号成功","", "success");
			})
		}
	});
	 
	$("#submit").click(function(){
		$('.datatable-basic').DataTable().draw();
	});

	$('select').select2({
	    minimumResultsForSearch: Infinity
	});	
	
})
