$('.datatable-basic').DataTable({
	"columnDefs": [
         {
             width: '150px',
             targets: 3
         }
     ],
	 "ajax" : {
		"url" : contextpath + "/storereserve/"+$("#customerId").val()+"/list",
		"data" : function(d) {
            var query = {};
            query["size"] = d.length;
            query["page"] = (d.start/d.length);
			return query;
		},
		"dataFilter": function(data){
            var json = jQuery.parseJSON( data );
            json.recordsTotal = json.totalElements;
            json.recordsFiltered = json.totalElements;
            json.data = json.content;
 
            return JSON.stringify( json );
        }
	 },
	 "columns": [
         { "data": "customerName" },
         { "data": "agree" },
         { "data": "reserveTime" },
         { "data": "comment" },
         { "data": "createTime" }
     ],
     "drawCallback": function( settings ) {
    	 
     }
});

$(function(){
	
	$(".btn-add").click(function(){
		$("#edit")[0].reset();
		$(".select").trigger("change");
	});
	 
	$("#submitEdit").click(function(){
		$.post(contextpath+"storereserve/save", $("#edit").serialize(), function(){
			$('.datatable-basic').DataTable().draw();
			$('.close').click();
			swal("新增预约成功","", "success");
		})
	});
	 
	$('select').select2({
	    minimumResultsForSearch: Infinity
	});
	
	$('select').change(function(){
		$(this).val()=='true' ? $("#detail").show() : $("#detail").hide();
	})
	
	$('#reserveTime').daterangepicker({
		"timePicker" : true,
		"timePicker24Hour" : true,
		"timePickerSeconds" : true,
		"singleDatePicker": true,
		"parentEl": "#edit_dialog",
		"locale" : {
			format : 'YYYY-MM-DD HH:mm:ss'
		}
	});
	
})
