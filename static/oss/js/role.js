$('.datatable-basic').DataTable({
	"columnDefs": [
         {
             className: 'text-overflow',
             width: '400px',
             targets: 2
         },
         {
             width: '150px',
             targets: 2
         }
     ],
	 "ajax" : {
		"url" : contextpath + "/role/list",
		"data" : function(d) {
            var query = $("#query").serializeObject();
            query["size"] = d.length;
            query["page"] = (d.start/d.length);
			return query;
		},
		"dataFilter": function(data){
            var json = jQuery.parseJSON( data );
            json.recordsTotal = json.totalElements;
            json.recordsFiltered = json.totalElements;
            json.data = json.content;
 
            return JSON.stringify( json );
        }
	 },
	 "columns": [
         { "data": "name" },
         { "data": "description" },
         {
            render : function(data,type, row, meta) {
            	if (row["readOnly"]){
            		return $("#btnGroupReadOnly").html();
				}
                return $("#btnGroup").html();
            }
         }
     ],
     "drawCallback": function( settings ) {
		 $(".btn-edit").click(function(){
		    $("#edit").fill($('.datatable-basic').DataTable().row($(this).closest("tr")).data());
		 });
		 
		 $(".btn-perm").click(function(){
		    $(".tree-default").fancytree("getTree").visit(function(node){
		       node.setSelected(false);
		    });
			
			var role = $('.datatable-basic').DataTable().row($(this).closest("tr")).data();
			$("div#perm_dialog").data("id", role.id);
			$.get(contextpath + "permissions/list/"+role.id, function(data){
				$(data).each(function(i,item){
					$(".tree-default").fancytree("getTree").getNodeByKey(item.permissionId + '').setSelected(true);
				})
			});
		 });
		 
		 $(".btn-del").click(function(){
			var row = $('.datatable-basic').DataTable().row($(this).closest("tr")).data();
			swal({
				title : "删除",
				text : "确定要角色 "+row.name+" ?",
				type : "warning",
				showCancelButton : true,
				confirmButtonColor : "#DD6B55",
				confirmButtonText : "确定",
				cancelButtonText: "取消",
				closeOnConfirm : false
			},function() {
				$.get(contextpath + "role/delete/" + row.id, function(d){
					$('.datatable-basic').DataTable().draw();
					swal("删除角色成功","", "success");
				})
			});
		});
      }
});

$(function(){
	
	$('#perm_dialog').on('show.bs.modal', function() {
		$("ul.fancytree-container").click();
    });
	
	$(".btn-add").click(function(){
		$("#edit").validate().resetForm();
		$("#edit")[0].reset();
	});
	 
	$("#submitEdit").click(function(){
		if($("#edit").valid()){
			$.post(contextpath+"role/save", $("#edit").serialize(), function(){
				$('.datatable-basic').DataTable().draw();
			})
			$('.close').click();
		}
	});
	
	$("#submitPerm").click(function(){
		var role = $("div#perm_dialog").data("id");
		
		var pIds = [];
		$($(".tree-default").fancytree("getTree").getSelectedNodes()).each(function(i,d){
			pIds.push(d.key);
		})
		
		$.post(contextpath+"permissions/save/" + role, {"pIds":pIds}, function(d){
			$('div#perm_dialog .close').click();
			swal("成功设置权限", "", "success");
		})		
	});
	 
	$("#submit").click(function(){
		$('.datatable-basic').DataTable().draw();
	});

	$('select').select2({
	    minimumResultsForSearch: Infinity
	});	
	
	$(".tree-default").fancytree({
        checkbox: true,
        selectMode: 2,
      //  autoCollapse:true,
        generateIds:false,
        clickFolderMode:3,
        defaultKey:function(node){
        	return $(node.li).attr("id");
        }
    });
	
})
