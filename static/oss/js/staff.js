$('.datatable-basic').DataTable({
	"columnDefs": [
         {
             width: '150px',
             targets: 6
         },
         {
             width: '300px',
             targets: 3
         }
     ],
	 "ajax" : {
		"url" : contextpath + "/staff/list",
		"data" : function(d) {
            var query = $("#query").serializeObject();
            query["size"] = d.length;
            query["page"] = (d.start/d.length);
			return query;
		},
		"dataFilter": function(data){
            var json = jQuery.parseJSON( data );
            json.recordsTotal = json.totalElements;
            json.recordsFiltered = json.totalElements;
            json.data = json.content;
 
            return JSON.stringify( json );
        }
	 },
	 "columns": [
         { "data": "name" },
         { "data": "phone" },
         { "data": "type" },
         { "data": "storeName" },
         {
             render : function(data,type, row, meta) {
                 return row["oauthId"] != null ? "已绑定":"";
             }
         },
         { "data": "bindTime" },
         {
            render : function(data,type, row, meta) {
                return $("#btnGroup").html();
            }
         }
     ],
     "drawCallback": function( settings ) {
    	 $(".btn-edit").click(function(){
 			$("#edit").validate().resetForm();
 		    $("#edit").fill($('.datatable-basic').DataTable().row($(this).closest("tr")).data());
 		    $(".select").trigger("change");
 		 });
    	 
		 $(".btn-del").click(function(){
			var row = $('.datatable-basic').DataTable().row($(this).closest("tr")).data();
			swal({
				title : "删除",
				text : "确定要删除员工用户 "+row.name+" ?",
				type : "warning",
				showCancelButton : true,
				confirmButtonColor : "#DD6B55",
				confirmButtonText : "确定",
				cancelButtonText: "取消",
				closeOnConfirm : false
			},function() {
				$.get(contextpath + "staff/delete/" + row.id, function(d){
					$('.datatable-basic').DataTable().draw();
					swal("删除员工成功","", "success");
				})
			});
			
		 });
      }
});

$(function(){
	 
	$(".btn-add").click(function(){
		$("#edit").validate().resetForm();
		$("#edit")[0].reset();
		$(".select").trigger("change");
	});
	 
	$("#submitEdit").click(function(){
		if($("#edit").valid()){
			$.post(contextpath+"staff/save", $("#edit").serialize(), function(){
				$('.datatable-basic').DataTable().draw();
				$('.close').click();
				swal("新增用户成功","", "success");
			})
		}
	});
	
	$("#submit").click(function(){
		$('.datatable-basic').DataTable().draw();
	});

	$('select').select2({
	    minimumResultsForSearch: Infinity
	});	
	
})
