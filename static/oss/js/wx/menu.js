function refresh(){
	$.get("/mall/wxmenu/query/"+$("#domain").val(), function(data){
		for(var i in data){
			$("[name='menu"+i+".name']").val(data[i]["name"]);
			$("[name='menu"+i+".url']").val(data[i]["url"]);

			for(var j in data[i]["subButton"]){
				$("[name='menu"+i+".sub"+j+".name']").val(data[i]["subButton"][j].name);
				$("[name='menu"+i+".sub"+j+".url']").val(data[i]["subButton"][j].url);
			}
		}
	})
}



$(function(){
	refresh();
	
	$("#save").click(function(){
		$.post("/mall/wxmenu/create", $("#query").serialize(), function(data){
			 swal("菜单保存成功", "", "success");
		});
	});
	
	$("#refresh").click(refresh);
	
	$("#reset").click(function(){
		$("#query")[0].reset();
	})
	
})
