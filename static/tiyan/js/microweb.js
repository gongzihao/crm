 function ImgPosInitMob(obj, type) {
 	var img = $(obj);
 	if (!type) type = 1
 	if (typeof obj.naturalWidth != "undefined") {
 		var realWidth = obj.naturalWidth;
 		var realHeight = obj.naturalHeight;
 		var standwidth = $(img.parent()).width();
 		var standheight = $(img.parent()).height();
 		var tempwidth = 0,
 			tempheight = 0;
 		var ratew = standwidth / realWidth,
 			rateh = standheight / realHeight;
 		if (type == 1) {
 			if (rateh >= ratew) {
 				tempheight = standheight;
 				tempwidth = realWidth * rateh;
 			} else {
 				tempwidth = standwidth;
 				tempheight = realHeight * ratew;
 			}
 		} else if (type == 2) {
 			if (rateh <= ratew) {
 				tempheight = standheight;
 				tempwidth = realWidth * rateh;
 			} else {
 				tempwidth = standwidth;
 				tempheight = realHeight * ratew;
 			}
 		}

 		img.height(tempheight);
 		img.width(tempwidth);
 		img.css("margin-top", -(tempheight - standheight) / 2);
 		img.css("margin-left", -(tempwidth - standwidth) / 2);
 	}
 }

 function replaceEm(html) {
	if (html == "" || html == undefined) return;
	var _html = html;
	var pattern = new RegExp("\\[em:[\\s\\S]*?:\\]", "g");
	var emarr = html.match(pattern);
	if (emarr != null) {
		emarr = $.unique(emarr);
		for (var i = 0; i < emarr.length; i++) {
			var emnum = emarr[i].split(':')[1];
			var empattern = new RegExp("\\[em:" + emnum + ":\\]", "g");
			_html = _html.replace(empattern, '<img src="http://res1.sheyi.com/images/face/' + emnum + '.gif">');
		}
	}
	return _html;
}

function checkPhone(strPhone) {
	var phoneReg = /^[1][3,4,5,7,8][0-9]{9}$/;
	return phoneReg.test(strPhone);
}

function checkQQ(strQQ) {
	var QQReg = /^[1-9]\d{4,12}$/;
	return QQReg.test(strQQ);
}

!function(e, t) {
    var n = e.documentElement,
        i = navigator.userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/),
        a = i ? Math.min(t.devicePixelRatio, 3) : 1,
        a = window.top === window.self ? a : 1,
        d = "orientationchange" in window ? "orientationchange" : "resize";
    n.dataset.dpr = a;
    var o = function() {
        var e = n.clientWidth;
        e / a > 750 && (e = 750 * a), n.dataset.width = e, n.dataset.percent = 100 * (e / 750), n.style.fontSize = 28 * (e / 750) + "px"
    };
    o(), e.documentElement.classList.add("iosx" + t.devicePixelRatio), e.addEventListener && t.addEventListener(d, o, !1)
}(document, window)